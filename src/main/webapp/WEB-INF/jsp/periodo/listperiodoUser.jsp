<%@ page contentType="text/html; charset=utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<html> 
<head>

	<title>WebEasyRent</title>

	<!-- ********************************* Included CSS Files (Compressed) ***************************************** -->
	<link rel="stylesheet" href="/static/css/foundation.css">
	<link rel="stylesheet" href="/static/css/main.css">
	<link rel="stylesheet" href="/static/css/app.css">
	<link rel="stylesheet" href="/static/css/ligature.css">
	<link rel="stylesheet" href="/static/fonts/foundation-icons.css">

	<!-- Google fonts -->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300|Playfair+Display:400italic'
		  rel='stylesheet' type='text/css'/>

	<%-- ***************************************** Java scripts ****************************************************--%>
	<script src="/static/js/modernizr.foundation.js"></script>
	<!-- Incluir JQuery -->
	<script src="/static/js/jquery.js" type="text/javascript"></script>
	<!-- Incluir JS especifico de esta página -->
	<script src="/static/js/index.js" type="text/javascript"></script>

  <script>
	function borrar(id_propiedad, id_periodo) {
	    var r = confirm("¿¡Estas seguro que desea borrar el period!?");
	    if (r == true) {
	    	var newURL = window.location.protocol + "//" + window.location.host +
					"/my/propiedad/" + id_propiedad + "/deleteperiodo/" + id_periodo + ".html";
	    	location.href = newURL;
	    }
	}
</script>

	<!-- *********************************************** Header **************************************************## -->
	<header>
		<div class="twelve columns">
			<div class="logo" style="margin-left: 5px">
				<a href="${pageContext.request.contextPath}/index.jsp"><img src="/static/imgs/logoV2.png" alt=""/></a>
			</div>
			<div class="row">
				<ul id="menu-header" class="nav-bar horizontal">
					<li class="" style="float:right"><i class="fi-torso" style="color: white"></i></a><t:logininfo/></li>
				</ul>
			</div>
		</div>
		<h2 class="welcome_text">Periodos de la Propiedad "${propiedad.titulo}"</h2>
	</header>
</head>
<body>
<!-- ************************************* Lista Propiedades link *********************************************** -->
<div class="row">
	<h2><a href="${pageContext.request.contextPath}/my/propiedad/listpropiedades.html">Volver a la lista de Propiedades</a></h2>
</div>

<div><a href="addperiodo.html"><input type="button" class="button success" value="Nuevo periodo" /></a></div>
<div>
	<table class="table-propio">
		<tr>
			<th>Id propiedad</th>
			<th>Fecha inicio</th>
			<th>Fecha fin</th>
		</tr>
		<c:forEach items="${periodo}" var="periodo">
			<tr>
				<td class="col-md-4">${periodo.id_propiedad}</td>
				<td class="col-md-4">${periodo.inicio}</td>
				<td class="col-md-4">${periodo.fin}</td>
				<td class="col-md-4"><a href="updateperiodo/${periodo.id_periodo}.html">
					<input type="button" class="button expand" value="Modificar" style="width: 100px; height: 35px"/></a>
					<input type="button" class="button alert" value="Borrar"
						   onclick="borrar('${periodo.id_propiedad}', '${periodo.id_periodo}')" style="width: 100px; height: 35px"/></td>
			</tr>
		</c:forEach>
	</table>
	</div>
</body> 
</html>