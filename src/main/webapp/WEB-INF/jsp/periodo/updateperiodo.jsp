<%@ page contentType="text/html; charset=utf-8" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
 
<html> 
<head>
	<title>WebEasyRent</title>

	<!-- ********************************* Included CSS Files (Compressed) ***************************************** -->
	<link rel="stylesheet" href="/static/css/foundation.css">
	<link rel="stylesheet" href="/static/css/main.css">
	<link rel="stylesheet" href="/static/css/app.css">
	<link rel="stylesheet" href="/static/css/ligature.css">
	<link rel="stylesheet" href="/static/fonts/foundation-icons.css">

	<!-- Google fonts -->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300|Playfair+Display:400italic' rel='stylesheet' type='text/css'/>

	<%-- ***************************************** Java scripts ****************************************************--%>
	<script src="/static/js/modernizr.foundation.js"></script>
	<!-- Incluir JQuery -->
	<script src="/static/js/jquery.js" type="text/javascript"></script>
	<!-- Incluir JS especifico de esta página -->
	<script src="/static/js/index.js" type="text/javascript"></script>

	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
	<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>

	<script>
		$(function() {
			$( ".datepicker" ).datepicker({ dateFormat: 'yy-mm-dd' });
		});
	</script>

	<!-- *********************************************** Header **************************************************## -->
	<header>
		<div class="twelve columns">
			<div class="logo" style="margin-left: 5px">
				<a href="${pageContext.request.contextPath}/index.jsp"><img src="/static/imgs/logoV2.png" alt=""/></a>
			</div>
			<div class="row">
				<ul id="menu-header" class="nav-bar horizontal">
					<li class="" style="float:right"><i class="fi-torso" style="color: white"></i></a><t:logininfo/></li>
				</ul>
			</div>
		</div>
		<h2 class="welcome_text">Modificar datos del Periodo de la Propiedad "${propiedad.titulo}"</h2>
	</header>
</head>
<body>
<div>
	<form:form method="post" modelAttribute="periodo">
		<div>
			<table>
				<tr>
					<td><form:label path="inicio">Fecha inicio *</form:label></td>
					<td><form:input path="inicio" cssClass="datepicker start" /></td>
					<td><form:errors path="inicio" cssClass="error"/></td>
				</tr>
				<tr>
					<td><form:label path="fin">Fecha fin *</form:label></td>
					<td><form:input path="fin" cssClass="datepicker start" /></td>
					<td><form:errors path="fin" cssClass="error"/></td>
				</tr>
				<tr>
					<td>
						<small>(*) Campos obligatorios</small>
					</td>
					<td><a href="${pageContext.request.contextPath}/my/propiedad/${periodo.id_propiedad}/periodos.html">
						<input type="button" class="button alert" value="Cancelar"/></a></td>
					<td><input type="submit" class="button success" value="Guardar"/></td>
				</tr>
			</table>
		</div>
	</form:form>
</div>
</body>
</html>