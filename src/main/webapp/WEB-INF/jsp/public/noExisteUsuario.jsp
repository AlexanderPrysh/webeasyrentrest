<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ page contentType="text/html; charset=utf-8" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>


<head>

    <meta charset="UTF-8"/>

    <!-- Set the viewport width to device width for mobile -->
    <meta name="viewport" content="width=device-width"/>

    <title>EasyRent</title>

    <!-- Included CSS Files (Compressed) -->
    <link rel="stylesheet" href="/static/css/foundation.css">
    <link rel="stylesheet" href="/static/css/main.css">
    <link rel="stylesheet" href="/static/css/app.css">

    <script src="/static/js/modernizr.foundation.js"></script>

    <link rel="stylesheet" href="/static/css/ligature.css">

    <link rel="stylesheet" href="/static/fonts/foundation-icons.css">


    <!-- Google fonts -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300|Playfair+Display:400italic'
          rel='stylesheet' type='text/css'/>


    <!-- ######################## Scripts ######################## -->

    <!-- Incluir JQuery -->
    <script src="/static/js/jquery.js" type="text/javascript"></script>
    <!-- Incluir JS especifico de esta página -->
    <script src="/static/js/index.js" type="text/javascript"></script>

    <!-- ######################## Header ######################## -->

    <header>
        <div class="twelve columns">
            <div class="logo" style="margin-left: 5px">
                <a href="${pageContext.request.contextPath}/index.jsp"><img src="/static/imgs/logoV2.png" alt=""/></a>
            </div>
            <div class="row">
                <ul id="menu-header" class="nav-bar horizontal">
                    <li class="" style="float:right"><a href="login.html"><i class="fi-torso"></i> Login</a><t:logininfo/></li>
                </ul>
            </div>

        </div>
        <%--<h1 class="heading_supersize">Herramientas del administrador</h1>  --%>
        <h2 class="welcome_text">¡Atención!</h2>
    </header>
</head>
<body>

<!-- ######################## Section Home ######################## -->
<%--<div class="row">--%>
<%--<h3><a href="${pageContext.request.contextPath}/index.jsp">Ir al Inicio...</a></h3>--%>
<%--</div>--%>
<!-- ######################## Section ######################## -->

<section class="section_main">

    <h2 style="text-align:center">¡¡¡¡¡ EL USUARIO NO EXISTE !!!!!</h2>
</section>

</body>
</html>