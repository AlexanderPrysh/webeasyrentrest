<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>
<head>

    <meta charset="UTF-8"/>

    <!-- Set the viewport width to device width for mobile -->
    <meta name="viewport" content="width=device-width"/>

    <title>EasyRent</title>

    <!-- Included CSS Files (Compressed) -->
    <link rel="stylesheet" href="/static/css/foundation.css">
    <link rel="stylesheet" href="/static/css/main.css">
    <link rel="stylesheet" href="/static/css/app.css">

    <script src="/static/js/modernizr.foundation.js"></script>

    <link rel="stylesheet" href="/static/css/ligature.css">

    <link rel="stylesheet" href="/static/fonts/foundation-icons.css">


    <!-- Google fonts -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300|Playfair+Display:400italic'
          rel='stylesheet' type='text/css'/>


    <!-- ######################## Scripts ######################## -->

    <!-- Incluir JQuery -->
    <script src="/static/js/jquery.js" type="text/javascript"></script>
    <!-- Incluir JS especifico de esta página -->
    <script src="/static/js/index.js" type="text/javascript"></script>




</head>

<body>
<!-- ######################## Up-buttom ######################## -->

    <span class="to-top icon-arrow-up2" style="display: none;">^</span>

<!-- ######################## Header ######################## -->
<%--<div class="col-sm-8 header-left">--%>
    <%--<div class="logo">--%>
        <%--<a><img src="/static/imgs/logo.png" alt=""/></a>--%>
    <%--</div>--%>
<%--</div>--%>

<header>
    <div class="twelve columns">
        <div class="logo">
            <a href="index.jsp"><img src="/static/imgs/logoV2.png" alt=""/></a>
        </div>

        <div class="row">
            <ul id="menu-header" class="nav-bar horizontal">
                <li class="" style="float:right">
                    <a href="login.html"><i class="fi-torso"></i> Login</a>
                    <t:logininfo/>
                </li>
            </ul>
        </div>
    </div>
    <h1 class="heading_supersize">Easy Rent</h1>
    <h2 class="welcome_text">Renting nunca fue tan facil!</h2>
</header>

<!-- ######################## Search bar section ######################## -->
<section class="section_main">

    <%--<h2 style="text-align:center">Barra de búsqueda</h2>--%>
    <li class="has-form">
        <div class="row collapse">
            <div class="large-18 small-9 columns">
                <input type="text" id="search_descrip" placeholder="Palabra clave..." style="width: 600px">
            </div>
            <div class="large-4 small-3 columns">
                <a type="submit" class="success button expand" style="width: 200px"
                        onclick="this.href='<%= request.getContextPath() %>/buscador.html?descrip=' + document.getElementById('search_descrip').value;" href=''>
                    Buscar
                </a>

            </div>
        </div>
    </li>

</section>

<!-- ######################## Newest item section ######################## -->
<section style="background:#fafafa">


    <div class="row">
    <h2>Lo más nuevo</h2>
    <c:forEach items="${indeximages}" var="img">
        <div class="box photo col2" style="padding: 0">
            <a href="../showpropiedad/${img.id_propiedad}.html"><img src="${img.url}" alt="desc" style="height:150px"></a>
            <p >
                <a href="../showpropiedad/${img.id_propiedad}.html">Mas Info</a>
            </p>
        </div>
    </c:forEach>
    </div>

</section>


<!-- ######################## Top rated section ######################## -->

<section style="background:#fafafa">

    <div class="row">

        <h2>Los más valorados</h2>

        <div class="box photo col2" style="padding: 0">
            <a href="product-single.html"><img src="/static/imgs/house_demo.jpg" alt="desc"></a>
            <p >
                <a href="product-single.html">Mas Info</a>
            </p>
        </div>

        <div class="box photo col2" style="padding: 0">
            <a href="product-single.html"><img src="/static/imgs/house_demo.jpg" alt="desc"></a>
            <p >
                <a href="product-single.html">Mas Info</a>
            </p>
        </div>

        <div class="box photo col2" style="padding: 0">
            <a href="product-single.html"><img src="/static/imgs/house_demo.jpg" alt="desc"></a>
            <p>
                <a href="product-single.html" >Mas Info</a>
            </p>
        </div>

        <div class="box photo col2" style="padding: 0">
            <a href="product-single.html"><img src="/static/imgs/house_demo.jpg" alt="desc"></a>
            <p >
                <a href="product-single.html">Mas Info</a>
            </p>
        </div>

    </div>
    <br>

</section>

<!-- ######################## Additional Section ######################## -->

<section class="section_main">

    <h2 style="text-align:center">Ver más ofertas...</h2>

</section>


<ul id="lista_ejemplo">

</ul>


<!-- ######################## Footer section ######################## -->

<footer>
    <!-- .footer start -->
    <!-- ================ -->
    <div class="footer section">
        <div class="container">
            <h1 class="title text-center" id="contacta">Contacto</h1>
            <div class="space"></div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="footer-content">
                        <p class="large">¿Tiene dudas?, ¿preguntas?, no dude en enviarnos un mensaje. ¡Le contestaremos lo antes posible!.</p>
                        <ul class="list-icons">
                            <li><i class="fa fa-map-marker pr-10"></i> C/ QueCalle, 12 Castellon España</li>
                            <li><i class="fa fa-phone pr-10"></i> +34 65465465464564</li>
                            <li><i class="fa fa-fax pr-10"></i> +34 545646546 </li>
                            <li><i class="fa fa-envelope-o pr-10"></i> easyrentproyect@gmail.com</li>
                        </ul>
                        <i class="fa fa-info-circle"></i>&nbsp;&nbsp;&nbsp;&nbsp;También puedes ponerte en contacto a través de redes sociales
                        <div class="row">

                            <div class="twelve columns footer">
                                <a href="http://twitter.com/" class="lsf-icon" style="font-size:16px; margin-right:15px"
                                   title="twitter">Twitter</a>
                                <a href="http://facebook.com/" class="lsf-icon"
                                   style="font-size:16px; margin-right:15px" title="facebook">Facebook</a>
                                <a href="http://pinterest.com/" class="lsf-icon"
                                   style="font-size:16px; margin-right:15px" title="pinterest">Pinterest</a>
                                <a href="http://instagram.com/" class="lsf-icon" style="font-size:16px"
                                   title="instagram">Instagram</a>
                            </div>

                        </div>
                    </div>
                </div>


                <div>
                    <%--<p style="text-align:center;">Para contacPulsa sobre el boton para poder enviarnos tu pregunta</p>--%>
                    <input type="checkbox"  id="spoiler2" />
                    <label for="spoiler2" >Contactar</label>
                    <div class="spoiler">
                        <div class="footer-content">
                            <form:form method="post" role="form"  id="footer-form">
                                <div>
                                    <label class="sr-only" for="name2">Nombre</label>
                                    <input type="text" id="name2" placeholder="Nombre" name="nombre" required>
                                </div>
                                <div>
                                    <label class="sr-only" for="email2">Correo electronico</label>
                                    <input type="email" id="email2" placeholder="Correo electronico" name="email" required>
                                </div>
                                <div>
                                    <label class="sr-only" for="message2">Mensaje</label>
                                    <textarea rows="8" id="message2" placeholder="Mensaje, pregunta..." name="mensaje" required></textarea>
                                </div>
                                <div class="col-lg-4">	<input type="submit" value="¡Enviar!" onclick="doAjax()" class="button expand"></div>
                            </form:form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- .footer end -->
</footer>

<%--<script>--%>
    <%--window.alert("¡Cuidado con lo que haces en esta pagina!");--%>
<%--</script>--%>

<!-- JavaScript files placed at the end of the document so the pages load faster
		================================================== -->
<!-- Jquery and Bootstap core js files -->
<script type="text/javascript" src="${pageContext.request.contextPath}/webapp/static/plugins/jquery.min.js"></script>
<%--<script type="text/javascript" src="${pageContext.request.contextPath}/resources/bootstrap/js/bootstrap.min.js"></script>--%>

<!-- Modernizr javascript -->
<script type="text/javascript" src="${pageContext.request.contextPath}/webapp/static/plugins/modernizr.js"></script>

<!-- Isotope javascript -->
<script type="text/javascript" src="${pageContext.request.contextPath}/webapp/static/plugins/isotope/isotope.pkgd.min.js"></script>

<!-- Backstretch javascript -->
<script type="text/javascript" src="${pageContext.request.contextPath}/webapp/static/plugins/jquery.backstretch.min.js"></script>

<!-- Appear javascript -->
<script type="text/javascript" src="${pageContext.request.contextPath}/webapp/static/plugins/jquery.appear.js"></script>

<!-- Initialization of Plugins -->
<script type="text/javascript" src="${pageContext.request.contextPath}/webapp/static/js/template.js"></script>

<script type="text/javascript">
//    alert("Estoy llamando al script");
    $( "#footer-form" ).submit(function( event ) {
        event.preventDefault();
        var $form = $( this ),
                nombre = $form.find( "input[name='nombre']" ).val(),
                email = $form.find( "input[name='email']" ).val(),
                mensaje = $form.find( "textarea[name='mensaje']" ).val();
        var posting = $.post( "ajaxmailcontact.html", {nombre: nombre, email: email, mensaje: mensaje});
        posting.done(function( data ) {
            $( "#result" ).empty().append( data );
            $( "#result" )
                    .slideUp( 300 ).delay( 800 ).fadeIn( 400 );
        });
        alert("El correo se ha enviado correctamente. Le enviaremos una respuesta lo mas pronto posible, ¡gracias! ");
        location.reload(true);
    });
</script>
</body>
</html>