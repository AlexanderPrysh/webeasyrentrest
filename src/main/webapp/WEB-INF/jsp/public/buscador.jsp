<%@page contentType="text/html; charset=iso-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<!DOCTYPE html>
<head>

  <meta charset="UTF-8"/>

  <!-- Set the viewport width to device width for mobile -->
  <meta name="viewport" content="width=device-width"/>

  <title>EasyRent</title>

  <!-- Included CSS Files (Compressed) -->
  <link rel="stylesheet" href="/static/css/foundation.css">
  <link rel="stylesheet" href="/static/css/main.css">
  <link rel="stylesheet" href="/static/css/app.css">
  <script src="/static/js/modernizr.foundation.js"></script>
  <link rel="stylesheet" href="/static/css/ligature.css">
  <link rel="stylesheet" href="/static/fonts/foundation-icons.css">

            <link rel="stylesheet" href="/static/css/lista-pisos.css"/>



  <!-- Google fonts -->
  <link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300|Playfair+Display:400italic'
        rel='stylesheet' type='text/css'/>


  <!-- ######################## Scripts ######################## -->

  <!-- Incluir JQuery -->
  <script src="/static/js/jquery.js" type="text/javascript"></script>
  <!-- Incluir JS especifico de esta p�gina -->
  <script src="/static/js/index.js" type="text/javascript"></script>
  <!-- Script para busqueda de propiedad dinamica -->
  <script src="/static/js/search.js"></script>

    <script type="text/javascript">
        $('document').ready(function() {
            var url = decodeURI(window.location.search.substring(1))
            var val = url.split('=');
            $('#descripcion').val(val[1])
            $('#search-button').click();
        })
    </script>
  <!-- ######################## Header ######################## -->

  <header>
    <%--<h1 class="heading_supersize">Herramientas del administrador</h1>  --%>
        <div class="twelve columns">
            <div class="logo" style="margin-left: 5px">
                <a href="${pageContext.request.contextPath}/index.jsp"><img src="/static/imgs/logoV2.png" alt=""/></a>
            </div>
            <div class="row">
                <ul id="menu-header" class="nav-bar horizontal">
                    <li class="" style="float:right"><a href="../login.html"><i class="fi-torso"></i> Login</a><t:logininfo/></li>
                </ul>
            </div>
        </div>
    <h2 class="welcome_text">Buscador Inmobiliario</h2>
  </header>

<body>
<div class="row">
    <fieldset>
        <legend><h2 style="color:white; background:dodgerblue">Filtro de b�squeda</h2></legend>

            <div class="row">
                <div class="large-6 columns">
                    <label>Tipo de propiedad
                        <input id="tipo_prop" type="text" placeholder="casa,chalet,adosado..."/>
                    </label>
                </div>
                <div class="large-6 columns">
                    <label>Provincia
                        <input id="ciudad" type="text" placeholder="provincia"/>
                    </label>
                </div>
            </div>

            <div class="row">
                <div class="large-4 columns">
                    <label>Precio
                        <input id="precioMin" type="text" placeholder="precio m�nimo"/>
                        <input id="precioMax" type="text" placeholder="precio m�ximo"/>
                    </label>
                </div>
                <div class="large-4 columns">
                    <label>N� habitaciones
                        <input id="num_hab_min" type="text" placeholder="m�nimo"/>
                        <input id="num_hab_max" type="text" placeholder="m�ximo"/>
                    </label>
                </div>
                <div class="large-4 columns">
                    <label>Metros cuadrados
                        <input id="metros_min" type="text" placeholder="m�nimo"/>
                        <input id="metros_max" type="text" placeholder="m�ximo"/>
                    </label>
                </div>
            </div>

            <div class="row">
                <div class="large-6 columns">
                    <label>Descripcion
                        <input id="descripcion" name="descrip" type="text" placeholder="descripci�n"/>
                    </label>
                </div>
                <div class="large-6 columns">

                    <a class="button dropdown" href="#" data-dropdown="drop1">Ordenar</a>
                    <%--<ul class="f-dropdown" id="ordenar" data-dropdown-content="">
                        <li><a href="#">Tipo</a></li>
                        <li><a href="#">Precio mas bajo</a></li>
                        <li><a href="#">Servicios</a></li>
                    </ul>--%>

                </div>
            </div>

        <div class="large-6 columns">
            <input type="button" id="search-button" value="Buscar" class="button round "/>
        </div>
    </fieldset>
</div>
<div class="row">
    <div class="large-12 columns">
        <div class="table-responsive">
            <table class="table table-hover" id="lista-pisos">
                <tr class="row">
                    <th>Titulo</th>
                    <th>Tipo propiedad</th>
                    <th>Capacidad</th>
                    <th>Numero habitaciones</th>
                    <th>Metros cuadrados</th>
                    <th>Ciudad</th>
                    <th>Precio diario</th>
                    <th>Descripcion</th>
                   <%-- <th>Imagenes</th>
                    <th>Servicios</th>--%>
                </tr>
            </table>
        </div>
    </div>
</div>
</body>

</html>