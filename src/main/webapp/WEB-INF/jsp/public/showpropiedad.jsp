<%@ page contentType="text/html; charset=utf-8" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<html> 
<head>

	<!-- Included CSS Files (Compressed) -->
	<link rel="stylesheet" href="/static/css/foundation.css">
	<link rel="stylesheet" href="/static/css/main.css">
	<link rel="stylesheet" href="/static/css/app.css">

	<script src="/static/js/modernizr.foundation.js"></script>
    <script src="/static/js/jquery.js"></script>
	<link rel="stylesheet" href="/static/css/ligature.css">

	<link rel="stylesheet" href="/static/fonts/foundation-icons.css">

	<script type="text/javascript">
		$(document).ready(function(){
			$("#peques img").hover(function(){
				var imagen=$(this).attr("src");
				$("#grande").attr("src",imagen);
			});
		})
	</script>

	<!-- Google fonts -->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300|Playfair+Display:400italic'
		  rel='stylesheet' type='text/css'/>

  <title>Easy Rent</title>
	<header>
		<div class="twelve columns">
			<div class="logo" style="margin-left: 5px">
				<a href="${pageContext.request.contextPath}/index.jsp"><img src="/static/imgs/logoV2.png" alt=""/></a>
			</div>
			<div class="row">
				<ul id="menu-header" class="nav-bar horizontal">
					<li class="" style="float:right"><a href="login.html"><i class="fi-torso"></i> Login</a><t:logininfo/></li>
				</ul>
			</div>

		</div>
		<%--<h1 class="heading_supersize">Herramientas del administrador</h1>  --%>
		<h2 class="welcome_text">Informacion de la propiedad</h2>
	</header>
</head>
<body>


<table>
	<tr>
		<td>

			<div id="peques">
				<c:forEach items="${imagen}" var="imagen">
					<img src="${imagen.url}" width="100" height="77"/>
				</c:forEach>
			</div>
			<img src="/static/imgs/prod_thumb.png" alt="prop_1a" name="grande" width="338" height="253" id="grande"/>
		</td>
		<div>
			<td>
				<h1 style="color:white; background:dodgerblue">Vista previa propiedad</h1>
				<form:form method="post" modelAttribute="propiedad">
					<div class="table-responsive">

						<li>Titulo: ${propiedad.titulo}</li>
						<li>Descripcion: ${propiedad.descripcion}</li>
						<li>Tipo de propiedad: ${propiedad.tipo_propiedad}</li>
						<li>Capacidad: ${propiedad.capacidad}</li>
						<li>Nº habitaciones: ${propiedad.num_habitaciones}</li>
						<li>Nº baños: ${propiedad.num_banyos}</li>
						<li>Nº camas: ${propiedad.num_camas}</li>
						<li>M^2: ${propiedad.metros_cuadrados}</li>
						<li>Direccion: ${propiedad.calle} num ${propiedad.numero} planta ${propiedad.planta}</li>
						<li>Provincia: ${propiedad.ciudad}</li>
						<li>Precio: ${propiedad.precio_diario} €</li>
					</div>
				</form:form>
				<table>
					<tr>
						<td colspan="2"><input type="button" class="button expand" value="Volver"
											   onclick="history.back()"/></td>
						<td>
							<br>
						</td>
						<td colspan="2"><a
								href="${pageContext.request.contextPath}/my/reserva/addreserva/${propiedad.id_propiedad}.html"><input
								type="button" class="button success" value="Reservar"/></a></td>
					</tr>
				</table>
			</td>
		</div>

		<td>
			<div>
				<table>
					<tr>
						<th>Periodos Libres</th>
					</tr>
					<c:forEach items="${fechalibre}" var="fecha">
						<tr>
							<td>${fecha}</td>
						</tr>
					</c:forEach>
				</table>
			</div>
		</td>
	</tr>
	<tr>
	<div>
		<tr>
			<div>
				<td colspan="3"><h3>Comentario (${media})<i class="fi-star" style="color: yellow"/></h3></td>
				<table>
					<c:forEach items="${valoracion}" var="valoracion">
						<tr>
							<td>${valoracion.voto}</td>
							<td>${valoracion.comentario}</td>
						</tr>
					</c:forEach>
				</table>
			</div>
		</tr>
	<div>
		<tr>
				<form:form method="post" modelAttribute="nuevavaloracion">
				<div><h3><form:errors path="voto" cssClass="error"/></h3></div>
				<div><Label>Valoracion</Label></div>
				<div><input path="voto" type="radio" name="voto" value="1">1
					<input path="voto" type="radio" name="voto" value="2">2
					<input path="voto" type="radio" name="voto" value="3">3
					<input path="voto" type="radio" name="voto" value="4">4
					<input path="voto" type="radio" name="voto" value="5">5
				</div>

		</tr>
		<tr>
			<div><Label>Comentario</Label></div>
			<div><form:textarea path="comentario"/></div>

			<div><input type="submit" class="button success" value="Enviar"/></div>
			</form:form>
		</tr>
		</div>
	</div>

	</tr>
</table>
</body>
</html>