<%@page contentType="text/html; charset=iso-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<head>

	<meta charset="utf-8"/>

	<!-- Set the viewport width to device width for mobile -->
	<meta name="viewport" content="width=device-width"/>

	<!-- Included CSS Files (Compressed) -->
	<link rel="stylesheet" href="/static/css/foundation.css">
	<link rel="stylesheet" href="/static/css/main.css">
	<link rel="stylesheet" href="/static/css/app.css">
	<link rel="stylesheet" href="/static/css/login.css">
	<link rel="stylesheet" href="/static/css/login.scss">
	<script src="/static/js/modernizr.foundation.js"></script>
	<link rel="stylesheet" href="/static/css/ligature.css">
	<link rel="stylesheet" href="/static/fonts/foundation-icons.css">


	<!-- Google fonts -->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300|Playfair+Display:400italic'
		  rel='stylesheet' type='text/css'/>


	<!-- ######################## Scripts ######################## -->

	<!-- Incluir JQuery -->
	<script src="/static/js/jquery.js" type="text/javascript"></script>
	<!-- Incluir JS especifico de esta página -->
	<script src="/static/js/index.js" type="text/javascript"></script>


	<title>Login</title>
	<header>
		<div class="twelve columns">
			<div class="logo" style="margin-left: 5px">
				<a href="${pageContext.request.contextPath}/index.jsp"><img src="/static/imgs/logoV2.png" alt=""/></a>
			</div>
			</div>
	<h2 class="welcome_text">Autentificación de ususario</h2>
	</header>
</head>
<body>

    <%--<h1 style="color:white; background:dodgerblue">Acceso</h1>--%>
	<div class="large-3 large-centered columns">
		<div class="login-box">
			<h1 style="color:white; background:dodgerblue">Acceso</h1>
			<div class="row">
				<div class="large-12 large-centered columns">
					<form:form method="post" modelAttribute="usuario">
						<div class="row"><h4><form:errors path="usuario" cssClass="error" /></h4></div>
						<div class="row">
							<div class="large-12 columns">
								<form:label path="usuario">Nombre de usuario:</form:label>
								<form:input path="usuario" placeholder="Usuario"/>
							</div>
						</div>
						<div class="row">
							<div class="large-12 columns">
								<form:label path="Contrasenya">Contraseña:</form:label>
								<form:password path="Contrasenya" placeholder="Contraseña"/>
							</div>
						</div>
						<div class="row">
							<div>
								<p class="text-center"><a href="${pageContext.request.contextPath}/contrasenya/recuperarcontrasenya.html">Te has olvidado la contraseña?</a></p>
							</div>
							<div class="large-12 large-centered columns">
								<td><a href="${pageContext.request.contextPath}/index.jsp"><input type="button" class="button expand" value="Volver" /></a></td>
							</div>

							<div class="large-12 large-centered columns">
								<td></ts><input type="submit" class="button success" value="Acceder" /></td>
							</div>
						</div>
					</form:form>
				</div>
			</div>
		</div>
	</div>


</body>
</html>

<!-- ######################## Contenido original del form ######################## -->


<%--<div class="row">--%>
	<%--<div class="large-12 columns">--%>
		<%--<form:label path="usuario">Nombre de usuario:</form:label>--%>
		<%--<form:input path="usuario" />--%>
	<%--</div>--%>
<%--</div>--%>
<%--<div class="row">--%>
	<%--<div class="large-12 columns">--%>
		<%--<form:label path="Contrasenya">Contraseña:</form:label>--%>
		<%--<form:password path="Contrasenya" />--%>
		<%--&lt;%&ndash;<form:errors path="contrasenya" cssClass="error" />&ndash;%&gt;--%>
	<%--</div>--%>
<%--</div>--%>
<%--<div class="row">--%>
	<%--<div>--%>
		<%--<p class="text-center"><a href="#">Te has olvidado la contraseña?</a></p>--%>
		<%--<br>--%>
	<%--</div>--%>
	<%--<div>--%>
		<%--<br>--%>
	<%--</div>--%>
	<%--<div class="large-12 large-centered columns">--%>
		<%--<td><a href="${pageContext.request.contextPath}/index.jsp"><input type="button" class="button expand" value="Volver" /></a></td>--%>
	<%--</div>--%>

	<%--<div class="large-12 large-centered columns">--%>
		<%--<td></ts><input type="submit" class="button success" value="Acceder" /></td>--%>
	<%--</div>--%>
<%--</div>--%>