<%@ page contentType="text/html; charset=utf-8" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>
<head>
	<title>WebEasyRent</title>

	<!-- ********************************* Included CSS Files (Compressed) ***************************************** -->
	<link rel="stylesheet" href="/static/css/foundation.css">
	<link rel="stylesheet" href="/static/css/main.css">
	<link rel="stylesheet" href="/static/css/app.css">
	<link rel="stylesheet" href="/static/css/ligature.css">
	<link rel="stylesheet" href="/static/fonts/foundation-icons.css">

	<!-- Google fonts -->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300|Playfair+Display:400italic' rel='stylesheet' type='text/css'/>

	<%-- ***************************************** Java scripts ****************************************************--%>
	<script src="/static/js/modernizr.foundation.js"></script>
	<!-- Incluir JQuery -->
	<script src="/static/js/jquery.js" type="text/javascript"></script>

	<%--<script>
		$.datepicker.regional['es'] = {
			closeText: 'Cerrar',
			prevText: '<Ant',
			nextText: 'Sig>',
			currentText: 'Hoy',
			monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
			monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
			dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
			dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
			dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
			weekHeader: 'Sm',
			dateFormat: 'yyyy-mm-dd',
			firstDay: 1,
			isRTL: false,
			showMonthAfterYear: false,
			yearSuffix: ''
		};
		$.datepicker.setDefaults($.datepicker.regional['es']);
		$(function () {
			$(".fecha").datepicker();
		});
	</script>--%>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
	<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
	<script>
		$(function() {
			$( ".datepicker" ).datepicker({ dateFormat: 'yy-mm-dd' });


		});
	</script>

	<!-- *********************************************** Header **************************************************## -->
	<header>
		<div class="twelve columns">
			<div class="logo" style="margin-left: 5px">
				<a href="${pageContext.request.contextPath}/index.jsp"><img src="/static/imgs/logoV2.png" alt=""/></a>
			</div>
			<div class="row">
				<ul id="menu-header" class="nav-bar horizontal">
					<li class="" style="float:right"><a href="login.html"><i
							class="fi-torso"></i>Login</a><t:logininfo/></li>
				</ul>
			</div>
		</div>
		<%--<h1 class="heading_supersize">Herramientas del administrador</h1>  --%>
		<h2 class="welcome_text">Añadir nuevo usuario</h2>
	</header>
</head>
<body>
<%--******************************************* Titulo ************************************************************ --%>
<div>
	<h1 class="titulo-lista">Nuevo usuario</h1>
</div>
<div>
	<form:form method="post" modelAttribute="usuario" onsubmit="alert('Una vez tenga los datos introducidos correctamente, se le va ha enviar un correo de confirmacion, para activar su cuenta por favor sigue las instrucciones  que se le indican el en correo, ¡gracias!');">
		<div>
			<table>
				<tr>
					<td><form:label path="usuario">Usuario *</form:label></td>
					<td><form:input path="usuario"/></td>
					<td><form:errors path="usuario" cssClass="error"/></td>
				</tr>
				<tr>
					<td><form:label path="contrasenya">Contraseña *</form:label></td>
					<td><form:input path="contrasenya" type="password"/></td>
					<td><form:errors path="contrasenya" cssClass="error"/></td>
				</tr>
				<tr>
					<td><form:label path="id_nacional">NIE/DNI *</form:label></td>
					<td><form:input path="id_nacional"/></td>
					<td><form:errors path="id_nacional" cssClass="error"/></td>
				</tr>
				<tr>
					<td><form:label path="nombre">Nombre *</form:label></td>
					<td><form:input path="nombre"/></td>
					<td><form:errors path="nombre" cssClass="error"/></td>
				</tr>
				<tr>
					<td><form:label path="apellido">Apellidos *</form:label></td>
					<td><form:input path="apellido"/></td>
					<td><form:errors path="apellido" cssClass="error"/></td>
				</tr>
				<tr>
					<td><form:label path="email">E-mail *</form:label></td>
					<td><form:input type="email" path="email"/></td>
					<td><form:errors path="email" cssClass="error"/></td>
				</tr>
				<tr>
					<td><form:label path="direccion">Direccion *</form:label></td>
					<td><form:input path="direccion"/></td>
					<td><form:errors path="direccion" cssClass="error"/></td>
				</tr>
				<tr>
					<td><form:label path="numero_telefono">Telefono *</form:label></td>
					<td><form:input path="numero_telefono"/></td>
					<td><form:errors path="numero_telefono" cssClass="error"/></td>
				</tr>
				<tr>
					<td>
						<small>(*) Campos obligatorios</small>
					</td>
					<td><a href="${pageContext.request.contextPath}/index.jsp">
						<input type="button" class="button alert" value="Cancelar"/></a>
					</td>
					<td><input type="submit" class="button success" onclick="this.form.submited=this.value;" value="Añadir usuario"/>
					</td>
				</tr>
			</table>
		</div>
	</form:form>
</div>

<%--<script language="JavaScript">--%>
	<%--function pregunta(){--%>
		<%--if (confirm('Una vez tenga los datos introducidos correctamente, se le va ha enviar un correo de confirmacion, para activar su cuenta por favor sigue las instrucciones  que se le indican el en correo, ¡gracias!')){--%>
			<%--document.tuformulario.submit()--%>
		<%--}--%>
	<%--}--%>
<%--</script>--%>

<%--<script>--%>
	<%--window.alert("¡Se te va a enviar un correo de confirmacion, para darte de alta por favor sigue las instrucciones  que se te indican el en correo!");--%>
<%--</script>--%>
</body>
</html>