<%@ page contentType="text/html; charset=utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<html>
<head>
    <title>WebEasyRent</title>

    <!-- ********************************* Included CSS Files (Compressed) ***************************************** -->
    <link rel="stylesheet" href="/static/css/foundation.css">
    <link rel="stylesheet" href="/static/css/main.css">
    <link rel="stylesheet" href="/static/css/app.css">
    <link rel="stylesheet" href="/static/css/ligature.css">
    <link rel="stylesheet" href="/static/fonts/foundation-icons.css">

    <!-- Google fonts -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300|Playfair+Display:400italic' rel='stylesheet' type='text/css'/>

    <%-- ***************************************** Java scripts ****************************************************--%>
    <script src="/static/js/modernizr.foundation.js"></script>
    <!-- Incluir JQuery -->
    <script src="/static/js/jquery.js" type="text/javascript"></script>
    <!-- Incluir JS especifico de esta página -->
    <script src="/static/js/index.js" type="text/javascript"></script>

    <script>
        function borrar(id_usuario) {
            var r = confirm("¿¡Estas seguro que desea borrar el usuario!?");
            if (r == true) {
                var newURL = window.location.protocol + "//" + window.location.host + "/my/usuario/deleteusuario/" + id_usuario + ".html";
                location.href = newURL;
            }
        }
    </script>

    <!-- *********************************************** Header **************************************************## -->
    <header>
        <div class="twelve columns">
            <div class="logo" style="margin-left: 5px">
                <a href="${pageContext.request.contextPath}/index.jsp"><img src="/static/imgs/logoV2.png" alt=""/></a>
            </div>
            <div class="row">
                <ul id="menu-header" class="nav-bar horizontal">
                    <li class="" style="float:right"><i class="fi-torso" style="color: white"></i></a><t:logininfo/></li>
                </ul>
            </div>
        </div>
        <h2 class="welcome_text">Lista de todos los usuarios</h2>
    </header>
</head>
<body>
    <!-- ************************************* Pagina principal link *********************************************** -->
    <div class="row">
        <h2><a href="${pageContext.request.contextPath}/my/dashboard.html">Volver al menu principal</a></h2>
    </div>

    <%--******************************************* Titulo de la tabla ******************************************** --%>

    <div><a href="addusuario.html"><input type="button" class="button success" value="Nuevo usuario" /></a></div>
    <div>
        <table class="table-propio">
            <tr>
                <th>ID usuario</th>
                <th>ID nacional</th>
                <th>Usuario</th>
                <th>Rol</th>
                <th>Nombre</th>
                <th>Apellido</th>
                <th>E-mail</th>
                <th>Direccion</th>
                <th>Fecha de registro</th>
                <th>Telefono</th>
                <th>Activo</th>
            </tr>
            <c:forEach items="${usuarios}" var="usuarios">
                <tr>
                    <td class="col-md-12">${usuarios.id_usuario}</td>
                    <td class="col-md-12">${usuarios.id_nacional}</td>
                    <td class="col-md-12">${usuarios.usuario}</td>
                    <td class="col-md-12">${usuarios.rol}</td>
                    <td class="col-md-12">${usuarios.nombre}</td>
                    <td class="col-md-12">${usuarios.apellido}</td>
                    <td class="col-md-12">${usuarios.email}</td>
                    <td class="col-md-12">${usuarios.direccion}</td>
                    <td class="col-md-12">${usuarios.fecha_registro}</td>
                    <td class="col-md-12">${usuarios.numero_telefono}</td>
                    <td class="col-md-12">${usuarios.es_activo}</td>

                    <td class="col-md-12">
                        <a href="updateusuario/${usuarios.id_usuario}.html">
                            <input type="button" class="button expand" value="Modificar" style="width:120px; height:35px"/></a>
                        <a href="${pageContext.request.contextPath}/my/usuario/${usuarios.id_usuario}/cambiarcontrasenya.html" style="width:100px; height:35px">
                            <input type="button" class="button expand" value="Contraseña" /></a>
                            <input type="button" class="button alert" value="Borrar" onclick="borrar('${usuarios.id_usuario}')" style="width:120px; height:35px"/>
                    </td>
                </tr>
            </c:forEach>
        </table>
    </div>
</body>
</html>
