<%@ page contentType="text/html; charset=utf-8" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<html> 
<head>

	<!-- Included CSS Files (Compressed) -->
	<link rel="stylesheet" href="/static/css/foundation.css">
	<link rel="stylesheet" href="/static/css/main.css">
	<link rel="stylesheet" href="/static/css/app.css">

	<script src="/static/js/modernizr.foundation.js"></script>

	<link rel="stylesheet" href="/static/css/ligature.css">

	<link rel="stylesheet" href="/static/fonts/foundation-icons.css">


	<!-- Google fonts -->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300|Playfair+Display:400italic'
		  rel='stylesheet' type='text/css'/>

	<script>
		$.datepicker.regional['es'] = {
			closeText: 'Cerrar',
			prevText: '<Ant',
			nextText: 'Sig>',
			currentText: 'Hoy',
			monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
			monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
			dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
			dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
			dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
			weekHeader: 'Sm',
			dateFormat: 'yyyy-mm-dd',
			firstDay: 1,
			isRTL: false,
			showMonthAfterYear: false,
			yearSuffix: ''
		};
		$.datepicker.setDefaults($.datepicker.regional['es']);
		$(function () {
			$(".fecha").datepicker();
		});
	</script>
	<!-- ######################## Header ######################## -->

	<header>
		<div class="twelve columns">
			<div class="row">

				<ul id="menu-header" class="nav-bar horizontal">
					<li class="" style="float:right"><a href="login.html"><i class="fi-torso"></i>Login</a><t:logininfo/></li>

				</ul>

			</div>

		</div>
		<h2 class="welcome_text">Modifica información del usuario: ${usuario.nombre} ${usuario.apellido}</h2>
	</header>
  <title>EI1027 - Disseny i Implementació de Sistemes d’Informació</title>
</head>
<body>
<div>
	<h1 class="titulo-lista">Modificar datos usuario</h1>
</div>
<div>
	<form:form method="post" modelAttribute="usuario">
		<div>
			<table>
				<tr>
					<td><form:label path="id_nacional" value="${usuario.id_nacional}">NIE/DNI *</form:label></td>
					<td><form:input path="id_nacional"/></td>
					<td><form:errors path="id_nacional" cssClass="error"/></td>
				</tr>
				<tr>
					<td><form:label path="nombre" value="${usuario.nombre}">Nombre *</form:label></td>
					<td><form:input path="nombre"/></td>
					<td><form:errors path="nombre" cssClass="error"/></td>
				</tr>
				<tr>
					<td><form:label path="apellido" value="${usuario.apellido}">Apellidos *</form:label></td>
					<td><form:input path="apellido"/></td>
					<td><form:errors path="apellido" cssClass="error"/></td>
				</tr>
				<tr>
					<td><form:label path="email" value="${usuario.email}">E-mail *</form:label></td>
					<td><form:input type="email" path="email"/></td>
					<td><form:errors path="email" cssClass="error"/></td>
				</tr>
				<tr>
					<td><form:label path="direccion" value="${usuario.direccion}">Direccion *</form:label></td>
					<td><form:input path="direccion"/></td>
					<td><form:errors path="direccion" cssClass="error"/></td>
				</tr>
				<tr>
					<td><form:label path="numero_telefono"
									value="${usuario.numero_telefono}">Telefono *</form:label></td>
					<td><form:input path="numero_telefono"/></td>
					<td><form:errors path="numero_telefono" cssClass="error"/></td>
				</tr>
				<tr>
					<td>
						<small>(*) Campos obligatorios</small>
					</td>
					<td><a href="${pageContext.request.contextPath}/my/usuario/userprofile.html"><input
							type="button" class="button alert" value="Cancelar"/></a></td>
					<td><input type="submit" class="button success" value="Guardar"/>
					</td>
				</tr>
			</table>
		</div>
	</form:form>
</div>
</body>
</html>