<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ page contentType="text/html; charset=utf-8" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<head>
    <title>WebEasyRent</title>

    <!-- ********************************* Included CSS Files (Compressed) ***************************************** -->
    <link rel="stylesheet" href="/static/css/foundation.css">
    <link rel="stylesheet" href="/static/css/main.css">
    <link rel="stylesheet" href="/static/css/app.css">
    <link rel="stylesheet" href="/static/css/ligature.css">
    <link rel="stylesheet" href="/static/fonts/foundation-icons.css">

    <!-- Google fonts -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300|Playfair+Display:400italic'
          rel='stylesheet' type='text/css'/>

    <%-- ***************************************** Java scripts ****************************************************--%>
    <script src="/static/js/modernizr.foundation.js"></script>
    <!-- Incluir JQuery -->
    <script src="/static/js/jquery.js" type="text/javascript"></script>
    <!-- Incluir JS especifico de esta página -->
    <script src="/static/js/index.js" type="text/javascript"></script>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>


    <!-- *********************************************** Header **************************************************## -->
    <header>
        <div class="twelve columns">
            <div class="logo" style="margin-left: 5px">
                <a href="${pageContext.request.contextPath}/index.jsp"><img src="/static/imgs/logoV2.png" alt=""/></a>
            </div>
            <div class="row">
                <ul id="menu-header" class="nav-bar horizontal">
                    <li class="" style="float:right"><i class="fi-torso" style="color: white"></i></a><t:logininfo/>
                    </li>
                </ul>
            </div>
        </div>
        <h2 class="welcome_text">Mis datos personales</h2>
    </header>
</head>
<body>
<!-- ************************************* Pagina principal link *********************************************** -->
<div class="row">
    <h2><a href="${pageContext.request.contextPath}/my/dashboard.html">Volver al menu principal</a></h2>
</div>

<div>
        <table>
            <tr>
                <td>NIE/DNI: ${usuario.id_nacional}</td>
            </tr>
            <tr>
                <td>Nombre: ${usuario.nombre}</td>
            </tr>
            <tr>
                <td>Apellido: ${usuario.apellido}</td>
            </tr>
            <tr>
                <td>Nick: ${usuario.usuario}</td>
            </tr>
            <tr>
                <td>Mi e-mail: ${usuario.email}</td>
            </tr>
            <tr>
                <td>Dirercción: ${usuario.direccion}</td>
            </tr>
            <tr>
                <td>Fecha de registro: ${usuario.fecha_registro}</td>
            </tr>
            <tr>
                <td>Teléfono: ${usuario.numero_telefono}</td>
            </tr>

            <tr>
                <td><a href="${usuario.id_usuario}/cambiarcontrasenya.html"><input type="button" class="button expand" value="Cambiar contrasenya" /></a>
                </td>
                <td><a href="updateusuario/${usuario.id_usuario}.html"><input type="button" class="button expand" value="Editar" /></a></td>
            </tr>
        </table>
</div>
</body>
</html>