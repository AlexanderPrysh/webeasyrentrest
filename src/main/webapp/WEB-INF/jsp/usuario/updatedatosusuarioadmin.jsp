<%@ page contentType="text/html; charset=utf-8" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<html> 
<head>
	<title>WebEasyRent</title>

	<!-- ********************************* Included CSS Files (Compressed) ***************************************** -->
	<link rel="stylesheet" href="/static/css/foundation.css">
	<link rel="stylesheet" href="/static/css/main.css">
	<link rel="stylesheet" href="/static/css/app.css">
	<link rel="stylesheet" href="/static/css/ligature.css">
	<link rel="stylesheet" href="/static/fonts/foundation-icons.css">

	<!-- Google fonts -->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300|Playfair+Display:400italic' rel='stylesheet' type='text/css'/>

	<%-- ***************************************** Java scripts ****************************************************--%>
	<script src="/static/js/modernizr.foundation.js"></script>
	<!-- Incluir JQuery -->
	<script src="/static/js/jquery.js" type="text/javascript"></script>
	<!-- Incluir JS especifico de esta página -->
	<script src="/static/js/index.js" type="text/javascript"></script>

	<script>
		$.datepicker.regional['es'] = {
			closeText: 'Cerrar',
			prevText: '<Ant',
			nextText: 'Sig>',
			currentText: 'Hoy',
			monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
			monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
			dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
			dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
			dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
			weekHeader: 'Sm',
			dateFormat: 'yyyy-mm-dd',
			firstDay: 1,
			isRTL: false,
			showMonthAfterYear: false,
			yearSuffix: ''
		};
		$.datepicker.setDefaults($.datepicker.regional['es']);
		$(function () {
			$(".fecha").datepicker();
		});
	</script>

	<!-- *********************************************** Header **************************************************## -->
	<header>
		<div class="twelve columns">
			<div class="logo" style="margin-left: 5px">
				<a href="${pageContext.request.contextPath}/index.jsp"><img src="/static/imgs/logoV2.png" alt=""/></a>
			</div>
			<div class="row">
				<ul id="menu-header" class="nav-bar horizontal">
					<li class="" style="float:right"><i class="fi-torso" style="color: white"></i></a><t:logininfo/></li>
				</ul>
			</div>
		</div>
		<h2 class="welcome_text">Modificar datos del usuario con ID "${usuario.id_usuario}"</h2>
	</header>
</head>
<body>
<div>
	<form:form method="post" modelAttribute="usuario">
		<div>
			<table>
				<tr>
					<td><form:label path="id_nacional" value="${usuario.id_nacional}">NIE/DNI *</form:label></td>
					<td><form:input path="id_nacional"/></td>
					<td><form:errors path="id_nacional" cssClass="error"/></td>
				</tr>
				<tr>
					<td><form:label path="nombre" value="${usuario.nombre}">Nombre *</form:label></td>
					<td><form:input path="nombre"/></td>
					<td><form:errors path="nombre" cssClass="error"/></td>
				</tr>
				<tr>
					<td><form:label path="apellido" value="${usuario.apellido}">Apellidos *</form:label></td>
					<td><form:input path="apellido"/></td>
					<td><form:errors path="apellido" cssClass="error"/></td>
				</tr>
				<tr>
					<td><form:label path="email" value="${usuario.email}">E-mail *</form:label></td>
					<td><form:input type="email" path="email"/></td>
					<td><form:errors path="email" cssClass="error"/></td>
				</tr>
				<tr>
					<td><form:label path="direccion" value="${usuario.direccion}">Direccion *</form:label></td>
					<td><form:input path="direccion"/></td>
					<td><form:errors path="direccion" cssClass="error"/></td>
				</tr>
				<tr>
					<td><form:label path="numero_telefono"
									value="${usuario.numero_telefono}">Telefono *</form:label></td>
					<td><form:input path="numero_telefono"/></td>
					<td><form:errors path="numero_telefono" cssClass="error"/></td>
				</tr>
				<tr>
					<td><Label>Rol *</label></td>
					<td><input path="rol" type="radio" name="rol" value="user" checked="${usuario.rol}">user
						<input path="rol" type="radio" name="rol" value="admin">admin
					</td>
					<td><form:errors path="rol" cssClass="error"/></td>
				</tr>
				<tr>
					<td><Label>Activo *</label></td>
					<td><input path="es_activo" type="radio" name="es_activo" value=true
							   checked="${usuario.es_activo}">true
						<input path="es_activo" type="radio" name="es_activo" value=false>false
					</td>
					<td><form:errors path="rol" cssClass="error"/></td>
				</tr>
				<tr>
					<td>
						<small>(*) Campos obligatorios</small>
					</td>
					<td><a href="${pageContext.request.contextPath}/my/usuario/listusuarios.html"><input
							type="button" class="button alert" value="Cancelar"/></a></td>
					<td><input type="submit" class="button success" value="Guardar"/></td>
				</tr>
			</table>
		</div>
	</form:form>
</div>
</body>
</html>