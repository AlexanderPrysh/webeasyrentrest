<%@ page contentType="text/html; charset=utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<html>
<head>
    <title>WebEasyRent</title>

    <!-- ********************************* Included CSS Files (Compressed) ***************************************** -->
    <link rel="stylesheet" href="/static/css/foundation.css">
    <link rel="stylesheet" href="/static/css/main.css">
    <link rel="stylesheet" href="/static/css/app.css">
    <link rel="stylesheet" href="/static/css/ligature.css">
    <link rel="stylesheet" href="/static/fonts/foundation-icons.css">

    <!-- Google fonts -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300|Playfair+Display:400italic'
          rel='stylesheet' type='text/css'/>

    <%-- ***************************************** Java scripts ****************************************************--%>
    <script src="/static/js/modernizr.foundation.js"></script>
    <!-- Incluir JQuery -->
    <script src="/static/js/jquery.js" type="text/javascript"></script>
    <!-- Incluir JS especifico de esta página -->
    <script src="/static/js/index.js" type="text/javascript"></script>


    <script>
        function borrar(id_servicio) {
            var r = confirm("¿¡Estas seguro que desea borrar el servicio!?");
            if (r == true) {
                var newURL = window.location.protocol + "//" + window.location.host + "/my/servicio/deleteservicio/" + id_servicio + ".html";
                location.href = newURL;
            }
        }
    </script>

    <!-- *********************************************** Header **************************************************## -->
    <header>
        <div class="twelve columns">
            <div class="logo" style="margin-left: 5px">
                <a href="${pageContext.request.contextPath}/index.jsp"><img src="/static/imgs/logoV2.png" alt=""/></a>
            </div>
            <div class="row">
                <ul id="menu-header" class="nav-bar horizontal">
                    <li class="" style="float:right"><i class="fi-torso" style="color: white"></i></a><t:logininfo/>
                    </li>
                </ul>
            </div>
        </div>
        <h2 class="welcome_text">Lista de Servicios</h2>
    </header>
</head>
<!--##############################################################################################################  -->
<body>
<!-- ************************************* Pagina principal link *********************************************** -->
<div class="row">
    <h2><a href="${pageContext.request.contextPath}/my/dashboard.html">Volver al menu principal</a></h2>
</div>

<div><a href="addservicio.html"><input type="button" class="button success" value="Nuevo servicio"/></a></div>
<div>
    <table class="table-propio">
        <tr>
            <th>ID servicio</th>
            <th>Nombre</th>
        </tr>
        <c:forEach items="${servicio}" var="servicio">
            <tr>
                <td class="col-md-3">${servicio.id_servicio}</td>
                <td class="col-md-3">${servicio.nombre}</td>

                <td class="col-md-3">
                    <a href="updateservicio/${servicio.id_servicio}.html"><input type="button" class="button expand"
                                                                                 value="Modificar"
                                                                                 style="width: 120px; height: 35px"/></a>
                    <input type="button" class="button alert" value="Borrar"
                           onclick="borrar('${servicio.id_servicio}')" style="width: 120px; height: 35px"/>
                </td>
            </tr>
        </c:forEach>
    </table>
</div>
</body>
</html>
