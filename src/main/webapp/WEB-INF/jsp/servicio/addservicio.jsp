<%@ page contentType="text/html; charset=utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>
<head>
    <title>WebEasyRent</title>

    <!-- ********************************* Included CSS Files (Compressed) ***************************************** -->
    <link rel="stylesheet" href="/static/css/foundation.css">
    <link rel="stylesheet" href="/static/css/main.css">
    <link rel="stylesheet" href="/static/css/app.css">
    <link rel="stylesheet" href="/static/css/ligature.css">
    <link rel="stylesheet" href="/static/fonts/foundation-icons.css">

    <!-- Google fonts -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300|Playfair+Display:400italic' rel='stylesheet' type='text/css'/>

    <%-- ***************************************** Java scripts ****************************************************--%>
    <script src="/static/js/modernizr.foundation.js"></script>
    <!-- Incluir JQuery -->
    <script src="/static/js/jquery.js" type="text/javascript"></script>
    <!-- Incluir JS especifico de esta página -->
    <script src="/static/js/index.js" type="text/javascript"></script>


    <script>
        $.datepicker.regional['es'] = {
            closeText: 'Cerrar',
            prevText: '<Ant',
            nextText: 'Sig>',
            currentText: 'Hoy',
            monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
            dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
            dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
            dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
            weekHeader: 'Sm',
            dateFormat: 'dd/mm/yy',
            firstDay: 1,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: ''
        };
        $.datepicker.setDefaults($.datepicker.regional['es']);
        $(function () {
            $(".fecha").datepicker();
        });
    </script>

    <!-- *********************************************** Header **************************************************## -->
    <header>
        <div class="twelve columns">
            <div class="logo" style="margin-left: 5px">
                <a href="${pageContext.request.contextPath}/index.jsp"><img src="/static/imgs/logoV2.png" alt=""/></a>
            </div>
            <div class="row">
                <ul id="menu-header" class="nav-bar horizontal">
                    <li class="" style="float:right"><i class="fi-torso" style="color: white"></i></a><t:logininfo/></li>
                </ul>
            </div>
        </div>
        <h2 class="welcome_text">Añadir nuevo servicio</h2>
    </header>
</head>
<body>
<%-- ******************************************* Anyadir nuevo servicio ******************************************* --%>
<div>
    <form:form method="post" modelAttribute="servicio">
        <div>
            <table>
                <tr>
                    <td><form:label path="nombre">Nombre de servicio *</form:label></td>
                    <td><form:input path="nombre"/></td>
                    <td><form:errors path="nombre" cssClass="error"/></td>
                </tr>
                <tr>
                    <td>
                        <small>(*) Campos obligatorios</small>
                    </td>
                    <td><a href="${pageContext.request.contextPath}/my/servicio/listservicios.html"><input
                            type="button" class="button alert" value="Cancelar"/></a></td>
                    <td><input type="submit" class="button success" value="Añadir servicio"/>
                    </td>
                </tr>
            </table>
        </div>
    </form:form>
</div>
</body>
</html>
