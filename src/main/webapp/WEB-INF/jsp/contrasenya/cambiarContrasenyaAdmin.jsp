<%@ page contentType="text/html; charset=utf-8" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>
<head>
	<title>WebEasyRent</title>

	<!-- ********************************* Included CSS Files (Compressed) ***************************************** -->
	<link rel="stylesheet" href="/static/css/foundation.css">
	<link rel="stylesheet" href="/static/css/main.css">
	<link rel="stylesheet" href="/static/css/app.css">
	<link rel="stylesheet" href="/static/css/ligature.css">
	<link rel="stylesheet" href="/static/fonts/foundation-icons.css">

	<!-- Google fonts -->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300|Playfair+Display:400italic' rel='stylesheet' type='text/css'/>

	<%-- ***************************************** Java scripts ****************************************************--%>
	<script src="/static/js/modernizr.foundation.js"></script>
	<!-- Incluir JQuery -->
	<script src="/static/js/jquery.js" type="text/javascript"></script>
	<!-- Incluir JS especifico de esta página -->
	<script src="/static/js/index.js" type="text/javascript"></script>

	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
	<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>

	<!-- *********************************************** Header **************************************************## -->
	<header>
		<div class="twelve columns">
			<div class="logo" style="margin-left: 5px">
				<a href="${pageContext.request.contextPath}/index.jsp"><img src="/static/imgs/logoV2.png" alt=""/></a>
			</div>
			<div class="row">
				<ul id="menu-header" class="nav-bar horizontal">
					<li class="" style="float:right"><i class="fi-torso" style="color: white"></i></a><t:logininfo/></li>
				</ul>
			</div>
		</div>
		<h2 class="welcome_text">Cambiar Contraseña del usuario "${usuariomod.usuario}"</h2>
	</header>
</head>
<body>
<div>
	<form:form method="post" modelAttribute="contrasenya">
		<div>
			<table>
				<tr>
					<td><form:label path="contrasenyaNueva">Contraseña nueva</form:label></td>
					<td><form:input path="contrasenyaNueva" type="password"/></td>
					<td><form:errors path="contrasenyaNueva" cssClass="error"/></td>
				</tr>
				<tr>
					<td><form:label path="contrasenyaRepetir">Repetir contraseña</form:label></td>
					<td><form:input path="contrasenyaRepetir" type="password"/></td>
					<td><form:errors path="contrasenyaRepetir" cssClass="error"/></td>
				</tr>
				<tr>
					<td><a href="${pageContext.request.contextPath}/my/usuario/listusuarios.html"><input
							type="button" class="button alert" value="Cancelar"/></a></td>
					<td><input type="submit" class="button success" value="Guardar"/>
					</td>
				</tr>
			</table>
		</div>
	</form:form>
</div>
</body>
</html>