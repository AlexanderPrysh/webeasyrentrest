<%@ page contentType="text/html; charset=utf-8" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>
<head>
	<title>WebEasyRent</title>

	<!-- ********************************* Included CSS Files (Compressed) ***************************************** -->
	<link rel="stylesheet" href="/static/css/foundation.css">
	<link rel="stylesheet" href="/static/css/main.css">
	<link rel="stylesheet" href="/static/css/app.css">
	<link rel="stylesheet" href="/static/css/ligature.css">
	<link rel="stylesheet" href="/static/fonts/foundation-icons.css">

	<!-- Google fonts -->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300|Playfair+Display:400italic'
		  rel='stylesheet' type='text/css'/>

	<%-- ***************************************** Java scripts ****************************************************--%>
	<script src="/static/js/modernizr.foundation.js"></script>
	<!-- Incluir JQuery -->
	<script src="/static/js/jquery.js" type="text/javascript"></script>
	<!-- Incluir JS especifico de esta página -->
	<script src="/static/js/index.js" type="text/javascript"></script>

	<!-- ######################## Header ######################## -->

	<header>
		<div class="twelve columns">
			<div class="logo" style="margin-left: 5px">
				<a href="${pageContext.request.contextPath}/index.jsp"><img src="/static/imgs/logoV2.png" alt=""/></a>
			</div>
			<div class="row">
				<ul id="menu-header" class="nav-bar horizontal">
					<li class="" style="float:right"><a href="login.html"><i class="fi-torso"></i> Login</a><t:logininfo/></li>
				</ul>
			</div>
		</div>
		<h2 class="welcome_text">Recuperar Contraseña</h2>
	</header>
</head>
<body>
<%--******************************************* Titulo ************************************************************ --%>
<div>
	<form:form method="post" modelAttribute="usuario">
		<div>
			<table>
				<tr>
					<td><form:label path="email">E-mail *</form:label></td>
					<td><form:input type="email" path="email"/></td>
					<td><form:errors path="email" cssClass="error"/></td>
				</tr>
				<tr>
					<td><a href="${pageContext.request.contextPath}/index.html"><input
							type="button" class="button alert" value="Cancelar" style="width: 100px; height: 35px"/></a></td>
					<td><input type="submit" class="button success" value="Recuperar" style="width: 100px; height: 35px"/>
					</td>
				</tr>
			</table>
		</div>
	</form:form>
</div>
</body>
</html>