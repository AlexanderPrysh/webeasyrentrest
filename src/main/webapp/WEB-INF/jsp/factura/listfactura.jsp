<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<!DOCTYPE HTML>
<html>
<head>
    <title>WebEasyRent</title>

    <!-- ********************************* Included CSS Files (Compressed) ***************************************** -->
    <link rel="stylesheet" href="/static/css/foundation.css">
    <link rel="stylesheet" href="/static/css/main.css">
    <link rel="stylesheet" href="/static/css/app.css">
    <link rel="stylesheet" href="/static/css/ligature.css">
    <link rel="stylesheet" href="/static/fonts/foundation-icons.css">

    <!-- Google fonts -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300|Playfair+Display:400italic'
          rel='stylesheet' type='text/css'/>

    <%-- ***************************************** Java scripts ****************************************************--%>
    <script src="/static/js/modernizr.foundation.js"></script>
    <!-- Incluir JQuery -->
    <script src="/static/js/jquery.js" type="text/javascript"></script>
    <!-- Incluir JS especifico de esta página -->
    <script src="/static/js/index.js" type="text/javascript"></script>


    <!-- *********************************************** Header **************************************************## -->
    <header>
        <div class="twelve columns">
            <div class="logo" style="margin-left: 5px">
                <a href="${pageContext.request.contextPath}/index.jsp"><img src="/static/imgs/logoV2.png" alt=""/></a>
            </div>
            <div class="row">
                <ul id="menu-header" class="nav-bar horizontal">
                    <li class="" style="float:right"><i class="fi-torso" style="color: white"></i></a><t:logininfo/>
                    </li>
                </ul>
            </div>
        </div>
        <c:if test="${usuario.rol == 'admin'}">
            <h2 class="welcome_text">Lista de Facturas</h2>
        </c:if>
        <c:if test="${usuario.rol == 'user'}">
            <h2 class="welcome_text">Mis facturas</h2>
        </c:if>
    </header>
</head>
<body>
<!-- ************************************* Menu principal link *********************************************** -->
<div class="row">
    <h2><a href="${pageContext.request.contextPath}/my/dashboard.html">Volver al menu principal</a></h2>
</div>
<div>
    <table class="table-propio">
        <tr>
            <th>Nº factura</th>
            <th>Nº de seguimiento</th>
            <th>Fecha factura</th>
            <th>IVA</th>
        </tr>
        <c:forEach items="${factura}" var="factura">
            <tr>
                <td class="col-md-9">${factura.num_factura}</td>
                <td class="col-md-9">${factura.num_seguimiento}</td>
                <td class="col-md-9">${factura.fecha_factura}</td>
                <td class="col-md-9">${factura.iva}</td>
            </tr>
        </c:forEach>
    </table>
</div>
</body>
</html>	
