<%@ page contentType="text/html; charset=utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<!DOCTYPE HTML>
<html>
<head>
    <title>WebEasyRent</title>

    <!-- ********************************* Included CSS Files (Compressed) ***************************************** -->
    <link rel="stylesheet" href="/static/css/foundation.css">
    <link rel="stylesheet" href="/static/css/main.css">
    <link rel="stylesheet" href="/static/css/app.css">
    <link rel="stylesheet" href="/static/css/ligature.css">
    <link rel="stylesheet" href="/static/fonts/foundation-icons.css">

    <!-- Google fonts -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300|Playfair+Display:400italic'
          rel='stylesheet' type='text/css'/>

    <%-- ***************************************** Java scripts ****************************************************--%>
    <script src="/static/js/modernizr.foundation.js"></script>
    <!-- Incluir JQuery -->
    <script src="/static/js/jquery.js" type="text/javascript"></script>
    <!-- Incluir JS especifico de esta página -->
    <script src="/static/js/index.js" type="text/javascript"></script>


<script> 
	function myFunction(id_propiedad, id_servicio) {
	    var r = confirm("¿¡Estas seguro que desea borrar el servicio!?");
	    if (r == true) {
	    	var newURL = window.location.protocol + "//" + window.location.host + "/my/propiedad/" + id_propiedad + "/deletepropservicio/" + id_servicio + ".html";
	    	location.href = newURL;
	    }
	}
</script>

    <!-- *********************************************** Header **************************************************## -->
    <header>
        <div class="twelve columns">
            <div class="logo" style="margin-left: 5px">
                <a href="${pageContext.request.contextPath}/index.jsp"><img src="/static/imgs/logoV2.png" alt=""/></a>
            </div>
            <div class="row">
                <ul id="menu-header" class="nav-bar horizontal">
                    <li class="" style="float:right"><i class="fi-torso" style="color: white"></i></a><t:logininfo/>
                    </li>
                </ul>
            </div>
        </div>
        <h2 class="welcome_text">Servicios de la Propiedad "${propiedad.titulo}"</h2>
    </header>
</head>
<body>
<!-- ************************************* Lista Propiedades link *********************************************** -->
<div class="row">
    <h2><a href="${pageContext.request.contextPath}/my/propiedad/listpropiedades.html">Volver a la lista de
        Propiedades</a></h2>
</div>
<c:if test="${usuario.rol == 'user'}">
    <div><a href="addpropservicio.html"><input type="button" class="button success" value="Nuevo servicio" /></a></div>
</c:if>

<div>
	<table class="table-propio">
		<tr>
			<th>Id. propiedad</th>
			<th>Id. servicio</th>
		</tr>
		<c:forEach items="${propservicios}" var="propservicios"> 
			<tr>
			    <td class="col-md-3">${propiedad.id_propiedad}</td>
				<td class="col-md-3">${propservicios.nombre}</td>
                <td class="col-md-3"> <input type="button" class="button alert" value="Borrar" style="width: 100px; height: 35px"
                                             onclick="myFunction('${propiedad.id_propiedad}','${propservicios.id_servicio}')" /></td>
            </tr>
		</c:forEach>
	</table>
	</div>

<!--##############################################################################################################  -->   	     

</body>
</html>