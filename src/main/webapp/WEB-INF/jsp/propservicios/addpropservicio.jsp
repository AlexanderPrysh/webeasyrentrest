<%@page contentType="text/html; charset=iso-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" />
    <title>WebEasyRent</title>

    <!-- ********************************* Included CSS Files (Compressed) ***************************************** -->
    <link rel="stylesheet" href="/static/css/foundation.css">
    <link rel="stylesheet" href="/static/css/main.css">
    <link rel="stylesheet" href="/static/css/app.css">
    <link rel="stylesheet" href="/static/css/ligature.css">
    <link rel="stylesheet" href="/static/fonts/foundation-icons.css">

    <!-- Google fonts -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300|Playfair+Display:400italic'
          rel='stylesheet' type='text/css'/>

    <%-- ***************************************** Java scripts ****************************************************--%>
    <script src="/static/js/modernizr.foundation.js"></script>
    <!-- Incluir JQuery -->
    <script src="/static/js/jquery.js" type="text/javascript"></script>
    <!-- Incluir JS especifico de esta p�gina -->
    <script src="/static/js/index.js" type="text/javascript"></script>


    <!-- *********************************************** Header **************************************************## -->
    <header>
        <div class="twelve columns">
            <div class="logo" style="margin-left: 5px">
                <a href="${pageContext.request.contextPath}/index.jsp"><img src="/static/imgs/logoV2.png" alt=""/></a>
            </div>
            <div class="row">
                <ul id="menu-header" class="nav-bar horizontal">
                    <li class="" style="float:right"><i class="fi-torso" style="color: white"></i></a><t:logininfo/>
                    </li>
                </ul>
            </div>
        </div>
        <h2 class="welcome_text">A�adir Servicio a la Propiedad "${propiedad.titulo}"</h2>
    </header>
</head>
<body>
    <form:form method="post" modelAttribute="propServicio">
        <div>
            <c:forEach items="${servicio}" var="servicio">
		        <input type="radio" name="id_servicio" value=${servicio.id_servicio}>${servicio.nombre}<br>
            </c:forEach>
        </div>
        <div>
            <a href="${pageContext.request.contextPath}/my/propiedad/${propiedad.id_propiedad}/listpropservicios.html">
                <input type="button" class="button alert" value="Cancelar" style="width: 100px; height: 35px"/></a></td>
            <input type="submit" class="button success" value="A�adir" style="width: 100px; height: 35px"/></td>
        </div>
    </form:form>
</body>
</html>
