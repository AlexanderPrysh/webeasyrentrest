<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE HTML>
<html>
<head>
	<title>WebEasyRent</title>

	<!-- ********************************* Included CSS Files (Compressed) ***************************************** -->
	<link rel="stylesheet" href="/static/css/foundation.css">
	<link rel="stylesheet" href="/static/css/main.css">
	<link rel="stylesheet" href="/static/css/app.css">
	<link rel="stylesheet" href="/static/css/ligature.css">
	<link rel="stylesheet" href="/static/fonts/foundation-icons.css">

	<!-- Google fonts -->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300|Playfair+Display:400italic'
		  rel='stylesheet' type='text/css'/>

	<%-- ***************************************** Java scripts ****************************************************--%>
	<script src="/static/js/modernizr.foundation.js"></script>
	<!-- Incluir JQuery -->
	<script src="/static/js/jquery.js" type="text/javascript"></script>
	<!-- Incluir JS especifico de esta página -->
	<script src="/static/js/index.js" type="text/javascript"></script>

	<script>
	function borrar(num_seguimiento) {
	    var r = confirm("¿¡Estas seguro que desea borrar la reserva!?");
	    if (r == true) {
	    	var newURL = window.location.protocol + "//" + window.location.host + "/my/reserva/deletereserva/" + num_seguimiento + ".html";
	    	location.href = newURL;
	    }
	}
	</script>
	<script>
		function confirmar(num_seguimiento, id_propiedad) {
			var r = confirm("¿¡Confirmar reserva!?");
			if (r == true) {
				var newURL = window.location.protocol + "//" + window.location.host + "/my/reserva/" + id_propiedad + "/confirmarreserva/" + num_seguimiento + ".html";
				location.href = newURL;
			}else{

			}
		}
	</script>
	<script type="text/javascript" src="/static/js/jspdf.min.js"></script>
	<script type="text/javascript">
		function genPDF(num_seguimiento,id_nacional,id_propiedad,fecha_reserva,fecha_confirmacion,num_personas,
				fecha_inicio,fecha_fin,coste_total) {

           var doc = new jsPDF();

            doc.setFontSize(30);
            doc.text(35,20,'-----------------FACTURA-----------------');
            doc.setFontSize(10);
            doc.text(20,30,'Numero de seguimiento: '+num_seguimiento);
            doc.text(20,40,'NIF/CIF/NIE: '+id_nacional);
            doc.text(20,50,'ID de propiedad: '+id_propiedad);
            doc.text(20,60,'Fecha reserva: '+fecha_reserva);
            doc.text(20,70,'Fecha confirmacion: '+fecha_confirmacion);
            doc.text(20,80,'Nº de personas: '+num_personas);
            doc.text(20,90,'Fecha inicio: '+fecha_inicio);
            doc.text(20,100,'Fecha fin: '+fecha_fin);
            doc.text(20,110,'Coste Total: '+coste_total+'€');

            doc.save('factura.pdf');

		}
	</script>


	<!-- *********************************************** Header **************************************************## -->
	<header>
		<div class="twelve columns">
			<div class="logo" style="margin-left: 5px">
				<a href="${pageContext.request.contextPath}/index.jsp"><img src="/static/imgs/logoV2.png" alt=""/></a>
			</div>
			<div class="row">
				<ul id="menu-header" class="nav-bar horizontal">
					<li class="" style="float:right"><i class="fi-torso" style="color: white"></i></a><t:logininfo/></li>
				</ul>
			</div>
		</div>
		<h2 class="welcome_text">Lista de Reservas de la Propiedad "${propiedad.titulo}"</h2>
	</header>
</head>
<body>
<div class="row">
	<h2><a href="${pageContext.request.contextPath}/my/propiedad/listpropiedades.html">Volver a la lista de
		Propiedades</a></h2>
</div>

	<div>
	<table class="table-propio">
		<tr>
			<th>Nº de seguiemiento</th>
			<th>DNI/NIF</th>
			<th>Id. propiedad</th>
			<th>Fecha reserva</th>
			<th>Fecha confirmacion</th>
			<th>Nº de personas</th>
			<th>Fecha inicio</th>
			<th>Fecha fin</th>
			<th>Coste total</th>
			<th>Estado</th>
		</tr>
		<c:forEach items="${reservaprop}" var="reserva">
			<tr>
				<td class="col-md-12">${reserva.num_seguimiento}</td>
				<td class="col-md-12">${reserva.id_nacional}</td>
				<td class="col-md-12">${reserva.id_propiedad}</td>
				<td class="col-md-12">${reserva.fecha_reserva}</td>
				<td class="col-md-12">${reserva.fecha_confirmacion}</td>
				<td class="col-md-12">${reserva.num_personas}</td>
				<td class="col-md-12">${reserva.fecha_inicio}</td>
				<td class="col-md-12">${reserva.fecha_fin}</td>
				<td class="col-md-12">${reserva.coste_total}</td>
				<td class="col-md-12">${reserva.estado}</td>
				<td class="col-md-12"><c:if test="${reserva.estado == 'PENDIENTE'}">
					<input type="button" value="Confirmar" class="button success"
						   onclick="confirmar('${reserva.num_seguimiento}', '${reserva.id_propiedad}')" style="width: 100px; height: 35px"/>
					</c:if>
					<a href="javascript:genPDF('${reserva.num_seguimiento}','${reserva.id_nacional}','${reserva.id_propiedad}',
				'${reserva.fecha_reserva}','${reserva.fecha_confirmacion}','${reserva.num_personas}','${reserva.fecha_inicio}','${reserva.fecha_fin}',
				'${reserva.coste_total}')"><input type="button" class="button success" value="Factura" style="width: 100px; height: 35px"/></a>
				<input type="button" class="button alert" value="Borrar" onclick="borrar('${reserva.num_seguimiento}')" style="width: 100px; height: 35px"/>
			</tr>
		</c:forEach>
	</table>
	</div>
	<%--<a class="button expand" href="addreserva.html">Añadir una reserva</a>--%>


<!--##############################################################################################################  -->   	     

 
   <%--<div class="footer">--%>
   	<%--<div class="container">--%>
   	 <%--<div class="footer_top">--%>
   	  <%----%>
	  <%--</div>--%>
	  <%--<div class="footer_grids">--%>
		  <%--<div class="footer-grid last_grid">--%>
			<%--<h4>Follow Us</h4>--%>
			<%--<ul class="footer_social wow fadeInLeft" data-wow-delay="0.4s">--%>
			  <%--<li><a href="">Ola Amijo<i class="fb"> </i> </a></li>--%>
			  <%--<li><a href="">Siguenos<i class="tw"> </i> </a></li>--%>
			  <%--<li><a href="">Nosotos te indicaremos el camino<i class="google"> </i> </a></li>--%>
			  <%--<li><a href=""><i class="u_tube"> </i> </a></li>--%>
		 	<%--</ul>--%>
		 	<%--<div class="copy wow fadeInRight" data-wow-delay="0.4s">--%>
              <%--<p>&copy; Template by <a href="http://w3layouts.com/" target="_blank">W3layouts</a> </p>--%>
	        <%--</div>--%>
		  <%--</div>--%>
		  <%--<div class="clearfix"> </div>--%>
	   <%--</div>--%>
      <%--</div>--%>
   <%--</div>--%>

 </body>
</html>	
