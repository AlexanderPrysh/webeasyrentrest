<%@ page contentType="text/html; charset=utf-8" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8"/>
    <!-- Included CSS Files (Compressed) -->
    <link rel="stylesheet" href="/static/css/foundation.css">
    <link rel="stylesheet" href="/static/css/main.css">
    <link rel="stylesheet" href="/static/css/app.css">

    <script src="/static/js/modernizr.foundation.js"></script>
    <link rel="stylesheet" href="/static/css/ligature.css">
    <link rel="stylesheet" href="/static/fonts/foundation-icons.css">

    <!-- Google fonts -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300|Playfair+Display:400italic'
          rel='stylesheet' type='text/css'/>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>

    <script>//muestra calendarios al pulsar campos de fechas
    $(function () {
        $(".datepicker").datepicker({dateFormat: 'yy-mm-dd'});
    });
    </script>

    <script>//al apretar el boton pagar muestra el div con el pago
    $(document).ready(function () {
        $("#buttonAnim").click(function () {
            $("#divPay").show({
                height: 'toggle'
            });
        });
    });
    </script>

    <script>//calcula el coste total de la reserva
    function multiplicar() {
        m1 = document.getElementById("num_personas").value;
        r = m1 *${propiedad.precio_diario};
        document.getElementById("coste_total").value = r;
    }
    </script>
    <script>//muestra los datos extra si es metodo de pago tarjeta y oculta con paypal
    $(document).ready(function () {
        $("#radioEle").click(function () {
            $("#datosCard").hide();
        });
        $("#radioEle2").click(function () {
            $("#datosCard").show();
        });
    });
    </script>

    <script type="text/javascript">//pone la fecha actual en los campos indicados al cargar la pagina
    $('document').ready(function () {
        var date = new Date().toJSON().slice(0, 10);
        $('#fecha_confirmacion').val(date)
        $('#fecha_reserva').val(date)
    })
    </script>

    <!-- *********************************************** Header **************************************************## -->
    <header>
        <div class="twelve columns">
            <div class="logo" style="margin-left: 5px">
                <a href="${pageContext.request.contextPath}/index.jsp"><img src="/static/imgs/logoV2.png" alt=""/></a>
            </div>
            <div class="row">
                <ul id="menu-header" class="nav-bar horizontal">
                    <li class="" style="float:right"><i class="fi-torso" style="color: white"></i></a><t:logininfo/>
                    </li>
                </ul>
            </div>
        </div>
        <h2 class="welcome_text">Reserva de la Propiedad "${propiedad.titulo}</h2>
    </header>
</head>
<body>
<div>
    <form:form method="post" modelAttribute="reserva">
        <div>
            <table>

                    <td>
                        <table>
                            <tr>
                                <td><p>DATOS DE LA RESERVA</p></td>
                            </tr>
                            <tr>
                                <td><form:label path="id_nacional">DNI/NIF</form:label></td>
                                <td><form:input path="id_nacional" value="${usuario.id_nacional}" readonly="true"
                                                style="background-color: #ddd"/></td>
                            </tr>
                            <tr>
                                <td><form:label path="id_propiedad">Id. propiedad</form:label></td>
                                <td><form:input path="id_propiedad" value="${propiedad.id_propiedad}" readonly="true"
                                                style="background-color: #ddd"/></td>
                            </tr>
                            <tr>
                                <td><form:label path="num_personas">Nº de personas</form:label></td>
                                <td><form:input path="num_personas" id="num_personas" onChange="multiplicar();"/></td>
                                <td><form:errors path="num_personas" cssClass="error"/></td>
                            </tr>
                            <tr>
                                <td><form:label path="fecha_inicio">Fecha inicio</form:label></td>
                                <td><form:input path="fecha_inicio" cssClass="datepicker start"/></td>
                                <td><form:errors path="fecha_inicio" cssClass="error"/></td>
                            </tr>
                            <tr>
                                <td><form:label path="fecha_fin">Fecha fin</form:label></td>
                                <td><form:input path="fecha_fin" cssClass="datepicker end"/></td>
                                <td><form:errors path="fecha_fin" cssClass="error"/></td>
                            </tr>
                            <tr>
                                <td><label>Precio Diario (eur)</label></td>
                                <td><input value="${propiedad.precio_diario}" readonly=""
                                           style="background-color: #ddd"/></td>
                            </tr>
                            <tr>
                                <td><form:label path="coste_total">Coste total (eur)</form:label></td>
                                <td><form:input readonly="true" path="coste_total" id="coste_total"
                                                style="background-color: #ddd"/></td>
                            </tr>

                            <tr>
                                <td><a href="${pageContext.request.contextPath}/my/reserva/listreservas.html"><input
                                        type="button" class="button alert" value="Cancelar"/></a></td>
                                <td><input id="buttonAnim" type="button" class="button expand" value="Pagar"/></td>
                            </tr>
                        </table>
                    </td>
                    <td>
                        <div>
                            <table>
                                <tr>
                                    <th>Periodos Libres</th>
                                </tr>
                                <c:forEach items="${fechalibre}" var="fecha">
                                    <tr>
                                        <td>${fecha}</td>
                                    </tr>
                                </c:forEach>
                            </table>
                        </div>
                    </td>
                    <td>

                        <div hidden id="divPay" class="boxed-content">
                            <div class="primary billing-form" role="main">
                                <article>
                                    <h2>Por favor elija un método de pago:</h2>
                                    <form onsubmit="alert('Si todos los datos introducidos son correctos, la reserva se habra realizado con exito.');">
                                        <div class="highlightable">
                                            <fieldset name="payment-type" class="payment-type highlight"
                                                      id="choose-payment-method" style="z-index:5">
                                                <div class="select-paypal hotspot"><label for="radioEle"
                                                                                          style="display:none">Radio</label><input
                                                        type="radio" name="payment-type" id="radioEle"
                                                        checked="checked">
                                                    <img src="https://www.paypalobjects.com/webstatic/mktg/logo/pp_cc_mark_37x23.jpg"
                                                         border="0" alt="PayPal Logo">
                                                </div>
                                                <div class="select-creditcard hotspot"><label for="radioEle2"
                                                                                              style="display:none">Radio</label><input
                                                        type="radio" name="payment-type" id="radioEle2">
                                                    <img src="/static/imgs/visa.PNG"/>
                                                </div>
                                            </fieldset>
                                        </div>
                                        <div id="datosCard" class="pay-with-credit" hidden>
                                            <h3>Datos de la tarjeta de crédito</h3>
                                            <fieldset name="card-info" class="card-info">
                                                <div class="name"><label>Nombre completo tomador de tarjeta</label><br/><input
                                                        type="text" name="name" value="Jane Doe" disabled
                                                        style="padding-left:10px"><br/></div>
                                                <div class="card-number"><label>Número de tarjeta</label><br/><input
                                                        type="text" name="card-number" value="5496 7335 7650 7707"
                                                        disabled style="padding-left:10px"></div>
                                                <div class="security-code"><label>Código CCV</label><br/><input
                                                        type="text" name="security-code" value="655" disabled
                                                        style="padding-left:10px"></div>
                                                <div class="exp dropdown"><label>Fecha caducidad:</label><br/><input
                                                        type="text" name="exp" class="month" value="01" disabled
                                                        style="margin-left:0px !important"><input type="text" name="exp"
                                                                                                  class="year"
                                                                                                  value="2020" disabled>
                                                </div>
                                            </fieldset>

                                        </div>
                                    </form>
                                </article>
                            </div>
                            <input type="submit" class="button success" onclick="this.form.submited=this.value;"
                                   value="Reservar">
                        </div>
                    </td>

            </table>
        </div>
    </form:form>
</div>
</body>
</html>
