<%@ page contentType="text/html; charset=utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<!DOCTYPE HTML>
<html>
<head>
    <title>WebEasyRent</title>

    <!-- ********************************* Included CSS Files (Compressed) ***************************************** -->
    <link rel="stylesheet" href="/static/css/foundation.css">
    <link rel="stylesheet" href="/static/css/main.css">
    <link rel="stylesheet" href="/static/css/app.css">
    <link rel="stylesheet" href="/static/css/ligature.css">
    <link rel="stylesheet" href="/static/fonts/foundation-icons.css">

    <!-- Google fonts -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300|Playfair+Display:400italic'
          rel='stylesheet' type='text/css'/>

    <%-- ***************************************** Java scripts ****************************************************--%>
    <script src="/static/js/modernizr.foundation.js"></script>
    <!-- Incluir JQuery -->
    <script src="/static/js/jquery.js" type="text/javascript"></script>
    <!-- Incluir JS especifico de esta página -->
    <script src="/static/js/index.js" type="text/javascript"></script>

    <script>
        function borrar(id_propiedad, id_nacional) {
            var r = confirm("¿¡Estas seguro que desea borrar la valoración!?");
            if (r == true) {
                var newURL = window.location.protocol + "//" + window.location.host + "/my/valoracion/deletevaloracion/" + id_propiedad + "/" + id_nacional + ".html";
                location.href = newURL;
            }
        }
    </script>

    <!-- *********************************************** Header **************************************************## -->
    <header>
        <div class="twelve columns">
            <div class="logo" style="margin-left: 5px">
                <a href="${pageContext.request.contextPath}/index.jsp"><img src="/static/imgs/logoV2.png" alt=""/></a>
            </div>
            <div class="row">
                <ul id="menu-header" class="nav-bar horizontal">
                    <li class="" style="float:right"><i class="fi-torso" style="color: white"></i></a><t:logininfo/></li>
                </ul>
            </div>
        </div>
        <c:if test="${usuario.rol == 'admin'}">
            <h2 class="welcome_text">Lista de Valoraciones</h2>
        </c:if>
        <c:if test="${usuario.rol == 'user'}">
            <h2 class="welcome_text">Mis valoraciones</h2>
        </c:if>
    </header>
</head>
<body>
<!-- ************************************* Menu principal link *********************************************** -->
<div class="row">
    <h2><a href="${pageContext.request.contextPath}/my/dashboard.html">Volver al menu principal</a></h2>
</div>
<div>
    <table class="table-propio">
        <tr>
            <th>ID propiedad</th>
            <th>ID nacional</th>
            <th>Voto</th>
            <th>Comentario</th>
        </tr>
        <c:forEach items="${valoracion}" var="valoracion">
            <tr>
                <td class="col-md-9">${valoracion.id_propiedad}</td>
                <td class="col-md-9">${valoracion.id_nacional}</td>
                <td class="col-md-9">${valoracion.voto}</td>
                <td class="col-md-9">${valoracion.comentario}</td>
                <td class="col-md-9"><input type="button" class="button alert" value="Borrar" style="width: 100px; height: 35px"
                                            onclick="borrar('${valoracion.id_propiedad}','${valoracion.id_nacional}')"/>
                </td>
            </tr>
        </c:forEach>
    </table>
</div>
</body>
</html>	
