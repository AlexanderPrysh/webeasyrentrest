<%@ page contentType="text/html; charset=utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<html>
<head>
    <title>WebEasyRent</title>

    <!-- ********************************* Included CSS Files (Compressed) ***************************************** -->
    <link rel="stylesheet" href="/static/css/foundation.css">
    <link rel="stylesheet" href="/static/css/main.css">
    <link rel="stylesheet" href="/static/css/app.css">
    <link rel="stylesheet" href="/static/css/ligature.css">
    <link rel="stylesheet" href="/static/fonts/foundation-icons.css">

    <!-- Google fonts -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300|Playfair+Display:400italic'
          rel='stylesheet' type='text/css'/>

    <%-- ***************************************** Java scripts ****************************************************--%>
    <script src="/static/js/modernizr.foundation.js"></script>
    <!-- Incluir JQuery -->
    <script src="/static/js/jquery.js" type="text/javascript"></script>
    <!-- Incluir JS especifico de esta página -->
    <script src="/static/js/index.js" type="text/javascript"></script>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>

    <script src="/static/js/images.js"></script>

    <script>
        function borrar(id_propiedad, nom_imagen) {
            var r = confirm("¿¡Estas seguro que desea borrar la imagen!?");
            if (r == true) {
                var newURL = window.location.protocol + "//" + window.location.host + "/my/propiedad/" + id_propiedad + "/deleteimagen/"
                        + nom_imagen + ".html";
                location.href = newURL;
            }
        }
    </script>

    <!-- *********************************************** Header **************************************************## -->
    <header>
        <div class="twelve columns">
            <div class="logo" style="margin-left: 5px">
                <a href="${pageContext.request.contextPath}/index.jsp"><img src="/static/imgs/logoV2.png" alt=""/></a>
            </div>
            <div class="row">
                <ul id="menu-header" class="nav-bar horizontal">
                    <li class="" style="float:right"><i class="fi-torso" style="color: white"></i></a><t:logininfo/>
                    </li>
                </ul>
            </div>
        </div>
        <h2 class="welcome_text">Lista de Imagenes de la Propiedad "${propiedad.titulo}</h2>
    </header>
</head>
<body>

<!-- ************************************* Pagina principal link *********************************************** -->
<div class="row">
    <h2><a href="${pageContext.request.contextPath}/my/propiedad/listpropiedades.html">Volver a la lista de
        Propiedades</a></h2>
</div>

<c:if test="${usuario.rol == 'user'}">
    <div>
        <form action="/services/file/upload?id_prop=${propiedad.id_propiedad}" method="post"
              enctype="multipart/form-data">
            <pro>
                Añadir imagenes : <input type="file" multiple="multiple" name="file" size="45"/>
                <input type="submit" id="upload" class="button success" value="Subir"/>
            </pro>
        </form>
    </div>
</c:if>
<div>
    <table class="table-propior">
        <tr>
            <th>Nombre imagen</th>
            <th>Id propiedad</th>
            <th>URL</th>
        </tr>
        <c:forEach items="${imagen}" var="imagen">
            <tr>
                <td class="col-md-4">${imagen.nom_imagen}</td>
                <td class="col-md-4">${imagen.id_propiedad}</td>
                <td class="col-md-4">
                    <img src="${imagen.url}" alt="prop_1a" name="grande" width="1000" height="777" id="grande"/></td>
                <td class="col-md-4">
                    <input type="button" class="button alert" value="Borrar" style="width: 100px; height: 35px"
                           onclick="borrar('${imagen.id_propiedad}', '${imagen.nom_imagen}')" />
                </td>
            </tr>
        </c:forEach>
    </table>
</div>
</body>
</html>