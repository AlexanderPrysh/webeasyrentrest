<%@ page contentType="text/html; charset=utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<html>
<head>
    <title>WebEasyRent</title>

    <!-- ********************************* Included CSS Files (Compressed) ***************************************** -->
    <link rel="stylesheet" href="/static/css/foundation.css">
    <link rel="stylesheet" href="/static/css/main.css">
    <link rel="stylesheet" href="/static/css/app.css">
    <link rel="stylesheet" href="/static/css/ligature.css">
    <link rel="stylesheet" href="/static/fonts/foundation-icons.css">

    <!-- Google fonts -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300|Playfair+Display:400italic'
          rel='stylesheet' type='text/css'/>

    <%-- ***************************************** Java scripts ****************************************************--%>
    <script src="/static/js/modernizr.foundation.js"></script>
    <!-- Incluir JQuery -->
    <script src="/static/js/jquery.js" type="text/javascript"></script>
    <!-- Incluir JS especifico de esta página -->
    <script src="/static/js/index.js" type="text/javascript"></script>

    <script>
        function borrar(id_propiedad) {
            var r = confirm("¿¡Estas seguro que desea borrar la propiedad!?");
            if (r == true) {
                var newURL = window.location.protocol + "//" + window.location.host + "/my/propiedad/deletepropiedad/" + id_propiedad + ".html";
                location.href = newURL;
            }
        }
    </script>
    <script>
        function desactivar(id_propiedad) {
            var r = confirm("¿¡Desactivar la propiedad!?");
            if (r == true) {
                var newURL = window.location.protocol + "//" + window.location.host + "/my/propiedad/desactivarpropiedad/" + id_propiedad + ".html";
                location.href = newURL;
            }
        }
    </script>
    <script>
        function reactivar(id_propiedad) {
            var r = confirm("¿¡Reactivar la propiedad!?");
            if (r == true) {
                var newURL = window.location.protocol + "//" + window.location.host + "/my/propiedad/reactivarpropiedad/" + id_propiedad + ".html";
                location.href = newURL;
            }
        }
    </script>

    <!-- *********************************************** Header **************************************************## -->
    <header>
        <div class="twelve columns">
            <div class="logo" style="margin-left: 5px">
                <a href="${pageContext.request.contextPath}/index.jsp"><img src="/static/imgs/logoV2.png" alt=""/></a>
            </div>
            <div class="row">
                <ul id="menu-header" class="nav-bar horizontal">
                    <li class="" style="float:right"><i class="fi-torso" style="color: white"></i></a><t:logininfo/></li>
                </ul>
            </div>
        </div>
        <h2 class="welcome_text">Lista de todas las propiedades</h2>
    </header>
</head>
<body>
<!-- ************************************* Menu principal link *********************************************** -->
<div class="row">
    <h2><a href="${pageContext.request.contextPath}/my/dashboard.html">Volver al menu principal</a></h2>
</div>
<%-- ******************************************** Lista de propiedades ******************************************** --%>

<div>
    <table class="table-propio">
        <tr>
            <th>ID propiedad</th>
            <th>Propietario</th>
            <th>Titulo</th>
            <th>Tipo propiedad</th>
            <th>Calle</th>
            <th>Ciudad</th>
            <th>Es activo</th>
        </tr>
        <c:forEach items="${propiedad}" var="propiedad">
            <tr>
                <td class="col-md-9-propadmin"><a href="}${propiedad.id_propiedad}.html">${propiedad.id_propiedad}</a>
                </td>
                <td class="col-md-9-propadmin">${propiedad.id_nacional}</td>
                <td class="col-md-9-propadmin">${propiedad.titulo}</td>
                <td class="col-md-9-propadmin">${propiedad.tipo_propiedad}</td>
                <td class="col-md-9-propadmin">${propiedad.calle}
                    nº ${propiedad.numero} ${propiedad.planta}, ${propiedad.ciudad}</td>
                <td class="col-md-9-propadmin">${propiedad.ciudad}</td>
                <td class="col-md-9-propadmin">${propiedad.es_activo}</td>
                <td class="col-md-9-propadmin">
                    <c:choose>
                        <c:when test="${propiedad.es_activo == 'false'}">
                            <input type="button" class="button success" value="Reactivar"
                                   onclick="reactivar('${propiedad.id_propiedad}')" style="width: 100px; height: 35px"/>
                        </c:when>
                        <c:when test="${propiedad.es_activo == 'true'}">
                            <input type="button" class="button alert" value="Desactivar"
                                   onclick="desactivar('${propiedad.id_propiedad}')"
                                   style="width: 100px; height: 35px"/>
                        </c:when>
                    </c:choose>
                    <input type="button" class="button alert" value="Borrar"
                           onclick="borrar('${propiedad.id_propiedad}')" style="width: 100px; height: 35px"/>

                </td>
                <td class="col-md-9">
                    <a href="${propiedad.id_propiedad}/periodos.html"><input type="button" class="button success"
                                                                             value="Periodos"
                                                                             style="width: 110px; height: 35px"/></a>
                    <a href="${propiedad.id_propiedad}/imagenes.html"><input type="button" class="button success"
                                                                             value="Imagenes"
                                                                             style="width: 110px; height: 35px"/></a>
                    <a href="${propiedad.id_propiedad}/listpropservicios.html"><input type="buton"
                                                                                      class="button success"
                                                                                      value="Servicios"
                                                                                      style="width: 110px; height: 35px"/></a>
                    <a href="${propiedad.id_propiedad}/listreservaprop.html"><input type="buton" class="button success"
                                                                                    value="Reservas"
                                                                                    style='width:110px; height:35px;'/></a>
                </td>
            </tr>
        </c:forEach>
    </table>
</div>
</body>
</html>