<%@ page contentType="text/html; charset=utf-8" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<html> 
<head>
	<title>WebEasyRent</title>

	<!-- ********************************* Included CSS Files (Compressed) ***************************************** -->
	<link rel="stylesheet" href="/static/css/foundation.css">
	<link rel="stylesheet" href="/static/css/main.css">
	<link rel="stylesheet" href="/static/css/app.css">
	<link rel="stylesheet" href="/static/css/ligature.css">
	<link rel="stylesheet" href="/static/fonts/foundation-icons.css">

	<!-- Google fonts -->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300|Playfair+Display:400italic' rel='stylesheet' type='text/css'/>

	<%-- ***************************************** Java scripts ****************************************************--%>
	<script src="/static/js/modernizr.foundation.js"></script>
	<!-- Incluir JQuery -->
	<script src="/static/js/jquery.js" type="text/javascript"></script>
	<!-- Incluir JS especifico de esta página -->
	<script src="/static/js/index.js" type="text/javascript"></script>

	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
	<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>

	<!-- *********************************************** Header **************************************************## -->
	<header>
		<div class="twelve columns">
			<div class="logo" style="margin-left: 5px">
				<a href="${pageContext.request.contextPath}/index.jsp"><img src="/static/imgs/logoV2.png" alt=""/></a>
			</div>
			<div class="row">
				<ul id="menu-header" class="nav-bar horizontal">
					<li class="" style="float:right"><i class="fi-torso" style="color: white"></i></a><t:logininfo/></li>
				</ul>
			</div>
		</div>
		<h2 class="welcome_text">Modificar datos de la Propiedad "${propiedad.titulo}"</h2>
	</header>
</head>
<body>

<%-- ************************************* Anyadir nueva propiedad ************************************************ --%>
<div>
	<form:form method="post" modelAttribute="propiedad">
		<div>
			<table>
					<%-- * Aplico el error de propiedad duplicada en la path id_nacional (no tiene datos) * --%>
				<tr><h2><form:errors path="id_nacional" cssClass="error"/></h2></tr>
				<tr>
					<td><form:label path="titulo">Titulo *</form:label></td>
					<td><form:input path="titulo"/></td>
					<td><form:errors path="titulo" cssClass="error"/></td>
				</tr>
				<tr>
					<td><form:label path="descripcion">descripcion</form:label></td>
					<td><form:textarea path="descripcion"/></td>
					<td><form:errors path="descripcion" cssClass="error"/></td>
				</tr>
				<tr>
					<td><form:label path="tipo_propiedad">Tipo propiedad *</form:label></td>
					<td><form:input path="tipo_propiedad"/></td>
					<td><form:errors path="tipo_propiedad" cssClass="error"/></td>
				</tr>
				<tr>
					<td><form:label path="capacidad">Capacidad *</form:label></td>
					<td><form:input path="capacidad"/></td>
					<td><form:errors path="capacidad" cssClass="error"/></td>
				</tr>
				<tr>
					<td><form:label path="num_habitaciones">Num habitaciones *</form:label></td>
					<td><form:input path="num_habitaciones"/></td>
					<td><form:errors path="num_habitaciones" cssClass="error"/></td>
				</tr>
				<tr>
					<td><form:label path="num_banyos">Num banyos *</form:label></td>
					<td><form:input path="num_banyos"/></td>
					<td><form:errors path="num_banyos" cssClass="error"/></td>
				</tr>
				<tr>
					<td><form:label path="num_camas">Num camas *</form:label></td>
					<td><form:input path="num_camas"/></td>
					<td><form:errors path="num_camas" cssClass="error"/></td>
				</tr>
				<tr>
					<td><form:label path="metros_cuadrados">Metros cuadrados *</form:label></td>
					<td><form:input path="metros_cuadrados"/></td>
					<td><form:errors path="metros_cuadrados" cssClass="error"/></td>
				</tr>
				<tr>
					<td><form:label path="calle">Calle *</form:label></td>
					<td><form:input path="calle"/></td>
					<td><form:errors path="calle" cssClass="error"/></td>
				</tr>
				<tr>
					<td><form:label path="numero">Numero *</form:label></td>
					<td><form:input path="numero"/></td>
					<td><form:errors path="numero" cssClass="error"/></td>
				</tr>
				<tr>
					<td><form:label path="planta">Planta *</form:label></td>
					<td><form:input path="planta"/></td>
					<td><form:errors path="planta" cssClass="error"/></td>
				</tr>
				<tr>
					<td><form:label path="ciudad">Ciudad *</form:label></td>
					<td><form:input path="ciudad"/></td>
					<td><form:errors path="ciudad" cssClass="error"/></td>
				</tr>
				<tr>
					<td><form:label path="precio_diario">Precio diario *</form:label></td>
					<td><form:input path="precio_diario"/></td>
					<td><form:errors path="precio_diario" cssClass="error"/></td>
				</tr>
				<tr>
					<td>
						<small>(*) Campos obligatorios</small>
					</td>
					<td><a
							href="${pageContext.request.contextPath}/my/propiedad/listpropiedades.html"><input
							type="button" class="button alert" value="Cancelar"/></a></td>
					<td><input type="submit" class="button success" value="Guardar"
							   style="align-items: center;"/></td>
				</tr>
			</table>
		</div>
	</form:form>
</div>
</body>
</html>