<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<!DOCTYPE html>
<head>
  <title>WebEasyRent</title>

  <!-- ********************************* Included CSS Files (Compressed) ***************************************** -->
  <link rel="stylesheet" href="/static/css/foundation.css">
  <link rel="stylesheet" href="/static/css/main.css">
  <link rel="stylesheet" href="/static/css/app.css">
  <link rel="stylesheet" href="/static/css/ligature.css">
  <link rel="stylesheet" href="/static/fonts/foundation-icons.css">

  <!-- Google fonts -->
  <link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300|Playfair+Display:400italic'
        rel='stylesheet' type='text/css'/>

  <%-- ***************************************** Java scripts ****************************************************--%>
  <script src="/static/js/modernizr.foundation.js"></script>
  <!-- Incluir JQuery -->
  <script src="/static/js/jquery.js" type="text/javascript"></script>
  <!-- Incluir JS especifico de esta página -->
  <script src="/static/js/index.js" type="text/javascript"></script>

  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
  <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>


  <!-- *********************************************** Header **************************************************## -->
  <header>
    <div class="twelve columns">
      <div class="logo" style="margin-left: 5px">
        <a href="${pageContext.request.contextPath}/index.jsp"><img src="/static/imgs/logoV2.png" alt=""/></a>
      </div>
      <div class="row">
        <ul id="menu-header" class="nav-bar horizontal">
          <li class="" style="float:right"><i class="fi-torso" style="color: white"></i></a><t:logininfo/>
          </li>
        </ul>
      </div>
    </div>
    <h2 class="welcome_text">Menu Principal del Administrador</h2>
  </header>
</head>
<body>

<section class="section_main">

  <div class="col-md-1 text-center" style="padding-left: 0px;">
    <div class="col-md-1" style="padding-left: 0px;">
      <div class="container-fluid">
        <div style="margin-top: 30px">
          <table style="width: 90%">
            <tr>
              <td>
                <div class="col-md-1">
                  <div class="theme-forum" onclick="location.href='../my/usuario/userprofile.html'">
                    <div class="theme-forum-title">
                      <a href="../my/usuario/userprofile.html" class="linkforum">Datos personales</a>
                    </div>
                  </div>
                </div>
              </td>
            </tr>
            <tr>
              <td>
                <div class="col-md-1">
                  <div class="theme-forum" onclick="location.href='../my/usuario/listusuarios.html'">
                    <div class="theme-forum-title">
                      <a href="../my/usuario/listusuarios.html" class="linkforum">Lista usuarios</a>
                    </div>
                  </div>
                </div>
              </td>
              <td></td>
              <td>
                <div class="col-md-1">
                  <div class="theme-forum" onclick="location.href='../my/propiedad/listpropiedades.html'">
                    <div class="theme-forum-title">
                      <a href="../my/propiedad/listpropiedades.html" class="linkforum">Lista propiedades</a>
                    </div>
                  </div>
                </div>
              </td>
              <td></td>
              <td>
                <div class="col-md-1">
                  <div class="theme-forum" onclick="location.href='../my/servicio/listservicios.html'">
                    <div class="theme-forum-title">
                      <a href="../my/servicio/listservicios.html" class="linkforum">Lista sevicios</a>
                    </div>
                  </div>
                </div>
              </td>
              <td></td>
              <td>
                <div class="col-md-1">
                  <div class="theme-forum" onclick="location.href='../my/reserva/listreservas.html'">
                    <div class="theme-forum-title">
                      <a href="../my/reserva/listreservas.html" class="linkforum">Lista Reservas</a>
                    </div>
                  </div>
                </div>
              </td>
              <td></td>
              <td>
                <div class="col-md-1">
                  <div class="theme-forum" onclick="location.href='../my/factura/listfacturas.html'">
                    <div class="theme-forum-title">
                      <a href="../my/factura/listfacturas.html" class="linkforum">Lista facturas</a>
                    </div>
                  </div>
                </div>
              </td>
              <td></td>
              <td>
                <div class="col-md-1">
                  <div class="theme-forum" onclick="location.href='../my/valoracion/listvaloraciones.html'">
                    <div class="theme-forum-title">
                      <a href="../my/valoracion/listvaloraciones.html" class="linkforum">Lista valoraciones</a>
                    </div>
                  </div>
                </div>
              </td>
            </tr>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>

</body>
</html>