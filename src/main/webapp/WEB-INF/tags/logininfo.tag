<%-- <%@ tag language="java" pageEncoding="ISO-8859-1"%>--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!-- La sessio esta disponible automaticament en l'objecte "session" -->
<c:set var="usuario" scope="request" value='${session.getAttribute("usuario")}'/>
<a class="loggeduser">
<c:choose>
	<c:when test='${usuario == null}'>
		<a href="${pageContext.request.contextPath}/login.html" style="text-decoration:underline"> Entrar</a>
		/
		<a href="${pageContext.request.contextPath}/addusuario.html" style="text-decoration:underline">Registrar</a>
	</c:when>

	<c:otherwise>
		Autentificado como <a href="${pageContext.request.contextPath}/my/usermenu.html">"${usuario.usuario}"</a>
		/
		<a href="${pageContext.request.contextPath}/logout.html" style="text-decoration: underline">Salir</a>
	</c:otherwise>
</c:choose>
</a>
</p>