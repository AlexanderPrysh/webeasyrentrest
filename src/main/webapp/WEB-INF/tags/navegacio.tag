<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags"%>
<nav class="navbar navbar-default">
    <div class="container">
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li> <a href="${pageContext.request.contextPath}/index2.jsp">Pagina principal</a></li>
                <li> <a href="${pageContext.request.contextPath}/nadador/listnadadors.html">Gestio de nadadors</a></li>
                <li> <a href="${pageContext.request.contextPath}/user/listusers.html">Area Privada</a></li>
            </ul>
        </div>
    </div>
</nav>