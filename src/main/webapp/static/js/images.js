$( document ).ready(function() {
    $("#upload").click(function () {
        var form = $("#files").get(0)

        $.ajax({
            url: "/services/file/upload",
            type: "POST",
            enctype:"multipart/form-data",
            data :  new FormData(form),
            success: function(data, textStatus, jqXHR)
            {
                alert("All Files Have Been Uploaded ");
            },
            error: function (jqXHR, textStatus, errorThrown)
            {

            }
        });
    })
});
