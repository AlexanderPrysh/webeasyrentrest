$( document ).ready(function() {
    $("#search-button").click(function () {
        $("#lista-pisos").html("<tr class='row'>"+
            "<th>Titulo</th>"+
            "<th>Tipo propiedad</th>"+
            "<th>Capacidad</th>"+
            "<th>Numero habitaciones</th>"+
            "<th>Metros cuadrados</th>"+
            "<th>Ciudad</th>"+
            "<th>Precio diario</th>"+
            "<th>Descripcion</th>"+
            "</tr>");
        $.ajax({
            url: "/services/search?tipo_prop=" + $("#tipo_prop").val() +"&ciudad=" + $("#ciudad").val()
            +"&precioMin=" + $("#precioMin").val()+"&precioMax=" + $("#precioMax").val()
            +"&num_hab_min=" + $("#num_hab_min").val() +"&num_hab_max=" + $("#num_hab_max").val()
            +"&metros_min=" + $("#metros_min").val()+"&metros_max=" + $("#metros_max").val()
            +"&descripcion=" + $("#descripcion").val(),
            type: "GET",
            success: function (data) {
                $.each(data, function (index, element) {
                    $("#lista-pisos").append("<tr class='row'>"+
                                    "<td class='col-md-9'><a href='../showpropiedad/"+element.id_propiedad+".html'>"+element.titulo+"</a></td>"+
                                    "<td class='col-md-9'>"+element.tipo_propiedad+"</td>"+
                                    "<td class='col-md-9'>"+element.capacidad+"</td>"+
                                    "<td class='col-md-9'>"+element.num_habitaciones+"</td>"+
                                    "<td class='col-md-9'>"+element.metros_cuadrados+"</td>"+
                                    "<td class='col-md-9'>"+element.ciudad+"</td>"+
                                    "<td class='col-md-9'>"+element.precio_diario+"</td>"+
                                    "<td class='col-md-9'>"+element.descripcion+"</td>"+
                                    "</tr>")
                });
            }
        });
    })
});