$(document).ready(function () {
    //Funcion del boton ir arriba
    $('.to-top').click(function(){
        $('body, html').animate({
            scrollTop: '0px'
        }, 300);
    });

    $(window).scroll(function(){
        if( $(this).scrollTop() > 0 ){
            $('.to-top').slideDown(300);
        } else {
            $('.to-top').slideUp(300);
        }
    });

    //lo de aquí dentro se ejecutará cuando se haya cargado todo el css.

    //Ejemplo de llamada a un servicio rest
    setInterval(function () {
        $("#ultimos").html("")
        $.ajax({
            url: "/services/hello/super",
            type: "GET",
            success: function (data) {

                    $("#ultimos").append(' <div class="box photo col2" style="padding: 0">' +
                        ' <a href="product-single.html"><img src="/static/imgs/house_demo.jpg" alt="desc"></a>' +
                        '<p >' +
                        '<a href="product-single.html">Mas Info</a>' +
                        '</p>' +
                        '</div>')

            }
        })
    }, 5000)

});