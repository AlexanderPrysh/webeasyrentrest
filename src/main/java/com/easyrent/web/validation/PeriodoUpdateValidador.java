package com.easyrent.web.validation;

import com.easyrent.web.dao.PeriodoDaoTemp;
import com.easyrent.web.domain.Periodo;
import com.ibm.icu.text.SimpleDateFormat;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

public class PeriodoUpdateValidador implements Validator {

    private PeriodoDaoTemp periodoDao;
    private int id_propiedad;
    private Periodo periodoOriginal;

    public PeriodoUpdateValidador(PeriodoDaoTemp periodoDao, int id_propiedad, Periodo periodoOriginal) {
        this.periodoDao = periodoDao;
        this.id_propiedad = id_propiedad;
        this.periodoOriginal = periodoOriginal;
    }

    @Override
    public boolean supports(Class<?> cls) {
        return Periodo.class.equals(cls);
    }

    @Override
    public void validate(Object obj, Errors errors) {
        Periodo periodo = (Periodo) obj;

        // Obtengo la fecha actual ************************************************************************************
        Date ahora = new Date();
        SimpleDateFormat formateador = new SimpleDateFormat("yyyy-MM-dd");
        String fecha = formateador.format(ahora);
        Date fechaActual = null;

        try {
            fechaActual = formateador.parse(fecha);
        } catch (ParseException e) {
        }
        // ************************************************************************************************************

        // Comprobacion del campo FECHA INICIO
        if (periodo.getInicio() == null) {
            errors.rejectValue("inicio", "obligatorio", "Debe seleccionar una fecha de inicio.");
        } else if (fechaActual != null && periodo.getInicio().before(fechaActual)) {
            errors.rejectValue("inicio", "obligatorio", "La fecha de inicio no puede ser anterior a la fecha actual.");
        }

        // Comprobacion del campo FECHA FIN
        if (periodo.getFin() == null) {
            errors.rejectValue("fin", "obligatorio", "Debe seleccionar una fecha final.");
        } else if (periodo.getInicio() != null &&
                (periodo.getFin().before(periodo.getInicio()))) {
            errors.rejectValue("fin", "obligatorio", "La fecha final no puede ser anterior a la fecha de inicio.");
        }

        // Compruebo que no hay un periodo con estas fechas.
        List<Periodo> p = periodoDao.getPeriodoPropiedad(id_propiedad);

        if (periodo.getInicio() != null && periodo.getFin() != null) {
            for (Periodo i : p) {
                if (periodo.getId_periodo() != i.getId_periodo()) {
                    if ((periodo.getInicio().before(i.getFin()) || periodo.getInicio().equals(i.getFin())) &&
                            (periodo.getInicio().after(i.getInicio()) || periodo.getInicio().equals(i.getInicio()))) {
                        errors.rejectValue("inicio", "obligatorio", "Ya hay un periodo con esta fecha. Revise sus periodos.");
                        break;
                    } else if ((periodo.getFin().equals(i.getInicio()) || periodo.getFin().after(i.getInicio())) &&
                            (periodo.getFin().before(i.getFin()) || periodo.getFin().equals(i.getFin()))) {
                        errors.rejectValue("fin", "obligatorio", "Ya hay un periodo con esta fecha. Revise sus periodos.");
                        break;
                    } else if (periodo.getInicio().before(i.getInicio()) && periodo.getFin().after(i.getFin())) {
                        errors.rejectValue("inicio", "obligatorio", "Ya hay un periodo con esta fecha. Revise sus periodos.");
                        errors.rejectValue("fin", "obligatorio", "Ya hay un periodo con esta fecha. Revise sus periodos.");
                        break;
                    }
                } else {
                    if (periodo.getId_periodo() == i.getId_periodo()
                            && (!periodo.getInicio().equals(i.getInicio()) || !periodo.getFin().equals(i.getFin()))) {
                        if (periodo.getId_periodo() != i.getId_periodo()) {
                            if ((periodo.getInicio().before(i.getFin()) || periodo.getInicio().equals(i.getFin())) &&
                                    (periodo.getInicio().after(i.getInicio()) || periodo.getInicio().equals(i.getInicio()))) {
                                errors.rejectValue("inicio", "obligatorio", "Ya hay un periodo con esta fecha. Revise sus periodos.");
                                break;
                            } else if ((periodo.getFin().equals(i.getInicio()) || periodo.getFin().after(i.getInicio())) &&
                                    (periodo.getFin().before(i.getFin()) || periodo.getFin().equals(i.getFin()))) {
                                errors.rejectValue("fin", "obligatorio", "Ya hay un periodo con esta fecha. Revise sus periodos.");
                                break;
                            } else if (periodo.getInicio().before(i.getInicio()) && periodo.getFin().after(i.getFin())) {
                                errors.rejectValue("inicio", "obligatorio", "Ya hay un periodo con esta fecha. Revise sus periodos.");
                                errors.rejectValue("fin", "obligatorio", "Ya hay un periodo con esta fecha. Revise sus periodos.");
                                break;
                            }
                        }
                    }
                }
            }
        }
    }
}