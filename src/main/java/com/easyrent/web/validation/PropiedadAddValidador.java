package com.easyrent.web.validation;

import com.easyrent.web.dao.PropiedadDaoTemp;
import com.easyrent.web.domain.Propiedad;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

public class PropiedadAddValidador implements Validator {

    PropiedadDaoTemp propiedadDao;

    public PropiedadAddValidador(PropiedadDaoTemp propiedadDao) { this.propiedadDao = propiedadDao; }

    @Override
    public boolean supports(Class<?> cls) {
        return Propiedad.class.equals(cls);
    }

    @Override
    public void validate(Object obj, Errors errors) {

        Propiedad propiedad = (Propiedad) obj;

        // Comprobacion del campo TITULO.
        if (propiedad.getTitulo().trim().equals("")) {
            errors.rejectValue("titulo", "obligatorio", "Debe introducir un titulo");
        }

        // Comprobacion del campo TIPO_PROPIEDAD.
        if (propiedad.getTipo_propiedad().trim().equals("")) {
            errors.rejectValue("tipo_propiedad", "obligatorio", "Debe indicar el tipo de la propiedad");
        }

        // Comprobacion del campo CAPACIDAD.
        if (propiedad.getCapacidad() <= 0) {
            errors.rejectValue("capacidad", "obligatorio", "Ha de ser mayor que 0");
        }

        // Comprobacion del campo NUM_HABITACIONES.
        if (propiedad.getNum_habitaciones() <= 0) {
            errors.rejectValue("num_habitaciones", "obligatorio", "Ha de ser mayor que 0");
        }

        // Comprobacion del campo NUM_BANYOS.
        if (propiedad.getNum_banyos() <= 0) {
            errors.rejectValue("num_banyos", "obligatorio", "Ha de ser mayor que 0");
        }

        // Comprobacion del campo NUM_CAMAS.
        if (propiedad.getNum_camas() < 0) {
            errors.rejectValue("num_camas", "obligatorio", "Ha de ser un número positivo");
        }

        // Comprobacion del campo METROS_CUADRADOS.
        if (propiedad.getMetros_cuadrados() <= 0) {
            errors.rejectValue("metros_cuadrados", "obligatorio", "Ha de ser mayor que 0");
        }

        // Comprobacion del campo CALLE.
        if (propiedad.getCalle().trim().equals("")) {
            errors.rejectValue("calle", "obligatorio", "Debe introducir la dirección de la propiedad");
        }

        // Comprobacion del campo NUMERO.
        if (propiedad.getNumero() <= 0) {
            errors.rejectValue("numero", "obligatorio", "Debe ser un número válido. Mayor que 0");
        }

        // Comprobacion del campo PLANTA.
        if (propiedad.getPlanta() <= 0) {
            errors.rejectValue("planta", "obligatorio", "Debe ser un número válido. Mayor que 0");
        }

        // Comprobacion del campo CIUDAD.
        if (propiedad.getCiudad().trim().equals("")) {
            errors.rejectValue("ciudad", "obligatorio", "Debe introducir el nombre de la ciudad");
        }

        // Comprobacion del campo PRECIO_DIARIO.
        if (propiedad.getPrecio_diario() < 0) {
            errors.rejectValue("precio_diario", "obligatorio", "Ha de ser un número positivo");
        }

        // Compruebo que no hay ninguna propiedad registrada con la misma direccion completa.
        if (propiedadDao.getPropiedadDireccion(propiedad.getCalle(), propiedad.getNumero(), propiedad.getPlanta(),
                propiedad.getCiudad()) != null)
            errors.rejectValue("id_nacional", "obligatorio", "Ya hay una propiedad registrada con esta direccion.");
    }
}
