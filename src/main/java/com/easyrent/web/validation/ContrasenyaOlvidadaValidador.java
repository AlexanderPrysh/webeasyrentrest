package com.easyrent.web.validation;

import com.easyrent.web.dao.UsuarioDaoTemp;
import com.easyrent.web.domain.Usuario;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

public class ContrasenyaOlvidadaValidador implements Validator {

    private UsuarioDaoTemp usuarioDao;

    public ContrasenyaOlvidadaValidador(UsuarioDaoTemp usuarioDao) {
        this.usuarioDao = usuarioDao;
    }

    @Override
    public boolean supports(Class<?> cls) {return Usuario.class.equals(cls); }

    @Override
    public void validate(Object obj, Errors errors) {
        Usuario usuario = (Usuario) obj;

        if (usuario.getEmail().trim().equals("")) {
            errors.rejectValue("email", "obligatorio", "Debe introducir su correo electrónico.");
        }
        else if (usuarioDao.getUsuarioEmail(usuario.getEmail()) == null) {
            errors.rejectValue("email", "obligatorio", "No existe el correo.");
        }
    }
}
