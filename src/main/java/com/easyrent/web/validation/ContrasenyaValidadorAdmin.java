package com.easyrent.web.validation;

import com.easyrent.web.domain.Contrasenya;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

public class ContrasenyaValidadorAdmin implements Validator {

    @Override
    public boolean supports(Class<?> cls) {return Contrasenya.class.equals(cls); }

    @Override
    public void validate(Object obj, Errors errors) {
        Contrasenya contrasenya = (Contrasenya) obj;

        if (contrasenya.getContrasenyaNueva().trim().equals("")) {
            errors.rejectValue("contrasenyaNueva", "obligatorio", "Debe introducir la nueva contraseña.");
        }
        else if (contrasenya.getContrasenyaNueva().length() < 8) {
            errors.rejectValue("contrasenyaNueva", "obligatorio", "La contraseña debe tener mínimo 8 caracteres o dígitos.");
        }
        else if (contrasenya.getContrasenyaRepetir().trim().equals("")) {
            errors.rejectValue("contrasenyaRepetir", "obligatorio", "Repita la nueva contraseña.");
        }
        else if (! contrasenya.getContrasenyaNueva().equals(contrasenya.getContrasenyaRepetir())) {
            errors.rejectValue("contrasenyaNueva", "obligatorio", "La nueva contraseña no coinciden.");
            errors.rejectValue("contrasenyaRepetir", "obligatorio", "La nueva contraseña no coinciden.");
        }
    }
}
