package com.easyrent.web.validation;

import com.easyrent.web.dao.PeriodoDaoTemp;
import com.easyrent.web.dao.ReservaDaoTemp;
import com.easyrent.web.domain.Periodo;
import com.easyrent.web.domain.Propiedad;
import com.easyrent.web.domain.Reserva;

import com.ibm.icu.text.SimpleDateFormat;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

public class ReservaValidador implements Validator {

    private PeriodoDaoTemp periodoDao;
    private ReservaDaoTemp reservaDao;
    private Propiedad propiedad;

    public ReservaValidador(PeriodoDaoTemp periodoDao, ReservaDaoTemp reservaDao, Propiedad propiedad) {
        this.periodoDao = periodoDao;
        this.reservaDao = reservaDao;
        this.propiedad = propiedad;
    }

    public boolean supports(Class<?> cls) {
        return Reserva.class.equals(cls);
    }

    public void validate(Object obj, Errors errors) {
        Reserva reserva = (Reserva) obj;

        // Obtengo la fecha actual *************************************************************************************
        Date ahora = new Date();
        SimpleDateFormat formateador = new SimpleDateFormat("yyyy-MM-dd");
        String fecha = formateador.format(ahora);
        Date fechaActual = null;

        try {
            fechaActual = formateador.parse(fecha);
        }
        catch(ParseException e) {
        }
        // *************************************************************************************************************

        // Comprobacion del campo NUM_PERSONAS.
        if (reserva.getNum_personas() <= 0) {
            errors.rejectValue("num_personas", "obligatorio", "El número de personas ha de mayor que 0.");
        }
        else if (reserva.getNum_personas() > propiedad.getCapacidad()) {
            errors.rejectValue("num_personas", "obligatorio", "El número de personas supera el límite de la capacidad.");
        }

        // Comprobacion de los campos de las fechas.
        if (reserva.getFecha_inicio() == null) {
            errors.rejectValue("fecha_inicio", "obligatorio", "Debe seleccionar una fecha de inicio.");
        }
        else if (reserva.getFecha_inicio().before(fechaActual)) {
            errors.rejectValue("fecha_inicio", "obligatorio", "La fecha de inicio debe igual o posterior a la fecha actual.");
        }

        if (reserva.getFecha_fin() == null) {
            errors.rejectValue("fecha_fin", "obligatorio", "Debe seleccionar una fecha fin.");
        }
        else if (reserva.getFecha_fin().before(reserva.getFecha_inicio())) {
            errors.rejectValue("fecha_fin", "obligatorio", "La fecha fin debe ser igual o posterior a la fecha de inicio.");
        }

        if (reserva.getFecha_inicio() != null && reserva.getFecha_fin() != null && ! comprobarFechas(reserva)) {
            errors.rejectValue("fecha_inicio", "obligatorio", "El periodo de reserva es incorrecto.");
            errors.rejectValue("fecha_fin", "obligatorio", "El periodo de reserva es incorrecto.");
        }
    }

    // Comprueba si las fechas de la reserva es valida.
    private boolean comprobarFechas(Reserva reservaCliente) {
        List<Periodo> periodos = periodoDao.getPeriodoPropiedad(reservaCliente.getId_propiedad());
        List<Reserva> reservas = reservaDao.getReservasPropiedad(reservaCliente.getId_propiedad());

        for (Periodo p : periodos) {
            if (reservaCliente.getFecha_inicio().before(p.getInicio())) {
                return false;
            } else if ((reservaCliente.getFecha_inicio().equals(p.getInicio()) || reservaCliente.getFecha_inicio().after(p.getInicio()))
                    && (reservaCliente.getFecha_fin().before(p.getFin()) || reservaCliente.getFecha_fin().equals(p.getFin()))) {

                for (Reserva r : reservas) {
                    // Parar la comprobacion de reservas (ordenadas).
                    if (r.getFecha_inicio().after(reservaCliente.getFecha_fin())) break;
                    if (r.getFecha_inicio().after(p.getFin())) break;

                    if (reservaCliente.getFecha_inicio().before(r.getFecha_inicio()) && reservaCliente.getFecha_fin().after(r.getFecha_fin())) {
                        return false;
                    } else if (reservaCliente.getFecha_inicio().equals(r.getFecha_inicio()) || reservaCliente.getFecha_inicio().after(r.getFecha_inicio())
                            && reservaCliente.getFecha_inicio().before(r.getFecha_fin()) || reservaCliente.getFecha_inicio().equals(r.getFecha_fin())) {
                        return false;
                    } else if (reservaCliente.getFecha_fin().equals(r.getFecha_inicio()) || reservaCliente.getFecha_fin().after(r.getFecha_inicio())
                            && reservaCliente.getFecha_fin().before(r.getFecha_fin()) || reservaCliente.getFecha_fin().equals(r.getFecha_fin())) {
                        return false;
                    }
                }

                return true;
            }
        }

        return false;
    }
}
