package com.easyrent.web.validation;

import com.easyrent.web.dao.FacturaDaoTemp;
import com.easyrent.web.dao.ValoracionDaoTemp;
import com.easyrent.web.domain.Valoracion;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

public class ValoracionValidador implements Validator {

    private ValoracionDaoTemp valoracionDao;
    private FacturaDaoTemp facturaDao;

    public ValoracionValidador(ValoracionDaoTemp valoracionDao, FacturaDaoTemp facturaDao) {
        this.valoracionDao = valoracionDao;
        this.facturaDao = facturaDao;
    }

    @Override
    public boolean supports(Class<?> cls) { return Valoracion.class.equals(cls); }

    @Override
    public void validate(Object obj, Errors errors) {
        Valoracion valoracion = (Valoracion) obj;

        if (facturaDao.getFacturasPropiedadUsuario(valoracion.getId_nacional(), valoracion.getId_propiedad()).isEmpty()) {
            errors.rejectValue("voto", "obligatorio", "Para comentar, debes haber reservado esta propiedad.");
        }
        else if (valoracionDao.getValoracion(valoracion.getId_propiedad(), valoracion.getId_nacional()) != null) {
            errors.rejectValue("voto", "obligatorio", "Ya has comentado a esta propiedad.");
        }
        else {
            if (valoracion.getVoto() < 1 || valoracion.getVoto() > 5) {
                errors.rejectValue("voto", "obligatorio", "Debes seleccionar una puntuación");
            }

            if (valoracion.getComentario().trim().equals("")) {
                errors.rejectValue("voto", "obligatorio", "Debe escribir un comentario");
            }
            else if (valoracion.getComentario().length() >= 500) {
                errors.rejectValue("voto", "obligatorio", "Superas el límite de caracateres. Máximo 500 y tienes " +
                        valoracion.getComentario().length() + " caracteres.");
            }
        }
    }
}
