package com.easyrent.web.validation;

import com.easyrent.web.dao.UsuarioDaoTemp;
import com.easyrent.web.domain.Usuario;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UsuarioUpdateUserValidador implements Validator {

	UsuarioDaoTemp usuarioDao;
	Usuario usuarioOriginal;

	public UsuarioUpdateUserValidador(UsuarioDaoTemp usuarioDao, Usuario usuarioOriginal) {
		this.usuarioDao = usuarioDao;
		this.usuarioOriginal = usuarioOriginal;
	}

	@Override
	public boolean supports(Class<?> cls) {
		return Usuario.class.equals(cls);
	}

	@Override
	public void validate(Object obj, Errors errors) {

		Usuario usuario = (Usuario) obj;

		// Comprobacion del campo DNI.
		if (usuario.getId_nacional().trim().equals("")) {
			errors.rejectValue("id_nacional", "obligatorio", "Debe introducir su NIE/NIF. Formato: X1234567H o 1234567K");
		}
		else if (! usuario.comprobarDNI(usuario.getId_nacional())) {
			errors.rejectValue("id_nacional", "obligatorio", "El formato es incorrecto. Formato: X1234567H o 1234567K");
		}
		else if (usuarioDao.getUsuarioDNI(usuario.getId_nacional()) != null &&
				 ! usuario.getId_nacional().equals(usuarioOriginal.getId_nacional())) {
			errors.rejectValue("id_nacional", "obligatorio", "El DNI ya existe");
		}

		// Comprobacion del campo NOMBRE.
		if (usuario.getNombre().trim().equals("")) {
			errors.rejectValue("nombre", "obligatorio", "Debe introducir su nombre");
		}

		// Comprobacion del campo APELLIDOS.
		if (usuario.getApellido().trim().equals("")) {
			errors.rejectValue("apellido", "obligatorio", "Debe introducir sus apellidos");
		}

		// Comprobacion del campo EMAIL.
		if (usuario.getEmail().trim().equals("")) {
			errors.rejectValue("email","obligatorio", "Debe introducir un email");
		}
		else if (usuarioDao.getUsuarioEmail(usuario.getEmail()) != null &&
				 ! usuario.getEmail().equals(usuarioOriginal.getEmail())) {
			errors.rejectValue("email", "obligatorio", "El email ya existe");
		}

		// Comprobacion del campo DIRECCION.
		if (usuario.getDireccion().trim().equals("")) {
			errors.rejectValue("direccion", "obligatorio", "Debe introducir su dirección");
		}

		// Comprobacion del campo TELEFONO.
		if (usuario.getNumero_telefono().equals("")) {
			errors.rejectValue("numero_telefono", "obligatorio", "Debe introducir un número de teléfono");
		}
		else if (! usuario.comprobarTelefono(usuario.getNumero_telefono())) {
			errors.rejectValue("numero_telefono", "obligatorio", "El número introducido es incorrecto");
		}
	}
}
