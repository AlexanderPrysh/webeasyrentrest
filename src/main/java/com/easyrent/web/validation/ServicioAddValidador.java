package com.easyrent.web.validation;

import com.easyrent.web.dao.ServicioDaoTemp;
import com.easyrent.web.domain.Servicio;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

public class ServicioAddValidador implements Validator {

    ServicioDaoTemp servicioDao;

    public ServicioAddValidador(ServicioDaoTemp servicioDao) { this.servicioDao = servicioDao; }

    @Override
    public boolean supports(Class<?> cls) { return Servicio.class.equals(cls); }

    @Override
    public void validate(Object obj, Errors errors) {
        Servicio servicio = (Servicio) obj;

        if (servicio.getNombre().trim().equals("")) {
            errors.rejectValue("nombre", "obligatorio", "Debe introducir un nombre de servicio");
        }

        else if (servicioDao.getServicioNombre(servicio.getNombre()) != null) {
            errors.rejectValue("nombre", "obligatorio", "El servicio ya está registrado en la base de datos");
        }
    }
}
