package com.easyrent.web.validation;

import com.easyrent.web.dao.UsuarioDaoTemp;
import com.easyrent.web.domain.Usuario;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

public class LoginValidador implements Validator {

    private UsuarioDaoTemp usuarioDao;

    public LoginValidador(UsuarioDaoTemp usuarioDao) {
        this.usuarioDao = usuarioDao;
    }

    @Override
    public boolean supports(Class<?> cls) {return Usuario.class.equals(cls); }

    @Override
    public void validate(Object obj, Errors errors) {
        Usuario usuario = (Usuario) obj;

        if (usuario.getUsuario().trim().equals("") && usuario.getContrasenya().trim().equals("")) {
            errors.rejectValue("usuario", "obligatorio", "Introduzca su nombre de usuario y contraseña.");
        }
        else if (usuario.getUsuario().trim().equals("")) {
            errors.rejectValue("usuario", "oblitorio", "Debe introducir su nombre de usuario.");
        }
        else if (usuario.getContrasenya().trim().equals("")) {
            errors.rejectValue("usuario", "obligatorio", "Debe introducir su contraseña.");
        }
        else if (usuarioDao.getUsuarioNombreUsuario(usuario.getUsuario()) == null) {
            errors.rejectValue("usuario", "obligatorio", "El nombre de usuario no existe.");
        }
        else if (usuarioDao.getUsuario(usuario.getUsuario(), usuario.getContrasenya()) == null ||
                 usuarioDao.getUsuario(usuario.getUsuario(), usuario.getContrasenya()).isEs_activo() == false) {
            errors.rejectValue("usuario", "obligatorio", "La contraseña es incorrecta.\n" +
                    "Compruebe los datos e intente de nuevo.");
        }
    }
}
