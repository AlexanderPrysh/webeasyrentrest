package com.easyrent.web.domain;

public class Valoracion {

    private int id_propiedad;
    private String id_nacional;
    private int voto;
    private String comentario;

    public Valoracion() { }

    public int getId_propiedad() {
        return id_propiedad;
    }

    public void setId_propiedad(int id_propiedad) {
        this.id_propiedad = id_propiedad;
    }

    public String getId_nacional() {
        return id_nacional;
    }

    public void setId_nacional(String id_nacional) {
        this.id_nacional = id_nacional;
    }

    public int getVoto() {
        return voto;
    }

    public void setVoto(int voto) {
        this.voto = voto;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public String toString() {
        return "--- Informacion de la valoracion ---" + "\n"
                + "ID propiedad: " + id_propiedad + "\n"
                + "ID nacional: " + id_nacional + "\n"
                + "Voto: " + voto + "\n"
                + "Comentario: " + comentario;
    }
}
