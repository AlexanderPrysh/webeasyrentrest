package com.easyrent.web.domain;

public class Imagen {

	private String nom_imagen;
	private int id_propiedad;
	private String url;
	
	public Imagen() { }
	
	public String getNom_imagen() {
		return nom_imagen;
	}
	
	public void setNom_imagen(String nom_imagen) {
		this.nom_imagen = nom_imagen;
	}
	
	public int getId_propiedad() {
		return id_propiedad;
	}
	
	public void setId_propiedad(int id_propiedad) {
		this.id_propiedad = id_propiedad;
	}
	
	public String getUrl() {
		return url;
	}
	
	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public String toString() {
		return "--- Informacion de la imagen ---" + "\n"
				+ "ID propiedad: " + id_propiedad + "\n"
				+ "Nombre imagen: " + nom_imagen + "\n"
				+ "URL: " + url;
	}
}
