package com.easyrent.web.domain;

public class PropServicios {

	private int id_propiedad;
	private int id_servicio;

	public PropServicios() { }

	public int getId_propiedad() {
		return id_propiedad;
	}
	
	public void setId_propiedad(int id_propiedad) {
		this.id_propiedad = id_propiedad;
	}
	
	public int getId_servicio() {
		return id_servicio;
	}
	
	public void setId_servicio(int id_servicio) {
		this.id_servicio = id_servicio;
	}

	@Override
	public String toString() {
		return "--- Informacion sobre servicios de la propiedad ---" + "\n"
				+ "id_propiedad: " + id_propiedad + "\n"
				+ " id_servicio: " + id_servicio;
	}
}
