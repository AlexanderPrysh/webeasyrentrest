package com.easyrent.web.domain;

import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Usuario {

	private int id_usuario;
	private String id_nacional;
	private String usuario;
	private String contrasenya;
	private String rol;
	private String nombre;
	private String apellido;
	private String email;
	private String direccion;
    private Date fecha_registro;
	private String numero_telefono;
	private boolean es_activo;
	
	public Usuario() { }

	public int getId_usuario() {
		return id_usuario;
	}

	public void setId_usuario(int id_usuario) {
		this.id_usuario = id_usuario;
	}

	public String getId_nacional() {
		return id_nacional;
	}

	public void setId_nacional(String id_nacional) {
		this.id_nacional = id_nacional;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getContrasenya() {
		return contrasenya;
	}

	public void setContrasenya(String contrasenya) {
		this.contrasenya = contrasenya;
	}

	public String getRol() {
		return rol;
	}

	public void setRol(String rol) {
		this.rol = rol;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

    public Date getFecha_registro() {
        return fecha_registro;
    }

	public void setFecha_registro(Date fecha_registro) {
        this.fecha_registro = fecha_registro;
    }

	public String getNumero_telefono() {
		return numero_telefono;
	}

	public void setNumero_telefono(String numero_telefono) {
		this.numero_telefono = numero_telefono;
	}

	public boolean isEs_activo() {
		return es_activo;
	}

	public void setEs_activo(boolean es_activo) {
		this.es_activo = es_activo;
	}


	// Metodos de comprobacion de datos.
	public boolean comprobarDNI(String dni) {
		Pattern numeroPattern = Pattern.compile("(\\d{1,8})");
		Pattern letraPattern = Pattern.compile("([TRWAGMYFPDXBNJZSQVHLCKE])");

		if (dni.length() == 9) {
			if (dni.startsWith("X") || dni.startsWith("Y") || dni.startsWith("Z")) {
				String cadena = dni.substring(1, 8);

				Matcher m = numeroPattern.matcher(cadena);
				if (m.matches()) {
					cadena = dni.substring(8);
					m = letraPattern.matcher(cadena);

					if (m.matches())
						return true;
				}
			}

			return false;
		}

		if (dni.length() == 8) {
			String cadena = dni.substring(0, 6);

			Matcher m = numeroPattern.matcher(cadena);
			if (m.matches()) {
				cadena = dni.substring(7);
				m = letraPattern.matcher(cadena);

				if (m.matches())
					return true;
			}

			return false;
		}

		return false;
	}

	public boolean comprobarTelefono(String numero) {
		if (numero.length() == 9) {
			Pattern numeroPattern = Pattern.compile("(\\d{1,9})");

			Matcher m = numeroPattern.matcher(numero);
			if (m.matches()) return true;
		}

		return false;
	}

	// Metodo de impresion de datos (toString()).
	public String toString() {
		return "--- Informacion del usuario ---" + "\n"
				+ "ID usuario: " + id_usuario + "\n"
				+ "Usuario: " + usuario + "\n"
				+ "Rol: " + rol + "\n"
				+ "ID nacional: " + id_nacional + "\n"
				+ "Nombre usuario: " + nombre + "\n"
				+ "Apellidos: " + apellido + "\n"
				+ "Email: " + email + "\n"
				+ "Direccion: " + direccion + "\n"
				+ "Fecha de registro: " + fecha_registro + "\n"
				+ "Numero de telefono: " + numero_telefono + "\n"
				+ "Esta activo: " + es_activo;
	}
}
