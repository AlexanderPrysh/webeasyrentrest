package com.easyrent.web.domain;

public class Servicio {

	private String nombre;
	private int id_servicio;

	public Servicio() { }
	
	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public int getId_servicio() {
		return id_servicio;
	}
	
	public void setId_servicio(int id_propiedad) {
		this.id_servicio = id_propiedad;
	}

	@Override
	public String toString() {
		return "--- Informacion del servicio ---" + "\n"
				+ "ID servicio: " + id_servicio + "\n"
				+ "Nombre servicio: " + nombre;
	}
}
