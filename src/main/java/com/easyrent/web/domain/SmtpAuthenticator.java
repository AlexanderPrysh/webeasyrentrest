package com.easyrent.web.domain;

import javax.mail.PasswordAuthentication;
import javax.mail.Authenticator;

public class SmtpAuthenticator extends Authenticator {
    private String username;
    private String password;

    public SmtpAuthenticator(String username, String password) {
        super();
        this.username = username;
        this.password = password;
        System.out.println("SMTPAuthenticator invoked and finished");
        System.out.println(username);
        System.out.println(password);
    }

    public PasswordAuthentication getPasswordAuthentication() {
        return new PasswordAuthentication(username, password);
    }
}
