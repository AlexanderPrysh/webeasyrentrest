package com.easyrent.web.domain;

import java.sql.Timestamp;
import java.util.Date;

public class Reserva {

	private int num_seguimiento;
	private String id_nacional;
	private int id_propiedad;
	private Timestamp fecha_reserva;
	private Date fecha_confirmacion;
	private int num_personas;
	private Date fecha_inicio;
	private Date fecha_fin;
	private double coste_total;
	private String estado; // Como son dos estados, podriamos usar boolean.

    public int getNum_seguimiento() {
        return num_seguimiento;
    }

    public void setNum_seguimiento(int num_seguimiento) {
        this.num_seguimiento = num_seguimiento;
    }

    public String getId_nacional() {
        return id_nacional;
    }

    public void setId_nacional(String id_nacional) {
        this.id_nacional = id_nacional;
    }

    public int getId_propiedad() {
        return id_propiedad;
    }

    public void setId_propiedad(int id_propiedad) {
        this.id_propiedad = id_propiedad;
    }

    public Timestamp getFecha_reserva() {
        return fecha_reserva;
    }

    public void setFecha_reserva(Timestamp fecha_reserva) {
        this.fecha_reserva = fecha_reserva;
    }

    public Date getFecha_confirmacion() {
        return fecha_confirmacion;
    }

    public void setFecha_confirmacion(Date fecha_confirmacion) {
        this.fecha_confirmacion = fecha_confirmacion;
    }

    public int getNum_personas() {
        return num_personas;
    }

    public void setNum_personas(int num_personas) {
        this.num_personas = num_personas;
    }

    public Date getFecha_inicio() {
        return fecha_inicio;
    }

    public void setFecha_inicio(Date fecha_inicio) {
        this.fecha_inicio = fecha_inicio;
    }

    public Date getFecha_fin() {
        return fecha_fin;
    }

    public void setFecha_fin(Date fecha_fin) {
        this.fecha_fin = fecha_fin;
    }

    public double getCoste_total() {
        return coste_total;
    }

    public void setCoste_total(double coste_total) {
        this.coste_total = coste_total;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String toString() {
        return "--- Informacion de la reserva ---" + "\n"
                + "Numero de seguimiento: " + num_seguimiento + "\n"
                + "ID nacional: " + id_nacional + "\n"
                + "ID propiedad: " + id_propiedad + "\n"
                + "Fecha reserva: " + fecha_reserva + "\n"
                + "Fecha confirmacion: " + fecha_confirmacion.toString() + "\n"
                + "Numero de personas: " + num_personas + "\n"
                + "Fecha inicio: " + fecha_inicio.toString() + "\n"
                + "Fecha fin: " + fecha_fin.toString() + "\n"
                + "Coste total: " + coste_total + "\n"
                + "Estado: " + estado;
    }
}
