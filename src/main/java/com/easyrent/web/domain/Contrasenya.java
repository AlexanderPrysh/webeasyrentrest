package com.easyrent.web.domain;

public class Contrasenya {

    private String contrasenyaAntigua;
    private String contrasenyaNueva;
    private String contrasenyaRepetir;

    public Contrasenya() { }

    public String getContrasenyaAntigua() { return contrasenyaAntigua; }
    public void setContrasenyaAntigua(String contrasenyaAntigua) { this.contrasenyaAntigua = contrasenyaAntigua; }
    public String getContrasenyaNueva() { return contrasenyaNueva; }
    public void setContrasenyaNueva(String contrasenyaNueva) { this.contrasenyaNueva = contrasenyaNueva; }
    public String getContrasenyaRepetir() { return contrasenyaRepetir; }
    public void setContrasenyaRepetir(String contrasenyaRepetir) { this.contrasenyaRepetir = contrasenyaRepetir; }
}
