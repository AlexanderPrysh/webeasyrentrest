package com.easyrent.web.domain;

import java.util.Date;

public class Periodo {

	private int id_periodo;
	private int id_propiedad;
	private Date inicio;
	private Date fin;
	
	public Periodo() { }
	
	public int getId_periodo() {
		return id_periodo;
	}
	
	public void setId_periodo(int id_periodo) {
		this.id_periodo = id_periodo;
	}
	
	public int getId_propiedad() {
		return id_propiedad;
	}
	
	public void setId_propiedad(int id_propiedad) {
		this.id_propiedad = id_propiedad;
	}
	
	public Date getInicio() {return inicio;}

	public void setInicio(Date inicio) {this.inicio=inicio;}
	
	public Date getFin() {return fin;}
	
	public void setFin(Date fin) {
		this.fin=fin;
	}

	public String toString() {
		return "--- Informacion del periodo ---" + "\n"
				+ "ID pperiodo: " + id_periodo + "\n"
				+ "ID propiedad: " + id_propiedad + "\n"
				+ "Fecha inicio: " + inicio + "\n"
				+ "Fecha fin: " + fin;
	}
}
