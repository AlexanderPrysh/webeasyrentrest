package com.easyrent.web.domain;

import java.util.Date;

public class Factura {

	private int num_factura;
	private int num_seguimiento;
	private Date fecha_factura;
	private double iva;

	public Factura() { }
	
	public int getNum_factura() {
		return num_factura;
	}
	
	public void setNum_factura(int num_factura) {
		this.num_factura = num_factura;
	}
	
	public int getNum_seguimiento() {
		return num_seguimiento;
	}
	
	public void setNum_seguimiento(int num_seguimiento) { this.num_seguimiento = num_seguimiento; }
	
	public Date getFecha_factura() { return fecha_factura; }
	
	public void setFecha_factura(Date d) { this.fecha_factura = d; }
	
	public double getIva() {
		return iva;
	}
	
	public void setIva(double iva) {
		this.iva = iva;
	}

	@Override
	public String toString() {
		return "--- Informacion de la Factura ---" + "\n"
				+ "Numero factura: " + num_factura + "\n"
				+ "Numero de seguimiento: " + num_seguimiento + "\n"
				+ "Fecha factura: " + fecha_factura + "\n"
				+ "IVA: " + iva;
	}
}
