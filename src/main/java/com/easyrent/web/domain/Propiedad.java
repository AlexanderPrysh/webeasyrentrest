package com.easyrent.web.domain;

public class Propiedad {

	private int id_propiedad;
	private String titulo;
	private String id_nacional;
	private String descripcion;
	private String tipo_propiedad;
	private int capacidad;
	private int num_habitaciones;
	private int num_banyos;
	private int num_camas;
	private int metros_cuadrados;
	private String calle;
	private int numero;
	private int planta;
	private String ciudad;
	private double precio_diario;
	private boolean es_activo;
	
	public Propiedad() { }
	
	public int getId_propiedad() {
		return id_propiedad;
	}
	
	public void setId_propiedad(int id_propiedad) {
		this.id_propiedad = id_propiedad;
	}
	
	public String getId_nacional() {
		return id_nacional;
	}
	
	public void setId_nacional(String id_nacional) {
		this.id_nacional = id_nacional;
	}
	
	public String getTitulo() {
		return titulo;
	}
	
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	
	public String getDescripcion() {
		return descripcion;
	}
	
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	public String getTipo_propiedad() {
		return tipo_propiedad;
	}
	
	public void setTipo_propiedad(String tipo_propiedad) {
		this.tipo_propiedad = tipo_propiedad;
	}
	
	public int getCapacidad() {
		return capacidad;
	}
	
	public void setCapacidad(int capacidad) {
		this.capacidad = capacidad;
	}
	
	public int getNum_habitaciones() {
		return num_habitaciones;
	}
	
	public void setNum_habitaciones(int num_habitaciones) {
		this.num_habitaciones = num_habitaciones;
	}
	
	public int getNum_banyos() {
		return num_banyos;
	}
	
	public void setNum_banyos(int num_banyos) {
		this.num_banyos = num_banyos;
	}
	
	public int getNum_camas() {
		return num_camas;
	}
	
	public void setNum_camas(int num_camas) {
		this.num_camas = num_camas;
	}
	
	public int getMetros_cuadrados() {
		return metros_cuadrados;
	}
	
	public void setMetros_cuadrados(int metros_cuadrados) {
		this.metros_cuadrados = metros_cuadrados;
	}
	
	public String getCalle() {
		return calle;
	}
	
	public void setCalle(String calle) {
		this.calle = calle;
	}
	
	public int getNumero() {
		return numero;
	}
	
	public void setNumero(int numero) {
		this.numero = numero;
	}
	
	public int getPlanta() {
		return planta;
	}
	
	public void setPlanta(int planta) {
		this.planta = planta;
	}
	
	public String getCiudad() {
		return ciudad;
	}
	
	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}
	
	public double getPrecio_diario() {
		return precio_diario;
	}
	
	public void setPrecio_diario(double precio_diario) {
		this.precio_diario = precio_diario;
	}
	
	public boolean isEs_activo() {
		return es_activo;
	}
	
	public void setEs_activo(boolean es_activo) {
		this.es_activo = es_activo;
	}

	public String toString() {
		return "--- Informacion de la propiedad ---" + "\n"
				+ "ID propiedad: " + id_propiedad + "\n"
				+ "ID nacional propietario: " + id_nacional + "\n"
				+ "Titulo: " + titulo + "\n"
				+ "Descripcion: " + descripcion + "\n"
				+ "Tipo de propiedad: " + tipo_propiedad + "\n"
				+ "Capacidad: " + capacidad + "\n"
				+ "Numero de habitaciones: " + num_habitaciones + "\n"
				+ "Numero de banyos: " + num_banyos + "\n"
				+ "Numero de camas: " + num_camas + "\n"
				+ "Metros cuadrados: " + metros_cuadrados + "\n"
				+ "Calle: " + calle + "\n"
				+ "Numero: " + numero + "\n"
				+ "Planta: " + planta + "\n"
				+ "Ciudad: " + ciudad + "\n"
				+ "Precio diario: " + precio_diario + "\n"
				+ "Esta activo: "  + es_activo;
	}
}
