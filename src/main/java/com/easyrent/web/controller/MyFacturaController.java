package com.easyrent.web.controller;

import com.easyrent.web.dao.FacturaDaoTemp;
import com.easyrent.web.dao.PropiedadDaoTemp;
import com.easyrent.web.domain.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/my/factura")
public class MyFacturaController {
    private FacturaDaoTemp facturaDao;
    private PropiedadDaoTemp propiedadDao;

    @Autowired
    public void setFacturaDao(FacturaDaoTemp facturaDao, PropiedadDaoTemp propiedadDao) {
        this.facturaDao = facturaDao;
        this.propiedadDao = propiedadDao;
    }

    // Muestra una lista de todas las facturas de la base de datos.
    @RequestMapping("/listfacturas")
    public String listFactura(HttpSession session, Model model) {
        Usuario usuario = (Usuario) session.getAttribute("usuario");

        if (usuario.getRol().equals("admin")) {
            model.addAttribute("factura", facturaDao.getFacturasAdmin());
            model.addAttribute("usuario", usuario);
        }
        else {
            model.addAttribute("factura", facturaDao.getFacturasUser(usuario.getId_nacional()));
            model.addAttribute("usuario", usuario);
        }

        return "factura/listfactura";
    }
}
