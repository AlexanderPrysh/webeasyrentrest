package com.easyrent.web.controller;

import com.easyrent.web.dao.*;
import com.easyrent.web.domain.*;
import com.easyrent.web.validation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


@Controller
@RequestMapping("/my/propiedad")
public class MyPropiedadController {

    private PropiedadDaoTemp propiedadDao;
    private ImagenDaoTemp imagenDao;
    private PeriodoDaoTemp periodoDao;
    private ReservaDaoTemp reservaDao;
    private FacturaDaoTemp facturaDao;
    private ValoracionDaoTemp valoracionDao;
    private ServicioDaoTemp servicioDao;
    private PropServiciosDaoTemp propServiciosDao;

    @Autowired
    public void setPropiedadDao(PropiedadDaoTemp propiedadDao, ImagenDaoTemp imagenDao, PeriodoDaoTemp periodoDao, ReservaDaoTemp reservaDao,
                                FacturaDaoTemp facturaDao, ValoracionDaoTemp valoracionDao, ServicioDaoTemp servicioDao, PropServiciosDaoTemp propServiciosDao) {
        this.propiedadDao = propiedadDao;
        this.imagenDao = imagenDao;
        this.periodoDao = periodoDao;
        this.reservaDao = reservaDao;
        this.facturaDao = facturaDao;
        this.valoracionDao = valoracionDao;
        this.servicioDao = servicioDao;
        this.propServiciosDao = propServiciosDao;
    }

    // Define el formato de la fecha.
    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false));
    }

    // Muestra la informacion sobre una propiedad.
    @RequestMapping("/{id_propiedad}")
    public String mostrarPropiedad(HttpSession session, Model model, @PathVariable int id_propiedad) {
        Usuario usuario = (Usuario) session.getAttribute("usuario");
        Propiedad propiedad = propiedadDao.getPropiedad(id_propiedad);
        List<Periodo> periodos = periodoDao.getPeriodoPropiedad(id_propiedad);
        List<Reserva> reservas = reservaDao.getReservasPropiedad(id_propiedad);
        List<Valoracion> valoraciones = valoracionDao.getValoracionesPropiedad(id_propiedad);

        // Saco las listas de las fechas inicio y fin
        periodos = sacarPeriodosLibres(periodos, reservas);
        List<String> listaFechaLibre = listarFechaLibre(periodos);

        // Saco la media de los votos de las valoraciones de la propiedad.
        double media = mediaVoto(valoraciones);

        if (usuario.getRol().equals("admin") || usuario.getId_nacional().equals(propiedad.getId_nacional())) {
            model.addAttribute("propiedad", propiedad);
            model.addAttribute("periodo", periodoDao.getPeriodo(id_propiedad));
            model.addAttribute("imagen", imagenDao.getImagenPropiedad(id_propiedad));

            model.addAttribute("fechalibre", listaFechaLibre);
            model.addAttribute("valoracion", valoraciones);
            model.addAttribute("nuevavaloracion", new Valoracion());
            model.addAttribute("media", media);

            return "public/showpropiedad";
        }

        return "redirect:listpropiedades.html";
    }

    // Muestra una lista de propiedades del usuario o todas (administrador).
    @RequestMapping("/listpropiedades")
    public String listPropiedad(HttpSession session, Model model) {
        Usuario usuario = (Usuario) session.getAttribute("usuario");

        if (usuario.getRol().equals("admin")) {
            model.addAttribute("propiedad", propiedadDao.getPropiedadesAdmin());
            return "propiedad/listpropiedadadmin";
        }
        else {
            model.addAttribute("propiedad", propiedadDao.getPropiedadesUser(usuario.getId_nacional()));
            return "propiedad/listpropiedaduser";
        }
    }

    // Anyade una nueva propiedad a la base de datos.
    @RequestMapping(value = "/addpropiedad")
    public String addPropiedad(HttpSession session, Model model) {
        model.addAttribute("propiedad", new Propiedad());

        return "propiedad/addpropiedad";
    }

    @RequestMapping(value = "/addpropiedad", method = RequestMethod.POST)
    public String processAddSubmit(HttpSession session, @ModelAttribute("propiedad") Propiedad propiedad, BindingResult bindingResult) {
        PropiedadAddValidador propiedadValidator = new PropiedadAddValidador(propiedadDao);
        propiedadValidator.validate(propiedad, bindingResult);

		if (bindingResult.hasErrors())
            return "propiedad/addpropiedad";

        Usuario usuario = (Usuario) session.getAttribute("usuario");
        propiedad.setId_nacional(usuario.getId_nacional());
        propiedadDao.addPropiedad(propiedad);

        return "redirect:listpropiedades.html?success=propiedad";
    }

    // Modifica los datos de una propiedad.
    @RequestMapping(value = "/updatepropiedad/{id_propiedad}", method = RequestMethod.GET)
    public String updatePropiedad(HttpSession session, Model model, @PathVariable int id_propiedad) {
        Usuario usuario = (Usuario) session.getAttribute("usuario");
        Propiedad propiedad = propiedadDao.getPropiedad(id_propiedad);

        if (usuario.getId_nacional().equals(propiedad.getId_nacional())) {
            model.addAttribute("propiedad", propiedad);

            return "propiedad/updatepropiedad";
        }

        return "redirect:../listpropiedades.html";
    }

    @RequestMapping(value = "/updatepropiedad/{id_propiedad}", method = RequestMethod.POST)
    public String processUpdateSubmit(HttpSession session, @PathVariable int id_propiedad, @ModelAttribute("propiedad") Propiedad propiedad, BindingResult bindingResult) {
        Usuario usuario = (Usuario) session.getAttribute("usuario");
        // Aplico antes el cambio de id_propiedad e id_nacional para validar que los cambios sean sobre la misma propiedad.
        propiedad.setId_propiedad(id_propiedad);
        propiedad.setId_nacional(usuario.getId_nacional());

        PropiedadUpdateValidador propiedadValidator = new PropiedadUpdateValidador(propiedadDao, usuario);
        propiedadValidator.validate(propiedad, bindingResult);

		if (bindingResult.hasErrors())
			return "propiedad/updatepropiedad";

        propiedadDao.updatePropiedad(propiedad);

        return "redirect:../listpropiedades.html";
    }

    // Elimina una propiedad de la base de datos, solo si no queda reservas por proceder.
    @RequestMapping(value = "/deletepropiedad/{id_propiedad}")
    public String deletePropiedad(HttpSession session, @PathVariable int id_propiedad) {
        Usuario usuario = (Usuario) session.getAttribute("usuario");
        Propiedad propiedad = propiedadDao.getPropiedad(id_propiedad);

        if (usuario.getRol().equals("admin") || usuario.getId_nacional().equals(propiedad.getId_nacional())) {
            Date fechaActual = new Date();
            Reserva reserva = reservaDao.getReservaMasTarde(id_propiedad);

            if (reserva == null || reserva.getFecha_fin().before(fechaActual))
                propiedadDao.deletePropiedad(propiedad);
        }

        return "redirect:../listpropiedades.html";
    }

    // Desactiva una propiedad, sin eliminarla de la base de datos.
    @RequestMapping(value="/desactivarpropiedad/{id_propiedad}")
    public String desactivarPropiedad(HttpSession session, @PathVariable int id_propiedad) {
        Usuario usuario = (Usuario) session.getAttribute("usuario");
        Propiedad propiedad = propiedadDao.getPropiedad(id_propiedad);

        if (usuario.getRol().equals("admin") || usuario.getId_nacional().equals(propiedad.getId_nacional())) {
            if (reservaDao.getReservasPendientesPropiedad(id_propiedad).isEmpty()) {
                propiedad.setEs_activo(false);
                propiedadDao.activardesactivarPropiedad(propiedad);
            }
        }

        return "redirect:../listpropiedades.html";
    }

    // Reactivar una propiedad, cambia el estado a true.
    @RequestMapping(value="/reactivarpropiedad/{id_propiedad}")
    public String activarPropiedad(HttpSession session, @PathVariable int id_propiedad) {
        Usuario usuario = (Usuario) session.getAttribute("usuario");
        Propiedad propiedad = propiedadDao.getPropiedad(id_propiedad);

        if (usuario.getRol().equals("admin") || usuario.getId_nacional().equals(propiedad.getId_nacional())) {
            propiedad.setEs_activo(true);
            propiedadDao.activardesactivarPropiedad(propiedad);
        }

        return "redirect:../listpropiedades.html";
    }


    // ******************************* Acciones relacionadas con PERIODO ***********************************************
    // *****************************************************************************************************************

    // Muestra una lista de todos los periodos de una propiedad.
    @RequestMapping(value = "/{id_propiedad}/periodos")
    public String listPeriodoPropiedad(HttpSession session, Model model, @PathVariable int id_propiedad) {
        Usuario usuario = (Usuario) session.getAttribute("usuario");
        Propiedad propiedad = propiedadDao.getPropiedad(id_propiedad);

        if (usuario.getRol().equals("admin") || usuario.getId_nacional().equals(propiedad.getId_nacional())) {
            model.addAttribute("periodo", periodoDao.getPeriodoPropiedad(id_propiedad));
            model.addAttribute("propiedad", propiedad);

            if (usuario.getRol().equals("admin"))
                return "periodo/listperiodoAdmin";
            else
                return "periodo/listperiodoUser";
        }

        return "redirect:../listpropiedades.html";
    }

    // Anyade un nuevo periodo a una propiedad (solo propietario de la propiedad)..
    @RequestMapping(value = "/{id_propiedad}/addperiodo")
    public String addPeriodo(HttpSession session, Model model, @PathVariable int id_propiedad) {
        Usuario usuario = (Usuario) session.getAttribute("usuario");
        Propiedad propiedad = propiedadDao.getPropiedad(id_propiedad);

        if (usuario.getId_nacional().equals(propiedad.getId_nacional())) {
            model.addAttribute("periodo", new Periodo());
            model.addAttribute("propiedad", propiedad);

            return "periodo/addperiodo";
        }

        return "redirect:../listpropiedades.html";
    }

    @RequestMapping(value = "/{id_propiedad}/addperiodo", method = RequestMethod.POST)
    public String processAddSubmit(HttpSession session, @PathVariable int id_propiedad,
                                   @ModelAttribute("periodo") Periodo periodo, BindingResult bindingResult) {
        PeriodoAddValidador periodoValidator = new PeriodoAddValidador(periodoDao, id_propiedad);
        periodoValidator.validate(periodo, bindingResult);

        if (bindingResult.hasErrors())
            return "periodo/addperiodo";

        periodo.setId_propiedad(id_propiedad);
        periodoDao.addPeriodo(periodo);

        return "redirect:periodos.html";
    }

    // Modifica los datos de un periodo de una propiedad (solo propietario de la propiedad).
    @RequestMapping(value = "/{id_propiedad}/updateperiodo/{id_periodo}", method = RequestMethod.GET)
    public String updatePeriodo(HttpSession session, Model model, @PathVariable("id_propiedad") int id_propiedad,
                                @PathVariable("id_periodo") int id_periodo) {
        Usuario usuario = (Usuario) session.getAttribute("usuario");
        Propiedad propiedad = propiedadDao.getPropiedad(id_propiedad);

        if (usuario.getId_nacional().equals(propiedad.getId_nacional())) {
            model.addAttribute("periodo", periodoDao.getPeriodo(id_periodo));
            model.addAttribute("propiedad", propiedad);

            return "periodo/updateperiodo";
        }

        return "redirect:../../listpropiedades.html";
    }

    @RequestMapping(value = "/{id_propiedad}/updateperiodo/{id_periodo}", method = RequestMethod.POST)
    public String processUpdateSubmit(HttpSession session, @PathVariable("id_periodo") int id_periodo, @PathVariable("id_propiedad") int id_propiedad,
                                      @ModelAttribute("periodo") Periodo periodo, BindingResult bindingResult) {
		// Aplico antes el cambio de id_propiedad e id_periodo para el validador.
        periodo.setId_propiedad(id_propiedad);
        periodo.setId_periodo(id_periodo);

        PeriodoUpdateValidador periodoValidator = new PeriodoUpdateValidador(periodoDao, id_propiedad, periodoDao.getPeriodo(id_periodo));
        periodoValidator.validate(periodo, bindingResult);

        if (bindingResult.hasErrors())
            return "periodo/updateperiodo";

        periodoDao.updatePeriodo(periodo);

        return "redirect:../periodos.html";
    }

    // Elimina un periodo de una propiedad
    @RequestMapping(value = "/{id_propiedad}/deleteperiodo/{id_periodo}")
    public String deletePeriodo(HttpSession session, @PathVariable("id_propiedad") int id_propiedad, @PathVariable("id_periodo") int id_periodo) {
        Usuario usuario = (Usuario) session.getAttribute("usuario");
        Propiedad propiedad = propiedadDao.getPropiedad(id_propiedad);

        if (usuario.getRol().equals("admin") || usuario.getId_nacional().equals(propiedad.getId_nacional())) {
            periodoDao.deletePeriodo(periodoDao.getPeriodo(id_periodo));

            return "redirect:../periodos.html";
        }

        return "redirect:../../listpropiedades.html";
    }


    // ******************************* Acciones relacionadas con Imagen ************************************************
    // *****************************************************************************************************************

    // Muestra una lista de todas las imagenes de una propiedad.
    @RequestMapping(value = "/{id_propiedad}/imagenes")
    public String listImagenPropiedad(HttpSession session, Model model, @PathVariable int id_propiedad) {
        Usuario usuario = (Usuario) session.getAttribute("usuario");
        Propiedad propiedad = propiedadDao.getPropiedad(id_propiedad);

        if (usuario.getRol().equals("admin") || usuario.getId_nacional().equals(propiedad.getId_nacional())) {
            model.addAttribute("imagen", imagenDao.getImagenPropiedad(id_propiedad));
            model.addAttribute("propiedad", propiedad);


            return "imagen/listimagen";
        }

        return "redirect:../listpropiedades.html";
    }

    // Anyade una nueva imagen a una propiedad..
    @RequestMapping(value = "/{id_propiedad}/addimagen")
    public String addImagen(HttpSession session, Model model, @PathVariable int id_propiedad) {
        Usuario usuario = (Usuario) session.getAttribute("usuario");
        Propiedad propiedad = propiedadDao.getPropiedad(id_propiedad);

        if (usuario.getId_nacional().equals(propiedad.getId_nacional())) {
            model.addAttribute("imagen", new Imagen());
            model.addAttribute("propiedad", propiedad);
            model.addAttribute("usuario", usuario);

            return "imagen/addimagen";
        }

        return "redirect:../listpropiedades.html";
    }

    @RequestMapping(value = "/{id_propiedad}/addimagen", method = RequestMethod.POST)
    public String processAddSubmit(HttpSession session, @ModelAttribute("imagen") Imagen imagen, @PathVariable int id_propiedad, BindingResult bindingResult) {
        return "redirect:../" + id_propiedad + ".html";
    }

    // Modifica los datos de una imagen (solo usuario).
    @RequestMapping(value = "/{id_propiedad}/updateimagen/{nom_imagen}", method = RequestMethod.GET)
    public String updateImagen(HttpSession session, Model model, @PathVariable("id_propiedad") int id_propiedad, @PathVariable("nom_imagen") String nom_imagen) {
        Usuario usuario = (Usuario) session.getAttribute("usuario");
        Propiedad propiedad = propiedadDao.getPropiedad(id_propiedad);

        if (usuario.getId_nacional().equals(propiedad.getId_nacional())) {
            model.addAttribute("imagen", imagenDao.getImagenNomId(nom_imagen, id_propiedad));

            return "imagen/updateimagen";
        }

        return "redirect:../../listpropiedades.html";
    }

    @RequestMapping(value = "/{id_propiedad}/updateimagen/{nom_imagen}", method = RequestMethod.POST)
    public String processUpdateSubmit(HttpSession session, @ModelAttribute("imagen") Imagen imagen, @PathVariable("id_propiedad") String nom_imagen,
                                      @PathVariable("nom_imagen") int id_propiedad, BindingResult bindingResult) {

        imagen.setId_propiedad(id_propiedad);
        imagen.setNom_imagen(nom_imagen);
        imagenDao.updateImagen(imagen);

        return "redirect:../imagenes.html";
    }

    // Elimina una imagen de una propiedad.
    @RequestMapping(value = "/{id_propiedad}/deleteimagen/{nom_imagen}")
    public String deleteImagen(HttpSession session, @PathVariable("id_propiedad") int id_propiedad, @PathVariable("nom_imagen") String nom_imagen) {
        Usuario usuario = (Usuario) session.getAttribute("usuario");
        Propiedad propiedad = propiedadDao.getPropiedad(id_propiedad);

        if (usuario.getRol().equals("admin") || usuario.getId_nacional().equals(propiedad.getId_nacional())) {
            imagenDao.deleteImagen(imagenDao.getImagenNomId(nom_imagen, id_propiedad));

            return "redirect:../imagenes.html";
        }

        return "redirect:../../listpropiedades.html";
    }


    // ******************************* Acciones relacionadas con VALORACION ********************************************
    // *****************************************************************************************************************

    // Anyade una valoracion a una propiedad (solo con factura)
    @RequestMapping(value = "/{id_propiedad}/addvaloracion")
    public String addValoracion(HttpSession session, @PathVariable int id_propiedad) {
        Usuario usuario = (Usuario) session.getAttribute("usuario");
        Propiedad propiedad = propiedadDao.getPropiedad(id_propiedad);
        Valoracion valoracion = (Valoracion) session.getAttribute("valoracion");

        if (propiedad.getId_propiedad() == valoracion.getId_propiedad() &&
                ! propiedad.getId_nacional().equals(usuario.getId_nacional()))
        {
                valoracionDao.addValoracion(valoracion);
        }

        return "redirect:../../../showpropiedad/" + id_propiedad + ".html";
    }


    // ******************************* Acciones relacionadas con PROPSERVICIOS *********************************************
    // *****************************************************************************************************************

    // Anyade un nuevo servicio a una propiedad.
    @RequestMapping(value = "/{id_propiedad}/addpropservicio")
    public String addPropServicios(HttpSession session, Model model, @PathVariable int id_propiedad) {
        Usuario usuario = (Usuario) session.getAttribute("usuario");
        Propiedad propiedad = propiedadDao.getPropiedad(id_propiedad);

        if (usuario.getRol().equals("admin") || usuario.getId_nacional().equals(propiedad.getId_nacional())) {
            model.addAttribute("servicio", servicioDao.getServicios());
            model.addAttribute("propServicio", new PropServicios());
            model.addAttribute("propiedad", propiedad);

            return "propservicios/addpropservicio";
        }

        return "redirect:../listpropiedades.html";
    }

    @RequestMapping(value = "/{id_propiedad}/addpropservicio", method = RequestMethod.POST)
    public String processAddSubmit(@ModelAttribute("propServicio") PropServicios propServicio, @PathVariable int id_propiedad) {
        propServiciosDao.addPropServicios(propServicio.getId_servicio(), id_propiedad);

        return "redirect:listpropservicios.html";
    }

    // Elimina un servicio de una propiedad.
    @RequestMapping(value = "/{id_propiedad}/deletepropservicio/{id_servicio}")
    public String deleteServicio(HttpSession session, @PathVariable("id_propiedad") int id_propiedad, @PathVariable("id_servicio") int id_servicio) {
        Usuario usuario = (Usuario) session.getAttribute("usuario");
        Propiedad propiedad = propiedadDao.getPropiedad(id_propiedad);

        if (usuario.getRol().equals("admin") || usuario.getId_nacional().equals(propiedad.getId_nacional()))
            propServiciosDao.deletePropServicios(propServiciosDao.getPropServicios(id_propiedad, id_servicio));

        return "redirect:../listpropservicios.html";
    }

    @RequestMapping(value = "/{id_propiedad}/listpropservicios")
    public String listPropServicios(HttpSession session, Model model, @PathVariable int id_propiedad) {
        Usuario usuario = (Usuario) session.getAttribute("usuario");
        Propiedad propiedad = propiedadDao.getPropiedad(id_propiedad);

        if (usuario.getRol().equals("admin") || usuario.getId_nacional().equals(propiedad.getId_nacional())) {
            model.addAttribute("propservicios", servicioDao.getServiciosPropiedad(id_propiedad));
            model.addAttribute("propiedad", propiedad);

            return "propservicios/listpropservicios";
        }

        return "redirect:../listpropiedades.html";
    }

    // *****************************************************************************************************************
    // Muestra las reservas de una propiedad.
    @RequestMapping(value = "/{id_propiedad}/listreservaprop")
    public String listReservaProp(HttpSession session, Model model, @PathVariable int id_propiedad) {
        Usuario usuario = (Usuario) session.getAttribute("usuario");
        Propiedad propiedad = propiedadDao.getPropiedad(id_propiedad);

        if (usuario.getRol().equals("admin") || usuario.getId_nacional().equals(propiedad.getId_nacional())) {
            model.addAttribute("reservaprop", reservaDao.getReservasPropiedadOrdenadas(id_propiedad));
            model.addAttribute("propiedad", propiedad);
            model.addAttribute("usuario", usuario);

            if (usuario.getRol().equals("admin"))
                return "reserva/listreservapropAdmin";
            else
                return "reserva/listreservapropUser";
        }

        return "redirect:../listpropiedades.html";
    }


    // ******************************** Metodos para sacar los periodos libres *****************************************
    private List<Periodo> sacarPeriodosLibres(List<Periodo> periodos, List<Reserva> reservas) {
        List<Periodo> listaPeriodosLibres = new ArrayList<Periodo>();
        ArrayList<Reserva> borrarReservas = new ArrayList<Reserva>();

        for (Periodo p : periodos) {
            // Borramos de la lista de reservas, las reservas del anterior periodo.
            for (Reserva borrar : borrarReservas) {
                reservas.remove(borrar);
            }
            borrarReservas.clear();

            if (reservas.isEmpty()) {
                listaPeriodosLibres.add(p);
            }
            else {
                for (Reserva r : reservas) {
                    if (r.getFecha_inicio().after(p.getFin())) {
                        break;
                    } else if (r.getFecha_inicio().equals(p.getInicio()) && r.getFecha_fin().equals(p.getFin())) {
                        p.setInicio(null);
                        p.setFin(null);
                        borrarReservas.add(r);
                        break;
                    } else if (r.getFecha_inicio().equals(p.getInicio()) && r.getFecha_fin().before(p.getFin())) {
                        p.setInicio(diaPosterior(r.getFecha_fin()));
                        borrarReservas.add(r);
                    } else if (r.getFecha_inicio().after(p.getInicio()) && r.getFecha_fin().before(p.getFin())) {
                        Periodo nuevoPeriodo = new Periodo();
                        nuevoPeriodo.setInicio(p.getInicio());
                        nuevoPeriodo.setFin(diaAnterior(r.getFecha_inicio()));
                        listaPeriodosLibres.add(nuevoPeriodo);

                        p.setInicio(diaPosterior(r.getFecha_fin()));
                        borrarReservas.add(r);
                    } else if (r.getFecha_inicio().after(p.getInicio()) && r.getFecha_fin().equals(p.getFin())) {
                        p.setFin(diaAnterior(r.getFecha_inicio()));
                        borrarReservas.add(r);
                    }
                }
                if (p.getInicio() != null && p.getFin() != null)
                    listaPeriodosLibres.add(p);
            }
        }

        return listaPeriodosLibres;
    }

    private Date diaAnterior(Date fechaModificar) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(fechaModificar);
        calendar.add(Calendar.DAY_OF_YEAR, -1);

        Date fecha = calendar.getTime();

        return fecha;
    }

    private Date diaPosterior(Date fechaModificar) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(fechaModificar);
        calendar.add(Calendar.DAY_OF_YEAR, 1);

        Date fecha = calendar.getTime();

        return fecha;
    }

    private List<String> listarFechaLibre(List<Periodo> periodos) {
        List<String> listaFechaInicio = new ArrayList<String>();

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        for (Periodo p : periodos) {
            String inicio = format.format(p.getInicio());
            String fin = format.format(p.getFin());

            String fecha = inicio + "   --->  " + fin;
            listaFechaInicio.add(fecha);
        }

        return listaFechaInicio;
    }
    // *****************************************************************************************************************

    private double mediaVoto(List<Valoracion> valoraciones) {
        if (valoraciones.isEmpty()) return 0.0;

        double media = 0.0;

        for (Valoracion val : valoraciones) {
            media += val.getVoto();
        }

        return media / valoraciones.size();
    }
}
