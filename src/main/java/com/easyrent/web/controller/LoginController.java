package com.easyrent.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.easyrent.web.domain.Usuario;
import com.easyrent.web.validation.LoginValidador;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.easyrent.web.dao.UsuarioDaoTemp;

@Controller
public class LoginController {

	@Autowired
	private UsuarioDaoTemp usuarioDao;
	
	@RequestMapping("/login")
	public String login(HttpSession session, Model model) {
		model.addAttribute("usuario", new Usuario());
		return"login";
	}

	// Comprueba la autenticacion y redirige a la pagina principal del admin o del cliente
	@RequestMapping(value="/login", method=RequestMethod.POST)
	public String checkLogin(HttpSession session, @ModelAttribute("usuario") Usuario usuario,  BindingResult bindingResult) {
		LoginValidador loginValidator = new LoginValidador(usuarioDao);
		loginValidator.validate(usuario, bindingResult);

		if (bindingResult.hasErrors())
			return "login";

        String redirectUrl = (String) session.getAttribute("url_prior_login");

		usuario = usuarioDao.getUsuarioNombreUsuario(usuario.getUsuario());
		session.setAttribute("usuario", usuario);

		if (redirectUrl != null)
			return "redirect:" + redirectUrl;

		if (usuario.getRol().equals("admin"))
			return "redirect:/my/dashboard.html";
		else
			return "redirect:/my/usermenu.html";
	}

	@RequestMapping("/logout")
	public String logout(HttpSession session) {
		session.invalidate();
		return "redirect:index.jsp";
	}
}
