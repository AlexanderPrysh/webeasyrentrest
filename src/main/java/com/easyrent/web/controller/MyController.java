package com.easyrent.web.controller;

import com.easyrent.web.dao.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/my")
public class MyController {

    private UsuarioDaoTemp usuarioDao;

    @Autowired
    public void setMyDao(UsuarioDaoTemp usuarioDao) {
        this.usuarioDao = usuarioDao;
    }

    // Pagina principal del administrador.
    @RequestMapping("/dashboard")
    public String showAdminPage(HttpSession session, Model model){ return "dashboard/dashboard"; }

    // Pagina principal del usuario normal.
    @RequestMapping(value = "/usermenu", method = RequestMethod.GET)
    public String showUserPage(HttpSession session, Model model) {
        return "usuario/usermenu";
    }
}
