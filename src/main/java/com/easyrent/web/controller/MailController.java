package com.easyrent.web.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;

@Controller
@RequestMapping("/sendmail")
public abstract class MailController {
    protected final Log logger = LogFactory.getLog(getClass());
    private JavaMailSenderImpl mailSender;

//    System.out.println("He llamado al MailController");


    @RequestMapping(value="/send-mail", method= RequestMethod.POST)
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        System.out.println("Accediendo al metodo handleRequest");
        logger.info("Emviando email.");

        try {
            MimeMessage message = mailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, true);

            helper.setFrom("easyrentproyect@gmail.com");
            helper.setTo("alexanderprysh@gmail.com");
            helper.setSubject("Esto es un email de prueba.");
            helper.setText("<b>Esto es el texto del email en negrita.</b>", true);
            File file = new File("webapp/static/imgs/logo.png");
            helper.addAttachment("MiImagen", file);

            mailSender.send(message);
        } catch (MessagingException mx) {
            logger.error("No se ha podido enviar el email.");
            mx.printStackTrace();
        }

        return new ModelAndView("mivista");
    }

    public void setMailSender(JavaMailSenderImpl mailSender) {

        this.mailSender = mailSender;
    }
}