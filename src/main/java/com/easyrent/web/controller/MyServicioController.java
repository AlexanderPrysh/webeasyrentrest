package com.easyrent.web.controller;

import com.easyrent.web.dao.ServicioDaoTemp;
import com.easyrent.web.domain.Servicio;
import com.easyrent.web.validation.ServicioAddValidador;
import com.easyrent.web.validation.ServicioUpdateValidador;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/my/servicio")
public class MyServicioController {

    private ServicioDaoTemp servicioDao;

    @Autowired
    public void setServicioDao(ServicioDaoTemp servicioDao) {
        this.servicioDao = servicioDao;
    }

    // Muestra una lista con todos los servicios de la base de datos (solo administrador).
    @RequestMapping("/listservicios")
    public String listServicio(HttpSession session, Model model) {
        model.addAttribute("servicio", servicioDao.getServicios());

        return "servicio/listservicio";
    }

    // Anyade un nuevo servicio a la base de datos (solo administrador)
    @RequestMapping(value = "/addservicio")
    public String addServicio(HttpSession session, Model model) {
        model.addAttribute("servicio", new Servicio());

        return "servicio/addservicio";
    }

    @RequestMapping(value = "/addservicio", method = RequestMethod.POST)
    public String processAddSubmit(HttpSession session, @ModelAttribute("servicio") Servicio servicio, BindingResult bindingResult) {
        ServicioAddValidador servicioValidator = new ServicioAddValidador(servicioDao);
        servicioValidator.validate(servicio, bindingResult);

        if (bindingResult.hasErrors())
            return "servicio/addservicio";

        servicioDao.addServicio(servicio);

        return "redirect:listservicios.html";
    }

    // Modifica los datos de un servicio (solo administrador)
    @RequestMapping(value = "/updateservicio/{id_servicio}", method = RequestMethod.GET)
    public String updateServicio(HttpSession session, Model model, @PathVariable int id_servicio) {
        model.addAttribute("servicio", servicioDao.getServicio(id_servicio));

        return "servicio/updateservicio";
    }

    @RequestMapping(value = "/updateservicio/{id_servicio}", method = RequestMethod.POST)
    public String processUpdateSubmit(HttpSession session, @PathVariable int id_servicio,
                                      @ModelAttribute("servicio") Servicio servicio, BindingResult bindingResult) {
        Servicio servicioOriginal = servicioDao.getServicio(id_servicio);
        ServicioUpdateValidador servicioValidator = new ServicioUpdateValidador(servicioDao, servicioOriginal);
        servicioValidator.validate(servicio, bindingResult);

        if (bindingResult.hasErrors())
            return "servicio/updateservicio";

        servicioOriginal.setNombre(servicio.getNombre());
        servicioDao.updateServicio(servicioOriginal);

        return "redirect:../listservicios.html";
    }

    // Elimina un servicio de la base de datos (solo administrador)
    @RequestMapping(value = "/deleteservicio/{id_servicio}")
    public String processDelete(HttpSession session, @PathVariable int id_servicio) {
        servicioDao.deleteServicio(servicioDao.getServicio(id_servicio));

        return "redirect:../listservicios.html";
    }
}
