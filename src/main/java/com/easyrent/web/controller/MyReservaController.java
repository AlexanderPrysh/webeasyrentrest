package com.easyrent.web.controller;

import com.easyrent.web.dao.*;
import com.easyrent.web.domain.*;
import com.easyrent.web.email.Mail;
import com.easyrent.web.validation.ReservaValidador;
import com.lowagie.text.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.io.FileOutputStream;

import com.lowagie.text.pdf.PdfWriter;


@Controller
@RequestMapping("/my/reserva")
public class MyReservaController {

    private ReservaDaoTemp reservaDao;
    private PropiedadDaoTemp propiedadDao;
    private PeriodoDaoTemp periodoDao;
    private UsuarioDaoTemp usuarioDao;
    private FacturaDaoTemp facturaDao;

    @Autowired
    public void setReservaDao(ReservaDaoTemp reservaDao, PropiedadDaoTemp propiedadDao, PeriodoDaoTemp periodoDao,
                              UsuarioDaoTemp usuarioDao, FacturaDaoTemp facturaDao) {
        this.reservaDao = reservaDao;
        this.propiedadDao = propiedadDao;
        this.periodoDao = periodoDao;
        this.usuarioDao = usuarioDao;
        this.facturaDao = facturaDao;
    }

    // Define el formato de la fecha.
    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false));
    }

    // Muestra una lista de reservas del usuario o todas (administrador).
    @RequestMapping("/listreservas")
    public String listReserva(HttpSession session, Model model) {
        Usuario usuario = (Usuario) session.getAttribute("usuario");

        if (usuario.getRol().equals("admin")) {
            model.addAttribute("reserva", reservaDao.getReservasAdmin());
            model.addAttribute("usuario", usuario);
        }
        else {
            model.addAttribute("reserva", reservaDao.getReservasUser(usuario.getId_nacional()));
            model.addAttribute("usuario", usuario);
        }

        return "reserva/listreserva";
    }

    // Realiza una reserva (solo usuario normales).
    @RequestMapping(value = "/addreserva/{id_propiedad}", method = RequestMethod.GET)
    public String addReserva(HttpSession session, Model model, @PathVariable int id_propiedad) {
        Usuario usuario = (Usuario) session.getAttribute("usuario");
        Propiedad propiedad = propiedadDao.getPropiedad(id_propiedad);
        List<Periodo> periodos = periodoDao.getPeriodoPropiedad(id_propiedad);
        List<Reserva> reservas = reservaDao.getReservasPropiedad(id_propiedad);

        // Saco la lista de las fechas inicio y fin.
        periodos = sacarPeriodosLibres(periodos, reservas);
        List<String> listaFechaLibre = listarFechaLibre(periodos);

        if (propiedad.isEs_activo() == true) {
            if (!usuario.getId_nacional().equals(propiedad.getId_nacional())) {
                model.addAttribute("usuario", session.getAttribute("usuario"));
                model.addAttribute("propiedad", propiedad);
                model.addAttribute("reserva", new Reserva());
                model.addAttribute("fechalibre", listaFechaLibre);

                return "reserva/addreserva";
            }
        }

        return "redirect:../listreservas.html";
    }

    @RequestMapping(value = "/addreserva/{id_propiedad}", method = RequestMethod.POST)
    public String processAddSubmit(HttpSession session, @PathVariable int id_propiedad, Model model,
                                   @ModelAttribute("reserva") Reserva reserva, BindingResult bindingResult) {
        Propiedad propiedad = propiedadDao.getPropiedad(id_propiedad);
        ReservaValidador reservaValidator = new ReservaValidador(periodoDao, reservaDao, propiedad);
        reservaValidator.validate(reserva, bindingResult);

        if (bindingResult.hasErrors()) {
            Usuario usuario = (Usuario) session.getAttribute("usuario");
            List<Periodo> periodos = periodoDao.getPeriodoPropiedad(id_propiedad);
            List<Reserva> reservas = reservaDao.getReservasPropiedad(id_propiedad);

            // Saco la lista de las fechas inicio y fin.
            periodos = sacarPeriodosLibres(periodos, reservas);
            List<String> listaFechaLibre = listarFechaLibre(periodos);

            model.addAttribute("usuario", session.getAttribute("usuario"));
            model.addAttribute("propiedad", propiedad);
            model.addAttribute("fechalibre", listaFechaLibre);

            return "reserva/addreserva";
        }

        reservaDao.addReserva(reserva);

        //Generamos un correo y se lo enviamos
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        Usuario usuarioReserva = (Usuario) session.getAttribute("usuario");
        String idPropietario = propiedad.getId_nacional();
        Usuario propietario = usuarioDao.getUsuarioDNI(idPropietario);

        String nombreUsuairo = usuarioReserva.getNombre()+" "+usuarioReserva.getApellido();
        String emailUsuario = usuarioReserva.getEmail();

        String nombrePropietario = propietario.getNombre();
        String emailPropietario = propietario.getEmail();

        String tituloPropiedad = propiedad.getTitulo();
        String descripcionPropiedad = propiedad.getDescripcion();
        String direccionPropiedad = "C. "+propiedad.getCalle()+" Num. "+propiedad.getNumero()+" Planta "+propiedad.getPlanta()+" Ciudad: "+propiedad.getCiudad();
        int idPropiedad = propiedad.getId_propiedad();

        int numPersonasReserva = reserva.getNum_personas();
//        Date date =  new Date(reserva.getFecha_reserva().getTime());
//        String fechaReserva = format.format(date);
        String fechaInicioReserva = format.format(reserva.getFecha_inicio());
        String fechaFinReserva = format.format(reserva.getFecha_fin());
        double costeTotalReserva = reserva.getCoste_total();


        //Correo para el usuario que ha hecho la reserva
        String result = "Correo enviado con éxito";
        Mail mail = new Mail();
        mail.setRecipient(emailUsuario);
        mail.setSubject("Informacion de la reserva en EasyRent");
        String body = "Hola "+nombreUsuairo+"!<br /><br /> Este es un correo autogenerado con la informacion de su reserva de una propiedad desde la pagina <a href='http://localhost:8080'>EasyRent.com</a><br /><br /> Informacion de la propiedad: <br />Titulo: "+tituloPropiedad+"<br />Descripcion: "+descripcionPropiedad+"<br />Direccion: "+direccionPropiedad+"<br /><br />Informacion de la Reserva:<br />Fecha de la reserva: "+"<br />Fecha inicio: "+fechaInicioReserva+"<br />Fecha fin: "+fechaFinReserva+"<br />Num. personas: "+numPersonasReserva+"<br />Coste total de la reserva: "+costeTotalReserva+"euros<br /><br />Su reserva actualmente se en cuentra en estado: pendiente de validacion<br />En breve le enviaremos un correo informando sobre la resolucion de su reserva.<br /><br />Atentamente,<br /> El Equipo de Asistencia de EasyRent.com <br />";
        mail.setBodyText(body);
        mail.setAttachFile(null);
        boolean verificacion= true;
        System.out.println(" Correo para usuario que ha hecho la reserva enviado: "+ verificacion);
        verificacion = mail.sendEmail();
        if (!verificacion) { result="Hay un problema con nuestro servidor de correo, por favor comunique con nosotros por teléfono, perdone las molestias"; }
        System.out.println(" Correo para usuario que ha hecho la reserva enviado: " + verificacion);
        System.out.println(result);


        //Correo para el propietario de la propiedad de la reserva
        String result2 = "Correo enviado con éxito";
        Mail mail2 = new Mail();
        mail2.setRecipient(emailPropietario);
        mail2.setSubject("Nueva reserva de una de sus propiedades - Accion Requerida");
        String body2 = "Hola "+nombrePropietario+"!<br /><br /> Un usuario ha realizado una nueva reserva de una de sus propiedades, por favor verifique la informacion de la reserva y proceda, mediante el enlace, para aceptarla o rechazarla.<br /><br />Informacion de la propiedad: <br />Titulo: "+tituloPropiedad+"<br />Descripcion: "+descripcionPropiedad+"<br />Direccion: "+direccionPropiedad+"<br /><br />Informacion de la reserva:<br />Usuario que ha realizado la reserva: "+nombreUsuairo+"<br />Correo del usuario que ha realizado la reserva: "+emailUsuario+"<br /><br />Informacion de la Reserva:<br />Fecha de la reserva: "+"<br />Fecha inicio: "+fechaInicioReserva+"<br />Fecha fin: "+fechaFinReserva+"<br />Num. personas: "+numPersonasReserva+"<br />Coste total de la reserva: "+costeTotalReserva+"euros<br /><br />Para confirmar o rechazar la reserva haz clic en el siguiente enlace que le llevara a la pagina de la gestion de dicha propiedad <a href='http://localhost:8080/my/propiedad/"+idPropiedad+"/listreservaprop.html'>Gestion de la propiedad</a><br /><br /> Atentamente,<br /> El Equipo de Asistencia de EasyRent.com <br />";
        mail2.setBodyText(body2);
        mail2.setAttachFile(null);
        boolean verificacion2= true;
        System.out.println(" Correo para confirmar una reserva enviado al propietario:  "+ verificacion2);
        verificacion = mail2.sendEmail();
        if (!verificacion2) { result2="Hay un problema con nuestro servidor de correo, por favor comunique con nosotros por teléfono, perdone las molestias"; }
        System.out.println(" Correo para confirmar una reserva enviado al propietario: " + verificacion2);
        System.out.println(result2);


        return "redirect:../listreservas.html?success=reserva";
    }

    // Elimina la reserva de la base de datos.
    @RequestMapping(value = "/deletereserva/{num_seguimiento}")
    public String deleteReserva(HttpSession session, @PathVariable int num_seguimiento) {
        Usuario usuario = (Usuario) session.getAttribute("usuario");
        Reserva reserva = reservaDao.getReserva(num_seguimiento);

        if (usuario.getRol().equals("admin") || usuario.getId_nacional().equals(reserva.getId_nacional()))
            reservaDao.deleteReserva(reservaDao.getReserva(num_seguimiento));

        return "redirect:../listreservas.html";
    }

    // Confirmar una reserva.
    @RequestMapping(value="{id_propiedad}/confirmarreserva/{num_seguimiento}")
    public String confirmarReserva(HttpSession session, @PathVariable("id_propiedad") int id_propiedad,
                                   @PathVariable("num_seguimiento") int num_seguimiento) {

        Usuario usuario = (Usuario) session.getAttribute("usuario");
        Propiedad propiedad = propiedadDao.getPropiedad(id_propiedad);
        Reserva reserva = reservaDao.getReserva(num_seguimiento);


        if (usuario.getId_nacional().equals(propiedad.getId_nacional()) && propiedad.getId_propiedad() == reserva.getId_propiedad()) {
            reserva.setFecha_confirmacion(new Date());
            reserva.setEstado("CONFIRMADO");
            reservaDao.confirmarReserva(reserva);

            // Creo la factura.
            Factura factura = new Factura();
            Date fecha = new Date();
            factura.setNum_seguimiento(num_seguimiento);
            factura.setFecha_factura(fecha);
            facturaDao.addFactura(factura);

            factura = facturaDao.getFacturaNumeroSeguimiento(num_seguimiento);

//        <<<<<<<<<<<<<<< Daatos necesarios para generar la factura >>>>>>>>>>>>>>>>>>>
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

            String dniUsuario = reserva.getId_nacional();
            Usuario usuarioReserva = usuarioDao.getUsuarioDNI(dniUsuario);
            String nombreUsuairo = usuarioReserva.getNombre()+" "+usuarioReserva.getApellido();
            String emailUsuario = usuarioReserva.getEmail();
            String direccionUsuario = usuarioReserva.getDireccion();


            String idPropietario = propiedad.getId_nacional();
            Usuario propietario = usuarioDao.getUsuarioDNI(idPropietario);
            String nombrePropietario = propietario.getNombre()+" "+propietario.getApellido();
            String emailPropietario = propietario.getEmail();

            String tituloPropiedad = propiedad.getTitulo();
            String descripcionPropiedad = propiedad.getDescripcion();
            String direccionPropiedad = "C. "+propiedad.getCalle()+" Num. "+propiedad.getNumero()+" Planta "+propiedad.getPlanta()+" Ciudad: "+propiedad.getCiudad();

            int numSeguimiento = reserva.getNum_seguimiento();
            String fechaReserva = format.format(reserva.getFecha_reserva());
            String fechaConfirmacion = format.format(reserva.getFecha_confirmacion());
            int numPersonas = reserva.getNum_personas();
            String fechaInicioReserva = format.format(reserva.getFecha_inicio());
            String fechaFinReserva = format.format(reserva.getFecha_fin());
            double costeTotalReserva = reserva.getCoste_total();

            String fechaFactura = format.format(factura.getFecha_factura());

            //Generamos el documento pdf
//            Document document = new Document(new Rectangle(1440, 1440));
            Document document = new Document(PageSize.A4, 50, 50, 50, 50);
            PdfWriter writer = null;

            try{
                File f1 = new File("");
                String pathABS = f1.getAbsolutePath()+"\\src\\main\\webapp\\static\\pdfreservas\\"+"Reserva"+num_seguimiento+".pdf";
                System.out.println("path:"+ pathABS);
                File f2 = new File(pathABS);
                writer = PdfWriter.getInstance(document, new FileOutputStream(f2));
            }catch ( FileNotFoundException ex){
                ex.printStackTrace();
            }catch (DocumentException ex2){
                ex2.printStackTrace();
            }

            writer.setPdfVersion(PdfWriter.VERSION_1_6);
            document.open();
            try{
                document.add(new Paragraph("__________________________FACTURA__________________________\n\n Numero de seguimiento de la factura: "+factura.getNum_factura()+"\n Fecha de realizacion de la factura: "+fechaFactura+"\n Iva aplicado: "+factura.getIva()+"\n\n <<<<<Datos de la factura>>>>>\n >Datos del propietario:" +
                        "    -Nombre: "+nombrePropietario+"\n    -email: "+emailPropietario+"\n    -telefono: "+propietario.getNumero_telefono()+"\n>Datos del cliente:\n    -Nombre: "+nombreUsuairo+"\n    -DNI: "+dniUsuario+"\n    -Direccion: "+direccionUsuario+"\n    -email: "+emailUsuario+"\n>Informacion de la propiedad:\n    -titulo: "+tituloPropiedad+"\n    -Descripcion: "+descripcionPropiedad+"\n    -Dirreccion: "+direccionPropiedad+"\n>Informacion de la reserva:\n    -Numero de seguimiento: "+numSeguimiento+"\n    -Fecha reserva: "+fechaReserva+"\n    -Fecha confirmacion de la reserva: "+fechaConfirmacion+"\n    -Numero de personas: "+numPersonas+"\n    -Fecha inicio: "+fechaInicioReserva+"\n    -Fecha fin: "+fechaFinReserva+"\n----------------------------------------------------------------------------Coste TOTAL: "+costeTotalReserva));
            }catch (DocumentException ex3){
                ex3.printStackTrace();
            }
            document.close();
            System.out.println("Se ha generado un pdf con la informacion de la reserva");


//          Creamos el email y le añadimos el prdf de la factura
            String result = "Correo enviado con éxito";
            Mail mail = new Mail();
            mail.setRecipient(emailUsuario);
            mail.setSubject("Factura de una reserva de EasyReant");
            String body = "Hola "+nombreUsuairo+"!<br /><br /> Su reserva com el numero de seguimiento: "+numSeguimiento+" ha sido confirmada por el propietario de la propidad.<br /><br /> Le adjuntamos una factura en formato pdf.<br />Muchas gracias por confiar en nosotos<br /> <br /> Atentamente,<br /> El Equipo de Asistencia de EasyRent.com <br />";
            mail.setBodyText(body);
            mail.setAttachFile("\\static\\pdfreservas\\Reserva"+numSeguimiento+".pdf");
            boolean verificacion= true;
            System.out.println(" Correo con el pdf de la factura enviado al correo: "+emailUsuario+" Estado: "+ verificacion);
            verificacion = mail.sendEmail();
            if (!verificacion) { result="Hay un problema con nuestro servidor de correo, por favor comunique con nosotros por teléfono, perdone las molestias"; }
            System.out.println(" Correo con el pdf de la factura enviado al correo: "+emailUsuario+" Estado: "+ verificacion);
            System.out.println(result);

            return "redirect:../../../propiedad/" + id_propiedad + "/listreservaprop.html";
        }

        return "redirect:../../../propiedad/listpropiedades.html";
    }


    // ******************************** Metodos para sacar los periodos libres *****************************************
    private List<Periodo> sacarPeriodosLibres(List<Periodo> periodos, List<Reserva> reservas) {
        List<Periodo> listaPeriodosLibres = new ArrayList<Periodo>();
        ArrayList<Reserva> borrarReservas = new ArrayList<Reserva>();

        for (Periodo p : periodos) {
            // Borramos de la lista de reservas, las reservas del anterior periodo.
            for (Reserva borrar : borrarReservas) {
                reservas.remove(borrar);
            }
            borrarReservas.clear();

            if (reservas.isEmpty()) {
                listaPeriodosLibres.add(p);
            }
            else {
                for (Reserva r : reservas) {
                    if (r.getFecha_inicio().after(p.getFin())) {
                        break;
                    } else if (r.getFecha_inicio().equals(p.getInicio()) && r.getFecha_fin().equals(p.getFin())) {
                        p.setInicio(null);
                        p.setFin(null);
                        borrarReservas.add(r);
                        break;
                    } else if (r.getFecha_inicio().equals(p.getInicio()) && r.getFecha_fin().before(p.getFin())) {
                        p.setInicio(diaPosterior(r.getFecha_fin()));
                        borrarReservas.add(r);
                    } else if (r.getFecha_inicio().after(p.getInicio()) && r.getFecha_fin().before(p.getFin())) {
                        Periodo nuevoPeriodo = new Periodo();
                        nuevoPeriodo.setInicio(p.getInicio());
                        nuevoPeriodo.setFin(diaAnterior(r.getFecha_inicio()));
                        listaPeriodosLibres.add(nuevoPeriodo);

                        p.setInicio(diaPosterior(r.getFecha_fin()));
                        borrarReservas.add(r);
                    } else if (r.getFecha_inicio().after(p.getInicio()) && r.getFecha_fin().equals(p.getFin())) {
                        p.setFin(diaAnterior(r.getFecha_inicio()));
                        borrarReservas.add(r);
                    }
                }
                if (p.getInicio() != null && p.getFin() != null)
                    listaPeriodosLibres.add(p);
            }
        }

        return listaPeriodosLibres;
    }

    private Date diaAnterior(Date fechaModificar) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(fechaModificar);
        calendar.add(Calendar.DAY_OF_YEAR, -1);

        Date fecha = calendar.getTime();

        return fecha;
    }

    private Date diaPosterior(Date fechaModificar) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(fechaModificar);
        calendar.add(Calendar.DAY_OF_YEAR, 1);

        Date fecha = calendar.getTime();

        return fecha;
    }

    private List<String> listarFechaLibre(List<Periodo> periodos) {
        List<String> listaFechaInicio = new ArrayList<String>();

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        for (Periodo p : periodos) {
            String inicio = format.format(p.getInicio());
            String fin = format.format(p.getFin());

            String fecha = inicio + "  --->   " + fin;
            listaFechaInicio.add(fecha);
        }

        return listaFechaInicio;
    }
    // *****************************************************************************************************************
}
