package com.easyrent.web.controller;

import com.easyrent.web.dao.ValoracionDaoTemp;

import com.easyrent.web.domain.Usuario;
import com.easyrent.web.domain.Valoracion;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/my/valoracion")
public class MyValoracionController {
    private ValoracionDaoTemp valoracionDao;

    @Autowired
    public void setValoracionDao(ValoracionDaoTemp valoracionDao) {
        this.valoracionDao = valoracionDao;
    }

    // Muestra una lista de valoraciones del usuario o todas (administrador).
    @RequestMapping("/listvaloraciones")
    public String listValoracion(HttpSession session, Model model) {
        Usuario usuario = (Usuario) session.getAttribute("usuario");

        if (usuario.getRol().equals("admin")) {
            model.addAttribute("valoracion", valoracionDao.getValoracionesAdmin());
            model.addAttribute("usuario", usuario);
        }
        else {
            model.addAttribute("valoracion", valoracionDao.getValoracionesUser(usuario.getId_nacional()));
            model.addAttribute("usuario", usuario);
        }

        return "valoracion/listvaloracion";
    }

    // Elimina una valoracion de una propiedad.
    @RequestMapping(value = "/deletevaloracion/{id_propiedad}/{id_nacional}")
    public String processDelete(HttpSession session, @PathVariable int id_propiedad, @PathVariable String id_nacional) {
        Usuario usuario = (Usuario) session.getAttribute("usuario");
        Valoracion valoracion = valoracionDao.getValoracion(id_propiedad, id_nacional);

        if (usuario.getRol().equals("admin") || usuario.getId_nacional().equals(valoracion.getId_nacional()))
            valoracionDao.deleteValoracion(valoracionDao.getValoracion(id_propiedad, id_nacional));

        return "redirect:../../listvaloraciones.html";
    }
}

