package com.easyrent.web.controller;

import com.easyrent.web.dao.PropiedadDaoTemp;
import com.easyrent.web.dao.UsuarioDaoTemp;
import com.easyrent.web.domain.Contrasenya;
import com.easyrent.web.domain.Usuario;
import com.easyrent.web.email.Mail;
import com.easyrent.web.validation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.Date;
import java.text.SimpleDateFormat;

@Controller
@RequestMapping("/my/usuario")
public class MyUsuarioController {

    private UsuarioDaoTemp usuarioDao;
    private PropiedadDaoTemp propiedadDao;

    @Autowired
    public void setUsuarioDao(UsuarioDaoTemp usuarioDao, PropiedadDaoTemp propiedadDao) {
        this.usuarioDao = usuarioDao;
        this.propiedadDao = propiedadDao;
    }

    // Define el formato de la fecha.
    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false));
    }

    // Muestra una lista con todos los usuarios de la base de datos (solo administrador).
    @RequestMapping("/listusuarios")
    public String listusuario(HttpSession session, Model model) {
        model.addAttribute("usuarios", usuarioDao.getUsuarios());

        return "usuario/listusuario";
    }

    // Anyade un nuevo usuario a la base de datos (solo administrador).
    @RequestMapping(value = "/addusuario")
    public String addUsuario(HttpSession session, Model model) {
        model.addAttribute("usuario", new Usuario());

        return "usuario/addusuarioadmin";
    }

    @RequestMapping(value = "/addusuario", method = RequestMethod.POST)
    public String processAddSubmit(HttpSession session, @ModelAttribute("usuario") Usuario usuario, BindingResult bindingResult) {
		UsuarioAddAdminValidador usuarioValidator = new UsuarioAddAdminValidador(usuarioDao);
	    usuarioValidator.validate(usuario, bindingResult);

		if (bindingResult.hasErrors())
            return "usuario/addusuarioadmin";

        Date fecha = new Date();
        usuario.setFecha_registro(fecha);
        usuarioDao.addUsuario(usuario);



        //Generamos un correo y se lo enviamos
        String nombre = usuario.getNombre();
        String email = usuario.getEmail();

        int idUser = usuario.getId_usuario();

        String result = "Correo enviado con éxito";
        Mail mail = new Mail();
        mail.setRecipient(usuario.getEmail());
        mail.setSubject("Informacion sobre su nueva cuenta de EasyRent.com - Accion Requerida");
        String body = "Hola "+nombre+"!<br /><br /> Se le ha creado una nueva cuenta en la Web <a href='http://localhost:8080'>EasyRent.com</a><br /><br /> Para acceder a su cuenta utilice estos credenciales: <br />Nombre de usuario: "+usuario.getUsuario()+"<br />Contraseña: "+usuario.getId_nacional()+"<br /><br />Para mejorar la seguridad sobre su cuenta, le recomentamos encarecidamente que despues del primer acceso cambie su contraseña.<br /><br /> Puede encontrar mas informacion sobre este proceso en nuestro articulo sobre la <a href='#'>Creacion de una cuenta de EasyRent.com</a> <br /><br /> Atentamente,<br /> El Equipo de Asistencia de EasyRent.com <br />";
        mail.setBodyText(body);
        boolean verificacion= true;
        System.out.println(" correo enviado  "+ verificacion);
        verificacion = mail.sendEmail();
        if (!verificacion) { result="Hay un problema con nuestro servidor de correo, por favor comunique con nosotros por teléfono, perdone las molestias"; }
        System.out.println(" correo enviado  "+ verificacion);




        return "redirect:listusuarios.html";
    }

    // Modifica los propios datos del usuario (user) o por administrador
    @RequestMapping(value = "/updateusuario/{id_usuario}", method = RequestMethod.GET)
    public String updateUsuario(HttpSession session, Model model, @PathVariable int id_usuario) {
        Usuario usuarioAutent = (Usuario) session.getAttribute("usuario");
        Usuario usuarioMod = usuarioDao.getUsuarioDatos(id_usuario);

        if (usuarioAutent.getRol().equals("admin")) {
            model.addAttribute("usuario", usuarioMod);

            return "usuario/updatedatosusuarioadmin";
        }
        else if (usuarioAutent.getId_nacional().equals(usuarioMod.getId_nacional())) {
            model.addAttribute("usuario", usuarioMod);

            return "usuario/updatedatosusuariouser";
        }

        return "redirect:../../usermenu.html";
    }

    @RequestMapping(value = "/updateusuario/{id_usuario}", method = RequestMethod.POST)
    public String processUpdateSubmit(HttpSession session, @PathVariable int id_usuario,
                                      @ModelAttribute("usuario") Usuario usuarioModificar, BindingResult bindingResult) {
        Usuario usuarioAccion = (Usuario) session.getAttribute("usuario");
        Usuario usuarioOriginal = usuarioDao.getUsuarioDatos(id_usuario);

        // Comprueba que los datos son correctos.
        if (usuarioAccion.getRol().equals("admin")) {
            UsuarioUpdateAdminValidador usuarioValidator = new UsuarioUpdateAdminValidador(usuarioDao, usuarioOriginal);
            usuarioValidator.validate(usuarioModificar, bindingResult);

            if (bindingResult.hasErrors())
                return "usuario/updatedatosusuarioadmin";

            // Cambio los datos del usuario por los modificados.
            usuarioOriginal.setId_nacional(usuarioModificar.getId_nacional());
            usuarioOriginal.setNombre(usuarioModificar.getNombre());
            usuarioOriginal.setApellido(usuarioModificar.getApellido());
            usuarioOriginal.setEmail(usuarioModificar.getEmail());
            usuarioOriginal.setDireccion(usuarioModificar.getDireccion());
            usuarioOriginal.setNumero_telefono(usuarioModificar.getNumero_telefono());
            usuarioOriginal.setRol(usuarioModificar.getRol());
            usuarioOriginal.setEs_activo(usuarioModificar.isEs_activo());

            // Si el admin cambia sus propios datos, cambio los datos de la session.
            if (usuarioOriginal.getUsuario().equals(usuarioAccion.getUsuario()))
                session.setAttribute("usuario", usuarioOriginal);
        }

        else {
            UsuarioUpdateUserValidador usuarioValidator = new UsuarioUpdateUserValidador(usuarioDao, usuarioOriginal);
            usuarioValidator.validate(usuarioModificar, bindingResult);

            if (bindingResult.hasErrors())
                return "usuario/updatedatosusuariouser";

            // Cambio los datos del usuario por los modificados.
            usuarioOriginal.setId_nacional(usuarioModificar.getId_nacional());
            usuarioOriginal.setNombre(usuarioModificar.getNombre());
            usuarioOriginal.setApellido(usuarioModificar.getApellido());
            usuarioOriginal.setEmail(usuarioModificar.getEmail());
            usuarioOriginal.setDireccion(usuarioModificar.getDireccion());
            usuarioOriginal.setNumero_telefono(usuarioModificar.getNumero_telefono());

            // Cambio los datos de la session.
            session.setAttribute("usuario", usuarioOriginal);
        }

        // Guarda los cambios realizados.
        usuarioDao.updateUsuario(usuarioOriginal);

        // Redireccion de pagina segun el rol del usuario.
        if (usuarioAccion.getRol().equals("admin"))
            return "redirect:../listusuarios.html";
        else
            return "redirect:../userprofile.html";
    }

    // Elimina de la base de datos el usuario y todas sus propiedades.
    @RequestMapping(value = "/deleteusuario/{id_usuario}")
    public String deleteUsuario(HttpSession session, @PathVariable int id_usuario) {
        Usuario usuarioAutent = (Usuario) session.getAttribute("usuario");
        Usuario usuarioMod = usuarioDao.getUsuarioDatos(id_usuario);

        if (usuarioAutent.getRol().equals("admin") || usuarioAutent.getId_nacional().equals(usuarioMod.getId_nacional())) {
            if (propiedadDao.getPropiedadesUser(usuarioMod.getId_nacional()).isEmpty()) {
                usuarioDao.deleteUsuario(usuarioMod);
            }
            else {
                propiedadDao.deleteTodasPropiedades(usuarioMod.getId_nacional());
                usuarioDao.deleteUsuario(usuarioMod);
            }
        }

        if (usuarioAutent.getRol().equals("admin"))
            return "redirect:../listusuarios.html";
        else
            return "redirect:logout.html";
    }

    // Muestra la pagina del perfil del usuario.
    @RequestMapping(value = "/userprofile", method = RequestMethod.GET)
    public String perfilusuario(HttpSession session, Model model) {
        Usuario usuario = (Usuario) session.getAttribute("usuario");
        model.addAttribute("usuario", usuarioDao.getUsuarioDatos(usuario.getId_usuario()));

        return "usuario/userprofile";
    }

    // Cambiar la contrasenya del usuario.
    @RequestMapping(value = "/{id_usuario}/cambiarcontrasenya")
    public String cambiarContrasenya(HttpSession session, @PathVariable int id_usuario, Model model) {
        Usuario usuarioAutent = (Usuario) session.getAttribute("usuario");
        Usuario usuarioMod = usuarioDao.getUsuarioDatos(id_usuario);

        if (usuarioAutent.getId_nacional().equals(usuarioMod.getId_nacional())) {
            model.addAttribute("contrasenya", new Contrasenya());
            model.addAttribute("usuario", usuarioMod);

            return "contrasenya/cambiarContrasenyaUser";
        }
        else if (usuarioAutent.getRol().equals("admin") && usuarioAutent.getId_usuario() != usuarioMod.getId_usuario()) {
            model.addAttribute("contrasenya", new Contrasenya());
            model.addAttribute("usuariomod", usuarioMod);

            return "contrasenya/cambiarContrasenyaAdmin";
        }

        return "redirect:../userprofile.html";
    }

    @RequestMapping(value = "/{id_usuario}/cambiarcontrasenya", method = RequestMethod.POST)
    public String processCambiarContrasenyaSubmit(HttpSession session, @ModelAttribute("contrasenya") Contrasenya contrasenya,
                                                  BindingResult bindingResult, @PathVariable int id_usuario) {
        Usuario usuarioAutent = (Usuario) session.getAttribute("usuario");
        Usuario usuarioMod = usuarioDao.getUsuarioDatos(id_usuario);

        if (usuarioAutent.getId_nacional().equals(usuarioMod.getId_nacional())) {
            ContrasenyaValidadorUser contrasenyaValidator = new ContrasenyaValidadorUser(usuarioMod);
            contrasenyaValidator.validate(contrasenya, bindingResult);

            if (bindingResult.hasErrors())
                return "contrasenya/cambiarContrasenyaUser";
        }
        else if (usuarioAutent.getRol().equals("admin") && usuarioAutent.getId_usuario() != usuarioMod.getId_usuario()) {
            ContrasenyaValidadorAdmin contrasenyaValidator = new ContrasenyaValidadorAdmin();
            contrasenyaValidator.validate(contrasenya, bindingResult);

            if (bindingResult.hasErrors())
                return "contrasenya/cambiarContrasenyaAdmin";
        }

        usuarioMod.setContrasenya(contrasenya.getContrasenyaNueva());
        usuarioDao.updateUsuarioContrasenya(usuarioMod);

        // Guardo los datos del usuario en la session.
        if (usuarioAutent.getId_nacional().equals(usuarioMod.getId_nacional())) {
            session.setAttribute("usuario", usuarioMod);

            return "redirect:../userprofile.html";
        }

        // Redireccion de pagina segun si es administrdor o usuario.
        return "redirect:../listusuarios.html";
    }
}
