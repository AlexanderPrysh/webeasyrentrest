package com.easyrent.web.controller;

import com.easyrent.web.dao.*;
import com.easyrent.web.domain.*;
import com.easyrent.web.email.Mail;
import com.easyrent.web.validation.ContrasenyaOlvidadaValidador;
import com.easyrent.web.validation.UsuarioAddUserValidador;
import com.easyrent.web.validation.ValoracionValidador;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Date;
import javax.mail.internet.AddressException;
import javax.servlet.http.HttpSession;

import org.springframework.web.bind.annotation.ResponseBody;


@Controller
@RequestMapping("")
public class PublicoController {

    private PropiedadDaoTemp propiedadDao;
    private ValoracionDaoTemp valoracionDao;
    private ImagenDaoTemp imagenDao;
    private PeriodoDaoTemp periodoDao;
    private UsuarioDaoTemp usuarioDao;
    private ReservaDaoTemp reservaDao;
    private FacturaDaoTemp facturaDao;

    @Autowired
    public void setPropiedadDao(PropiedadDaoTemp propiedadDao, ValoracionDaoTemp valorarcionDao, ImagenDaoTemp imagenDao,
                                PeriodoDaoTemp periodoDao, UsuarioDaoTemp usuarioDao, ReservaDaoTemp reservaDao,
                                FacturaDaoTemp facturaDao) {
        this.propiedadDao = propiedadDao;
        this.valoracionDao = valorarcionDao;
        this.imagenDao = imagenDao;
        this.periodoDao = periodoDao;
        this.usuarioDao = usuarioDao;
        this.reservaDao = reservaDao;
        this.facturaDao =facturaDao;
    }

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false));
    }

    @RequestMapping(value = "/index", method = RequestMethod.GET)
    public String preparaIMG(Model model){
        model.addAttribute("indeximages", imagenDao.getImagenesNuevas());
        return "public/index";
    }

    @RequestMapping(value = "/buscador", method = RequestMethod.GET)
    public String preparaForm(Model model){
        model.addAttribute("propiedad", new Propiedad());
        return "public/buscador";
    }

    // Muestra la informacion de una propiedad con sus valoraciones al publico.
    @RequestMapping(value = "/showpropiedad/{id_propiedad}")
    public String showPropiedad(Model model, @PathVariable int id_propiedad) {
        List<Periodo> periodos = periodoDao.getPeriodoPropiedad(id_propiedad);
        List<Reserva> reservas = reservaDao.getReservasPropiedad(id_propiedad);
        List<Valoracion> valoraciones = valoracionDao.getValoracionesPropiedad(id_propiedad);
        Propiedad propiedad = propiedadDao.getPropiedad(id_propiedad);

        // Saco las listas de las fechas inicio y fin
        periodos = sacarPeriodosLibres(periodos, reservas);
        List<String> listaFechaLibre = listarFechaLibre(periodos);

        // Saco la media de los votos de las valoraciones de la propiedad.
        double media = mediaVoto(valoraciones);

        if (propiedad.isEs_activo() == true) {
            model.addAttribute("propiedad", propiedadDao.getPropiedad(id_propiedad));
            model.addAttribute("imagen", imagenDao.getImagenPropiedad(id_propiedad));
            model.addAttribute("fechalibre", listaFechaLibre);
            model.addAttribute("valoracion", valoraciones);
            model.addAttribute("nuevavaloracion", new Valoracion());
            model.addAttribute("media", media);

            return "public/showpropiedad";
        }

        return "redirect:../index.html";
    }

    @RequestMapping(value="/showpropiedad/{id_propiedad}", method=RequestMethod.POST)
    public String processAddValoracionSubmit(HttpSession session, Model model, @PathVariable int id_propiedad,
                                             @ModelAttribute("nuevavaloracion") Valoracion valoracion, BindingResult bindingResult) {
        if (session.getAttribute("usuario") == null) {
            return "redirect:../login.html";
        }

        Usuario usuario = (Usuario) session.getAttribute("usuario");
        valoracion.setId_nacional(usuario.getId_nacional());
        valoracion.setId_propiedad(id_propiedad);

        ValoracionValidador valoracionValidator = new ValoracionValidador(valoracionDao, facturaDao);
        valoracionValidator.validate(valoracion, bindingResult);

        if (bindingResult.hasErrors()) {
            List<Periodo> periodos = periodoDao.getPeriodoPropiedad(id_propiedad);
            List<Reserva> reservas = reservaDao.getReservasPropiedad(id_propiedad);
            List<Valoracion> valoraciones = valoracionDao.getValoracionesPropiedad(id_propiedad);

            // Saco las listas de las fechas inicio y fin
            periodos = sacarPeriodosLibres(periodos, reservas);
            List<String> listaFechaLibre = listarFechaLibre(periodos);

            double media = mediaVoto(valoraciones);

            model.addAttribute("propiedad", propiedadDao.getPropiedad(id_propiedad));
            model.addAttribute("imagen", imagenDao.getImagenPropiedad(id_propiedad));
            model.addAttribute("fechalibre", listaFechaLibre);
            model.addAttribute("valoracion", valoraciones);
            model.addAttribute("media", media);

            return "public/showpropiedad";
        }

        session.setAttribute("valoracion", valoracion);

        return "redirect:../my/propiedad/" + id_propiedad + "/addvaloracion.html";
    }

    // Registra un nuevo usuario a la base de datos.
    @RequestMapping(value = "/addusuario")
    public String addUsuario(Model model) {
        model.addAttribute("usuario", new Usuario());

        return "usuario/addusuariouser";
    }

    @RequestMapping(value = "/addusuario", method = RequestMethod.POST)
    public String processAddSubmit(@ModelAttribute("usuario") Usuario usuario, BindingResult bindingResult) {
		UsuarioAddUserValidador usuarioValidator = new UsuarioAddUserValidador(usuarioDao);
	    usuarioValidator.validate(usuario, bindingResult);

		if (bindingResult.hasErrors())
			return "usuario/addusuariouser";

        Date fecha = new Date();
        usuario.setFecha_registro(fecha);
        usuarioDao.addUsuarioUser(usuario);


        usuario = usuarioDao.getUsuarioNombreUsuario(usuario.getUsuario());

        //Generamos un correo y se lo enviamos
        String nombre = usuario.getNombre();
        String emailUsuario = usuario.getEmail();
        int idUser = usuario.getId_usuario();

        String result = "Correo enviado con éxito";
        Mail mail = new Mail();
        mail.setRecipient(emailUsuario);
        mail.setSubject("Correo de verificacion de tu cuenta de EasyRent.com - Accion Requerida");
        String body = "Hola "+nombre+"!<br /><br /> Haz clic en el siguiente enlace para verificar la direccion de e-mail y activar el usuario de EasyRent.com <br /> http://localhost:8080/usuario/"+idUser+"/activar.html <br /><br /> Verificar la direccion de e-mail permitira añadir una capa de seguridad a esta cuenta de EasyRent.com, por lo que recomendamos encarecidamente realizar este proceso.<br /><br /> Puedes encontrar mas informacion acerca de la Verificacion de la cuenta de e-mail en nuestro articulo sobre la <a href='#'>Creacion de una cuenta de EasyRent.com</a> <br /><br /> Atentamente,<br /> El Equipo de Asistencia de EasyRent.com <br />";
        mail.setBodyText(body);
        boolean verificacion= true;
        System.out.println(" Correo para la confirmacion de la cuenta enviado: "+ verificacion);
        verificacion = mail.sendEmail();
        if (!verificacion) { result="Hay un problema con nuestro servidor de correo, por favor comunique con nosotros por teléfono, perdone las molestias"; }
        System.out.println(" Correo para la confirmacion de la cuenta enviado: " + verificacion);
        System.out.println(result);

        return "redirect:login.html";
    }

    @RequestMapping(value = "/usuario/{id_usuario}/activar", method = RequestMethod.GET)
    public String activarUsuario(@PathVariable int id_usuario) {
        Usuario usuario = usuarioDao.getUsuarioDatos(id_usuario);

        if (usuario !=  null) {
            usuario.setEs_activo(true);
            usuarioDao.updateUsuario(usuario);

            return "public/cuentaActivada";
        }

        return "public/noExisteUsuario";
    }

    // Recuperar contrasenya olvidada.
    @RequestMapping(value = "/contrasenya/recuperarcontrasenya")
    public String recuperarContrasenya(Model model) {
        model.addAttribute("usuario", new Usuario());

        return "contrasenya/recuperarcontrasenya";
    }

    @RequestMapping(value = "/contrasenya/recuperarcontrasenya", method = RequestMethod.POST)
    public String recuperarContrasenyaSubmit(@ModelAttribute("usuario") Usuario usuario, BindingResult bindingResult){
        ContrasenyaOlvidadaValidador contrasenyaValidator = new ContrasenyaOlvidadaValidador(usuarioDao);
        contrasenyaValidator.validate(usuario, bindingResult);

        if(bindingResult.hasErrors()){
            return "contrasenya/recuperarcontrasenya";
        }

        usuario = usuarioDao.getUsuarioEmail(usuario.getEmail());
        usuario.setContrasenya("88888888");
        usuarioDao.updateUsuario(usuario);

        // Aqui envias el correo, sacas el nombre del usuario del usuario anterior y la contrasenya en claro
        // 88888888 (no hacer .getContrasenya que te da la contrasenya cifrada.)
        String nombreUsuario = usuario.getNombre();
        String emailUsuario = usuario.getEmail();
        String result = "Correo enviado con éxito";
        Mail mail = new Mail();
        mail.setRecipient(emailUsuario);
        mail.setSubject("Nueva contraseña para accedr en EasyRent.com - importante");
        String body = "Hola "+nombreUsuario+"!<br /><br /> La nueva contraseña para su 'usuario' "+usuario.getUsuario()+" que servirá para poder iniciar sesion en nuestra pagina EasyRent es la siguiente:<br /> Nueva contraseña: 88888888<br /><br /> Le pedimos una vez que acceda a su perfil que cambie de la contraseña, si no lo hace puede que su cuenta este en peligro y pueda perder informacion importante.<br /><br /> Atentamente,<br /> El Equipo de Asistencia de EasyRent.com <br />";
        mail.setBodyText(body);
        boolean verificacion= true;
        System.out.println(" Correo con la nueva contraseña enviado: "+ verificacion);
        verificacion = mail.sendEmail();
        if (!verificacion) { result="Hay un problema con nuestro servidor de correo, por favor comunique con nosotros por teléfono, perdone las molestias"; }
        System.out.println(" Correo con la nueva contraseña enviado: " + verificacion);
        System.out.println(result);

        return "redirect:enviadocontrasenya.html";
    }

    @RequestMapping(value = "/contrasenya/enviadocontrasenya")
    public String enviadoContrasenya(Model model) {

        return "contrasenya/enviadoContrasenya";
    }


    @RequestMapping(value = "/ajaxmailcontact", method = RequestMethod.POST)
    @ResponseBody
    public String sendmail( HttpServletRequest request) throws AddressException {

        String nombre =request.getParameter("nombre");
        String email =request.getParameter("email");
        String mensaje =request.getParameter("mensaje");
        String result = "Correo enviado con éxito";
        Mail mail = new Mail();
        mail.setRecipient("easyrentproyect@gmail.com");
        mail.setSubject("Contacto con el servicio de Informacion de EasyRent");

        String body = nombre +", con el correo: "+email+" ha intentado contactar con el servicio de informacion de EasyRent.<br /> Pide información sobre : "+mensaje;
        mail.setBodyText(body);

        mail.setAttachFile(null);
//        mail.setAttachFile("\\static\\pdfreservas\\Reserva66.pdf");

        boolean verificacion= true;
        System.out.println(" Enviado el correo para contactar con el servicio de Informacion de EasyRent "+ verificacion);
        verificacion = mail.sendEmail();
        if (!verificacion) { result="Hay un problema con nuestro servidor de correo, por favor comunique con nosotros por teléfono, perdone las molestias"; }
        System.out.println(" Enviado el correo para contactar con el servicio de Informacion de EasyRent "+ verificacion);
        System.out.println(result);

        return "redirect:index.jsp";
    }


    // ******************************** Metodos para sacar los periodos libres *****************************************
    private List<Periodo> sacarPeriodosLibres(List<Periodo> periodos, List<Reserva> reservas) {
        List<Periodo> listaPeriodosLibres = new ArrayList<Periodo>();
        ArrayList<Reserva> borrarReservas = new ArrayList<Reserva>();

        for (Periodo p : periodos) {
            // Borramos de la lista de reservas, las reservas del anterior periodo.
            for (Reserva borrar : borrarReservas) {
                reservas.remove(borrar);
            }
            borrarReservas.clear();

            if (reservas.isEmpty()) {
                listaPeriodosLibres.add(p);
            }
            else {
                for (Reserva r : reservas) {
                    if (r.getFecha_inicio().after(p.getFin())) {
                        break;
                    } else if (r.getFecha_inicio().equals(p.getInicio()) && r.getFecha_fin().equals(p.getFin())) {
                        p.setInicio(null);
                        p.setFin(null);
                        borrarReservas.add(r);
                        break;
                    } else if (r.getFecha_inicio().equals(p.getInicio()) && r.getFecha_fin().before(p.getFin())) {
                        p.setInicio(diaPosterior(r.getFecha_fin()));
                        borrarReservas.add(r);
                    } else if (r.getFecha_inicio().after(p.getInicio()) && r.getFecha_fin().before(p.getFin())) {
                        Periodo nuevoPeriodo = new Periodo();
                        nuevoPeriodo.setInicio(p.getInicio());
                        nuevoPeriodo.setFin(diaAnterior(r.getFecha_inicio()));
                        listaPeriodosLibres.add(nuevoPeriodo);

                        p.setInicio(diaPosterior(r.getFecha_fin()));
                        borrarReservas.add(r);
                    } else if (r.getFecha_inicio().after(p.getInicio()) && r.getFecha_fin().equals(p.getFin())) {
                        p.setFin(diaAnterior(r.getFecha_inicio()));
                        borrarReservas.add(r);
                    }
                }
                if (p.getInicio() != null && p.getFin() != null)
                    listaPeriodosLibres.add(p);
            }
        }

        return listaPeriodosLibres;
    }

    private Date diaAnterior(Date fechaModificar) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(fechaModificar);
        calendar.add(Calendar.DAY_OF_YEAR, -1);

        Date fecha = calendar.getTime();

        return fecha;
    }

    private Date diaPosterior(Date fechaModificar) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(fechaModificar);
        calendar.add(Calendar.DAY_OF_YEAR, 1);

        Date fecha = calendar.getTime();

        return fecha;
    }

    private List<String> listarFechaLibre(List<Periodo> periodos) {
        List<String> listaFechaInicio = new ArrayList<String>();

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        for (Periodo p : periodos) {
            String inicio = format.format(p.getInicio());
            String fin = format.format(p.getFin());

            String fecha = inicio + "   --->  " + fin;
            listaFechaInicio.add(fecha);
        }

        return listaFechaInicio;
    }
    // *****************************************************************************************************************

    private double mediaVoto(List<Valoracion> valoraciones) {
        if (valoraciones.isEmpty()) return 0.0;

        double media = 0.0;

        for (Valoracion val : valoraciones) {
            media += val.getVoto();
        }

        return media / valoraciones.size();
    }
}
