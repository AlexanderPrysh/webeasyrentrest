package com.easyrent.web.controller;

import com.easyrent.web.dao.ImagenDaoTemp;
import com.easyrent.web.dao.PropiedadDaoTemp;
import com.easyrent.web.domain.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/my/imagen")
public class MyImagenController {

    private ImagenDaoTemp imagenDao;
    private PropiedadDaoTemp propiedadDao;

    @Autowired
    public void setImagenDao(ImagenDaoTemp imagenDao, PropiedadDaoTemp propiedadDao) {
        this.imagenDao = imagenDao;
        this.propiedadDao = propiedadDao;
    }

    // Muestra una lista de todas las imagenes de la base de datos..
    @RequestMapping("/listimagenes")
    public String listImagen(HttpSession session, Model model) {
        Usuario usuario = (Usuario) session.getAttribute("usuario");

        if (usuario.getRol().equals("admin"))
            model.addAttribute("imagen", imagenDao.getImagenesAdmin());
        else
            model.addAttribute("imagen", imagenDao.getImagenesUser(usuario.getId_nacional()));

        return "imagen/listimagen";
    }
}
