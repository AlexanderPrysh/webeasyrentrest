package com.easyrent.web.filtros;

import com.easyrent.web.domain.Usuario;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AddUsuarioPublicoFiltro implements Filter {


    private FilterConfig filterConfig = null;

//    public FilterConfig getFilterConfig() {
//        return filterConfig;
//    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        this.filterConfig = filterConfig;
    }

    @Override
    public void destroy() {
        this.filterConfig = null;
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        Usuario usuario = (Usuario) request.getSession().getAttribute("usuario");

        if (usuario == null)
            filterChain.doFilter(servletRequest, servletResponse);
        else {
            if (usuario.getRol().equals("admin"))
                response.sendRedirect("/my/dashboard.html");
            else
                response.sendRedirect("/my/usermenu.html");
        }
    }
}
