package com.easyrent.web.dao;

import com.easyrent.web.domain.Propiedad;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Repository
public class PropiedadDaoTemp {
	private JdbcTemplate jdbcTemplate;

	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	private static final class PropiedadMapper implements RowMapper<Propiedad> {
		public Propiedad mapRow(ResultSet rs, int rowNum) throws SQLException {
			Propiedad propiedad = new Propiedad();
			
			propiedad.setId_propiedad(rs.getInt("id_propiedad"));
			propiedad.setTitulo(rs.getString("titulo"));
			propiedad.setId_nacional(rs.getString("id_nacional"));
			propiedad.setDescripcion(rs.getString("descripcion"));
			propiedad.setTipo_propiedad(rs.getString("tipo_propiedad"));
			propiedad.setCapacidad(rs.getInt("capacidad"));
			propiedad.setNum_habitaciones(rs.getInt("num_habitaciones"));
			propiedad.setNum_banyos(rs.getInt("num_banyos"));
			propiedad.setNum_camas(rs.getInt("num_camas"));
			propiedad.setMetros_cuadrados(rs.getInt("metros_cuadrados"));
			propiedad.setCalle(rs.getString("calle"));
			propiedad.setNumero(rs.getInt("numero"));
			propiedad.setPlanta(rs.getInt("planta"));
			propiedad.setCiudad(rs.getString("ciudad"));
			propiedad.setPrecio_diario(rs.getDouble("precio_diario"));
			propiedad.setEs_activo(rs.getBoolean("es_activo"));

			return propiedad;
		}
	}


	// Devuelve un listado con las propiedades que hacen match con el filtro elegido.
    // "select * from propiedad WHERE ciudad = ? AND precio_diario BETWEEN ? AND ? order by id_propiedad;"
	public List<Propiedad> getPropiedadSearch(String tipoProp, String ciudad, Double minPrice,
											  Double maxPrice, int minhab, int maxhab, int minM2, int maxM2, String descrip) {

        String searchBase = "select * from propiedad where id_propiedad IS NOT NULL";
        Object[] variables = new Object[]{};

        if (tipoProp != null && !tipoProp.isEmpty()) {
            searchBase+=" AND tipo_propiedad = ?"; //selectable siempre pasa por aqui
            Object[] variablesN = appendValue(variables, tipoProp);
            variables=variablesN;
        }
        if (ciudad != null && !ciudad.isEmpty()) {
            searchBase+=" AND ciudad = ?";
            Object[] variablesN = appendValue(variables, ciudad);
            variables=variablesN;
        }
        if (minPrice != 0 && maxPrice != 0) {
            searchBase+=" AND precio_diario BETWEEN ? AND ?";
            Object[] variablesN = appendValue(variables, minPrice);
            variables=variablesN;
            Object[] variablesX = appendValue(variables, maxPrice);
            variables=variablesX;
        }
        if (minhab != 0 && maxhab != 0) {
            searchBase+=" AND num_habitaciones BETWEEN ? AND ?";
            Object[] variablesN = appendValue(variables, minhab);
            variables=variablesN;
            Object[] variablesX = appendValue(variables, maxhab);
            variables=variablesX;
        }
        if (minM2 != 0 && maxM2 != 0) {
            searchBase+=" AND metros_cuadrados BETWEEN ? AND ?";
            Object[] variablesN = appendValue(variables, minM2);
            variables=variablesN;
            Object[] variablesX = appendValue(variables, maxM2);
            variables=variablesX;
        }
        if (descrip != null && !descrip.isEmpty()) {
            searchBase+=" AND descripcion LIKE ?";
            Object[] variablesN = appendValue(variables, "%" + descrip + "%");
            variables=variablesN;
        }
        searchBase+=" AND es_activo = TRUE ORDER BY titulo;";

        System.out.println("#####################a ver si sale esto###########################");
        System.out.println("sB = " + searchBase);

        for (Object temp : variables) {
            System.out.println("nV = "+temp);
        }

		return this.jdbcTemplate.query(searchBase, variables ,new PropiedadMapper());
	}

    //funcion para añadir nuevo dato al objeto de las variables
    private Object[] appendValue(Object[] obj, Object newObj) {
        ArrayList<Object> temp = new ArrayList<Object>(Arrays.asList(obj));
        temp.add(newObj);
        return temp.toArray();
    }

	// Devuelve un listado con todas las propiedades de la base de datos.
	public List<Propiedad> getPropiedadesAdmin() {
		return this.jdbcTemplate.query("select * from propiedad order by id_propiedad;", new PropiedadMapper());
	}

	// Devuelve un listado con todas las propiedades de un usuario..
	public List<Propiedad> getPropiedadesUser(String id_nacional) {
		return this.jdbcTemplate.query("select * from propiedad where id_nacional = ? order by id_propiedad;",
					new Object[] {id_nacional}, new PropiedadMapper());
	}

	// Devuelve los datos de una propiedad.
	public Propiedad getPropiedad(int id_propiedad) {
		try {
			return this.jdbcTemplate.queryForObject("select * from propiedad where id_propiedad = ?;",
					new Object[] { id_propiedad }, new PropiedadMapper());
		}
		catch(EmptyResultDataAccessException e) {
			return null;
		}
	}

	// Devuelve los datos de una propiedad a partir de su direccion.
	public Propiedad getPropiedadDireccion(String calle, int numero, int planta, String ciudad) {
		try {
			return this.jdbcTemplate.queryForObject("select * from propiedad where calle = ? && numero = ? && planta = ? && ciudad = ?;",
					new Object[] {calle, numero, planta, ciudad}, new PropiedadMapper());
		}
		catch(EmptyResultDataAccessException e) {
			return null;
		}
	}

	// Anyade una nueva propiedad a la base de datos.
	public void addPropiedad(Propiedad propiedad) {
		this.jdbcTemplate.update("insert into propiedad(titulo, id_nacional, descripcion, tipo_propiedad, "
				+ "capacidad, num_habitaciones, num_banyos, num_camas, metros_cuadrados, calle, numero,  "
				+ "planta, ciudad, precio_diario, es_activo) "
				+ "values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);",
				propiedad.getTitulo(),
				propiedad.getId_nacional(),
				propiedad.getDescripcion(),
				propiedad.getTipo_propiedad(),
				propiedad.getCapacidad(),
				propiedad.getNum_habitaciones(),
				propiedad.getNum_banyos(),
				propiedad.getNum_camas(),
				propiedad.getMetros_cuadrados(),
				propiedad.getCalle(),
				propiedad.getNumero(),
				propiedad.getPlanta(),
				propiedad.getCiudad(),
				propiedad.getPrecio_diario(),
				true);
	}

	// Modifica los datos de una propiedad.
	public void updatePropiedad(Propiedad propiedad) {
		this.jdbcTemplate.update("update propiedad "
				+ "set titulo = ?, "
				+ "id_nacional = ?, "
				+ "descripcion = ?, "
				+ "tipo_propiedad = ?, "
				+ "capacidad = ?, "
				+ "num_habitaciones = ?, "
				+ "num_banyos = ?, "
				+ "num_camas = ?, "
				+ "metros_cuadrados = ?, "
				+ "calle = ?, "
				+ "numero = ?, "
				+ "planta = ?, "
				+ "ciudad = ?, "
				+ "precio_diario = ?, "
				+ "es_activo = ? "
				+ "where id_propiedad = ?;",

				propiedad.getTitulo(),
				propiedad.getId_nacional(),
				propiedad.getDescripcion(),
				propiedad.getTipo_propiedad(),
				propiedad.getCapacidad(),
				propiedad.getNum_habitaciones(),
				propiedad.getNum_banyos(),
				propiedad.getNum_camas(),
				propiedad.getMetros_cuadrados(),
				propiedad.getCalle(),
				propiedad.getNumero(),
				propiedad.getPlanta(),
				propiedad.getCiudad(),
				propiedad.getPrecio_diario(),
				true,
				propiedad.getId_propiedad());
	}

	// Desactiva una propiedad, cambia el estado a false.
	public void activardesactivarPropiedad(Propiedad propiedad) {
		this.jdbcTemplate.update("update propiedad "
						+ "set titulo = ?, "
						+ "id_nacional = ?, "
						+ "descripcion = ?, "
						+ "tipo_propiedad = ?, "
						+ "capacidad = ?, "
						+ "num_habitaciones = ?, "
						+ "num_banyos = ?, "
						+ "num_camas = ?, "
						+ "metros_cuadrados = ?, "
						+ "calle = ?, "
						+ "numero = ?, "
						+ "planta = ?, "
						+ "ciudad = ?, "
						+ "precio_diario = ?, "
						+ "es_activo = ? "
						+ "where id_propiedad = ?;",

				propiedad.getTitulo(),
				propiedad.getId_nacional(),
				propiedad.getDescripcion(),
				propiedad.getTipo_propiedad(),
				propiedad.getCapacidad(),
				propiedad.getNum_habitaciones(),
				propiedad.getNum_banyos(),
				propiedad.getNum_camas(),
				propiedad.getMetros_cuadrados(),
				propiedad.getCalle(),
				propiedad.getNumero(),
				propiedad.getPlanta(),
				propiedad.getCiudad(),
				propiedad.getPrecio_diario(),
				propiedad.isEs_activo(),
				propiedad.getId_propiedad());
	}

	// Borra una propiedad de la base de datos.
	public void deletePropiedad(Propiedad propiedad) {
		this.jdbcTemplate.update("delete from propiedad where id_propiedad = ?;", propiedad.getId_propiedad());
	}

	// Borra todas las propiedades de un usuario de la base de datos
	public void deleteTodasPropiedades(String id_nacional) {
		this.jdbcTemplate.update("delete from propiedad where id_nacional = ?;", id_nacional);
	}
}
