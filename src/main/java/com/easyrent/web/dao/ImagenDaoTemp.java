package com.easyrent.web.dao;

import com.easyrent.web.domain.Imagen;
import com.easyrent.web.domain.Propiedad;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;


@Repository
public class ImagenDaoTemp {
    private JdbcTemplate jdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    private static final class ImagenMapper implements RowMapper<Imagen> {
        public Imagen mapRow(ResultSet rs, int rowNum) throws SQLException {
            Imagen imagen = new Imagen();

            imagen.setNom_imagen(rs.getString("nom_imagen"));
            imagen.setId_propiedad(rs.getInt("id_propiedad"));
            imagen.setUrl(rs.getString("url"));

            return imagen;
        }
    }

    // Devuelve un listado con todas las imagenes de la base de datos.
    public List<Imagen> getImagenesAdmin() {
        return this.jdbcTemplate.query("select * from imagen order by nom_imagen;", new ImagenMapper());
    }

    // Devuelve un listado con todas las imagenes de un usuario.
    public List<Imagen> getImagenesUser(String id_nacional) {
        return this.jdbcTemplate.query("select * from imagen as img join propiedad as prop" +
                " ON img.id_propiedad = prop.id_propiedad where id_nacional = ?;",
                new Object[] { id_nacional }, new ImagenMapper());
    }

    public List<Imagen> getImagenesNuevas() {
        return this.jdbcTemplate.query("select * from imagen as img join propiedad as prop" +
                " ON img.id_propiedad = prop.id_propiedad where prop.es_activo=TRUE group by img.id_propiedad order by img.id_propiedad DESC LIMIT 4;",
                new ImagenMapper());
    }

    // Devuelve todas las imagenes de una propiedad.
    public List<Imagen> getImagenPropiedad(int id_propiedad) {
        return this.jdbcTemplate.query("select * from imagen where id_propiedad = ? order by nom_imagen;",
                new Object[]{id_propiedad}, new ImagenMapper());
    }

    // Devuelve los datos de una imagen a partir de su nombre e id propiedad.
    public Imagen getImagenNomId(String nom_imagen, int id_propiedad) {
        try {
            return this.jdbcTemplate.queryForObject("select * from imagen where nom_imagen = ? AND id_propiedad = ?;",
                    new Object[]{nom_imagen, id_propiedad}, new ImagenMapper());
        }
        catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    // Anyade una nueva imagen a la base de datos.
    public void addImagen(Imagen imagen) {
        this.jdbcTemplate.update("insert into imagen(nom_imagen, id_propiedad, url) values(?, ?, ?)",
                imagen.getNom_imagen(),
                imagen.getId_propiedad(),
                imagen.getUrl());
    }

    // Modifica los datos de una imagen.
    public void updateImagen(Imagen imagen) {
        this.jdbcTemplate.update("update imagen set id_propiedad = ?, url = ? WHERE nom_imagen = ?;",
                imagen.getId_propiedad(),
                imagen.getUrl(),
                imagen.getNom_imagen());
    }

    // Borra una imagen de la base de datos.
    public void deleteImagen(Imagen imagen) {
        this.jdbcTemplate.update("DELETE FROM imagen WHERE nom_imagen = ?", imagen.getNom_imagen());
    }
}
