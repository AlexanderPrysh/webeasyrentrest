package com.easyrent.web.dao;

import com.easyrent.web.domain.Reserva;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Repository
public class ReservaDaoTemp {
    private JdbcTemplate jdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    private static final class ReservaMapper implements RowMapper<Reserva> {
        public Reserva mapRow(ResultSet rs, int rowNum) throws SQLException {
            Reserva reserva = new Reserva();
            reserva.setNum_seguimiento(rs.getInt("num_seguimiento"));
            reserva.setId_nacional(rs.getString("id_nacional"));
            reserva.setId_propiedad(rs.getInt("id_propiedad"));
            reserva.setFecha_reserva(rs.getTimestamp("fecha_reserva"));
            reserva.setFecha_confirmacion(rs.getDate("fecha_confirmacion"));
            reserva.setNum_personas(rs.getInt("num_personas"));
            reserva.setFecha_inicio(rs.getDate("fecha_inicio"));
            reserva.setFecha_fin(rs.getDate("fecha_fin"));
            reserva.setCoste_total(rs.getDouble("coste_total"));
            reserva.setEstado(rs.getString("estado"));
            return reserva;
        }
    }

    // Devuelve una lista de todas las reservas de la base de datos.
    public List<Reserva> getReservasAdmin() {
        return this.jdbcTemplate.query("select * from reserva order by num_seguimiento ", new ReservaMapper());
    }

    // Devuelve una lista de todas las reservas de un usuario.
    public List<Reserva> getReservasUser(String id_nacional) {
        return this.jdbcTemplate.query("select * from reserva where id_nacional = ? order by num_seguimiento ",
                    new Object[]{id_nacional}, new ReservaMapper());
    }

    // Devuelve una lista de todas las reservas de una propiedad.
    public List<Reserva> getReservasPropiedad(int id_propiedad) {
        return this.jdbcTemplate.query("select * from reserva where id_propiedad = ? order by fecha_inicio ",
                    new Object[]{id_propiedad}, new ReservaMapper());
    }

    // Devuelve una lista de todas las reservas de una propiedad ordenadas.
    public List<Reserva> getReservasPropiedadOrdenadas(int id_propiedad) {
        return this.jdbcTemplate.query("select * from reserva where id_propiedad = ? order by estado DESC",
                new Object[]{id_propiedad}, new ReservaMapper());
    }

    // Devuelve una lista de reservas con estado "PENDIENTE" de una propiedad.
    public List<Reserva> getReservasPendientesPropiedad(int id_propiedad) {
        return this.jdbcTemplate.query("select * from reserva where id_propiedad = ? and estado = 'PENDIENTE';",
                    new Object[] {id_propiedad}, new ReservaMapper());
    }

    public List<Reserva> getReservasPendientes() {
        return this.jdbcTemplate.query("select * from reserva where estado = 'PENDIENTE';",
                 new ReservaMapper());
    }

    // Devuelve los datos de una reserva.
    public Reserva getReserva(int num_seguimiento) {
        try {
            return this.jdbcTemplate.queryForObject("select * from reserva where num_seguimiento = ?",
                    new Object[]{num_seguimiento}, new ReservaMapper());
        }
        catch(EmptyResultDataAccessException e) {
            return null;
        }
    }

    // Devuelve los datos de una reserva más tarde de una propiedad..
    public Reserva getReservaMasTarde(int id_propiedad) {
        try {
            return this.jdbcTemplate.queryForObject("select * from reserva where id_propiedad = ? order by fecha_inicio " +
                    "DESC LIMIT 1;", new Object[] {id_propiedad}, new ReservaMapper());
        }
        catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    // Anyade una reserva a la base de datos.
    public void addReserva(Reserva reserva) {
        this.jdbcTemplate.update(
                "insert into reserva(id_nacional, id_propiedad, fecha_reserva, fecha_confirmacion, num_personas, fecha_inicio, fecha_fin, coste_total, estado) "
                        + "values(?, ?, now(), ?, ?, ?, ?, ?, ?); ",
                reserva.getId_nacional(),
                reserva.getId_propiedad(),
                reserva.getFecha_confirmacion(),
                reserva.getNum_personas(),
                reserva.getFecha_inicio(),
                reserva.getFecha_fin(),
                reserva.getCoste_total(),
                "PENDIENTE");
    }

    public void confirmarReserva(Reserva reserva3) {
        this.jdbcTemplate.update(
                "update reserva "
                        + "set id_nacional = ?, "
                        + "id_propiedad = ?, "
                        + "fecha_reserva = ?, "
                        + "fecha_confirmacion = ?, "
                        + "num_personas = ?, "
                        + "fecha_inicio = ?, "
                        + "fecha_fin = ?, "
                        + "coste_total = ?, "
                        + "estado = ? "
                        + "where num_seguimiento = ?; ",
                reserva3.getId_nacional(),
                reserva3.getId_propiedad(),
                reserva3.getFecha_reserva(),
                reserva3.getFecha_confirmacion(),
                reserva3.getNum_personas(),
                reserva3.getFecha_inicio(),
                reserva3.getFecha_fin(),
                reserva3.getCoste_total(),
                reserva3.getEstado(),
                reserva3.getNum_seguimiento());
    }

    // Borra una reserva de la base de datos.
    public void deleteReserva(Reserva reserva) {
        this.jdbcTemplate.update("delete from reserva  where num_seguimiento = ?", reserva.getNum_seguimiento());
    }
}
