package com.easyrent.web.dao;

import com.easyrent.web.domain.Valoracion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class ValoracionDaoTemp {
    private JdbcTemplate jdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    private static final class ValoracionMapper implements RowMapper<Valoracion> {
        public Valoracion mapRow(ResultSet rs, int rowNum) throws SQLException {
            Valoracion valoracion = new Valoracion();
            valoracion.setId_propiedad(rs.getInt(1));
            valoracion.setId_nacional(rs.getString(2));
            valoracion.setVoto(rs.getInt(3));
            valoracion.setComentario(rs.getString(4));
            return valoracion;
        }
    }

    // Devuelve un listado con todas las valoraciones de la base de datos.
    public List<Valoracion> getValoracionesAdmin() {
        return this.jdbcTemplate.query("select * from valoracion order by id_nacional ", new ValoracionMapper());
    }

    // Devuelve un listado con todas las valoraciones de un usuario.
    public List<Valoracion> getValoracionesUser(String id_nacional) {
        return this.jdbcTemplate.query("select * from valoracion where id_nacional = ? order by id_propiedad;",
                new Object[] {id_nacional}, new ValoracionMapper());
    }

    // Devuelve un listado con todas las valoraciones de una propiedad.
    public List<Valoracion> getValoracionesPropiedad(int id_propiedad) {
        return this.jdbcTemplate.query("select * from valoracion where id_propiedad = ?;",
                new Object[] {id_propiedad}, new ValoracionMapper());
    }

    // Devuelve los datos de una valoracion.
    public Valoracion getValoracion(int id_propiedad, String id_nacional) {
        try {
            return this.jdbcTemplate.queryForObject("select * from valoracion where id_nacional = ? AND id_propiedad = ?",
                    new Object[]{id_nacional, id_propiedad}, new ValoracionMapper());
        }
        catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    // Anyade una nueva valoracion a la base de datos.
    public void addValoracion(Valoracion valoracion) {
        this.jdbcTemplate.update("insert into valoracion(id_propiedad, id_nacional, voto, comentario) values(?, ?, ?, ?)",
                valoracion.getId_propiedad(),
                valoracion.getId_nacional(),
                valoracion.getVoto(),
                valoracion.getComentario());
    }

    // Borra una valoracion de la base de datos.
    public void deleteValoracion(Valoracion valoracion) {
        this.jdbcTemplate.update("delete from valoracion where id_propiedad = ? AND id_nacional = ?",
                valoracion.getId_propiedad(), valoracion.getId_nacional());
    }
}