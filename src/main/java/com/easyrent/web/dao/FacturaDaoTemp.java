package com.easyrent.web.dao;

import com.easyrent.web.domain.Factura;

import java.sql.SQLException;
import java.sql.ResultSet;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class FacturaDaoTemp {
    private JdbcTemplate jdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    private static final class FacturaMapper implements RowMapper<Factura> {
        public Factura mapRow(ResultSet rs, int rowNum) throws SQLException {
            Factura factura = new Factura();
            factura.setNum_factura(rs.getInt(1));
            factura.setNum_seguimiento(rs.getInt(2));
            factura.setFecha_factura(rs.getDate(3));
            factura.setIva(rs.getDouble(4));
            return factura;
        }
    }

    // Devuleve una lista con todas las facturas de la base de datos.
    public List<Factura> getFacturasAdmin() {
        return this.jdbcTemplate.query("select * from factura order by num_factura ", new FacturaMapper());
    }

    // Devuleve una lista con todas las facturas de un usuario.
    public List<Factura> getFacturasUser(String id_nacional) {
        return this.jdbcTemplate.query("select * " +
                "from factura as fac join reserva as res ON fac.num_seguimiento = res.num_seguimiento where res.id_nacional = ?",
                new Object[] {id_nacional}, new FacturaMapper());
    }

    // Devuelve un a lista de facturas de una propiedad de un usuario.
    public List<Factura> getFacturasPropiedadUsuario(String id_nacional, int id_propiedad) {
        return this.jdbcTemplate.query("select * from factura as fac join reserva as res ON fac.num_seguimiento = res.num_seguimiento " +
                "where res.id_nacional = ? AND res.id_propiedad = ?;", new Object[] {id_nacional, id_propiedad}, new FacturaMapper());
    }

    // Devuelve los datos de una factura a partir de su numero de factura.
    public Factura getFactura(int num_factura) {
        try {
            return this.jdbcTemplate.queryForObject("select * from factura where num_factura = ?", new Object[]{num_factura}, new FacturaMapper());
        }
        catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    // Devuelve los datos de una factura a partir del numero de seguimiento de la reserva.
    public Factura getFacturaNumeroSeguimiento(int num_seguimiento) {
        try {
            return this.jdbcTemplate.queryForObject("select * from factura where num_seguimiento = ?",
                    new Object[]{ num_seguimiento }, new FacturaMapper());
        }
        catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    // Anyade una factura a la base de datos.
    public void addFactura(Factura factura) {
        this.jdbcTemplate.update("insert into factura(num_seguimiento, fecha_factura, iva) values(?, ?, ?)",
                factura.getNum_seguimiento(),
                factura.getFecha_factura(),
                21.0);
    }
}

