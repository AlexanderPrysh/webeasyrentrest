package com.easyrent.web.dao;

import com.easyrent.web.domain.Servicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class ServicioDaoTemp {
    private JdbcTemplate jdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    private static final class ServicioMapper implements RowMapper<Servicio> {
        public Servicio mapRow(ResultSet rs, int rowNum) throws SQLException {
            Servicio servicio = new Servicio();
            servicio.setId_servicio(rs.getInt(1));
            servicio.setNombre(rs.getString(2));

            return servicio;
        }
    }

    // Devuelve una lista con todos los servicios de la base de datos.
    public List<Servicio> getServicios() {
        return this.jdbcTemplate.query("select * from servicio order by id_servicio ", new ServicioMapper());
    }

    // Devuelve una lista de servicios de una propiedad.
    public List<Servicio> getServiciosPropiedad(int id_propiedad) {
        return this.jdbcTemplate.query("select * from servicio as ser join propServicios as propSer " +
                "ON ser.id_servicio = propSer.id_servicio where propSer.id_propiedad = ?;",
                new Object[] {id_propiedad}, new ServicioMapper());
    }

    // Devuelve los datos de un servicio a partir de su id de servicio.
    public Servicio getServicio(int id_servicio) {
        try {
            return this.jdbcTemplate.queryForObject("select * from servicio where id_servicio = ?",
                    new Object[]{id_servicio}, new ServicioMapper());
        }
        catch(EmptyResultDataAccessException e) {
            return null;
        }
    }

    // Devuelve los datos de un servicio a partir de su nombre de servicio.
    public Servicio getServicioNombre(String nombre) {
        try {
            return this.jdbcTemplate.queryForObject("select * from servicio where nombre = ?",
                    new Object[]{nombre}, new ServicioMapper());
        }
        catch(EmptyResultDataAccessException e) {
            return null;
        }
    }

    // Anyade un nuevo a la base de datos.
    public void addServicio(Servicio servicio) {
        this.jdbcTemplate.update("insert into servicio(nombre) values(?)", servicio.getNombre());
    }

    // Modifica los datos de un servicio
    public void updateServicio(Servicio servicio) {
        this.jdbcTemplate.update("update servicio set nombre = ? where id_servicio = ?",
                servicio.getNombre(), servicio.getId_servicio());
    }

    // Elimina un servicio de la base de datos.
    public void deleteServicio(Servicio servicio) {
        this.jdbcTemplate.update("delete from servicio where id_servicio = ?", servicio.getId_servicio());
    }
}

