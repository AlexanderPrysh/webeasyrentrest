package com.easyrent.web.dao;

import com.easyrent.web.domain.Usuario;
import org.jasypt.util.password.BasicPasswordEncryptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class UsuarioDaoTemp {

	private JdbcTemplate jdbcTemplate;
	private BasicPasswordEncryptor passwordEncryptor = new BasicPasswordEncryptor();

	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	private static final class UsuarioMapper implements RowMapper<Usuario> {
		public Usuario mapRow(ResultSet rs, int rowNum) throws SQLException {
			Usuario usuario = new Usuario();

			usuario.setId_usuario(rs.getInt("id_usuario"));
			usuario.setId_nacional(rs.getString("id_nacional"));
			usuario.setUsuario(rs.getString("usuario"));
			usuario.setContrasenya(rs.getString("contrasenya"));
			usuario.setRol(rs.getString("rol"));
			usuario.setNombre(rs.getString("nombre"));
			usuario.setApellido(rs.getString("apellido"));
			usuario.setEmail(rs.getString("email"));
			usuario.setDireccion(rs.getString("direccion"));
			usuario.setFecha_registro(rs.getDate("fecha_registro"));
			usuario.setNumero_telefono(rs.getString("numero_telefono"));
			usuario.setEs_activo(rs.getBoolean("es_activo"));

			return usuario;
		}
	}

	// Devuelve un listado con todos los usuarios de la base de datos.
	public List<Usuario> getUsuarios() {
		return this.jdbcTemplate.query("select * from usuario order by id_usuario;", new UsuarioMapper());

	}

	// Si existe el usuario y la contrasenya correcta, devuelve los datos del usuario.
	public Usuario getUsuario(String usuario, String contrasenya) {
		Usuario usu = getUsuarioLogin(usuario);

		if (usu != null && passwordEncryptor.checkPassword(contrasenya, usu.getContrasenya()))
			return usu;

		return null;
	}

	// Devuelve los datos de un usuario (para el login).
	private Usuario getUsuarioLogin(String usuario) {
		try {
			return this.jdbcTemplate.queryForObject("select * from usuario where usuario = ?;",
					new Object[] { usuario }, new UsuarioMapper());
		}
		catch (EmptyResultDataAccessException e) {
			return null;
		}
	}

	// Devuelve los datos de un usuario a partir de su id_usuario.
	public Usuario getUsuarioDatos(int id_usuario) {
		try {
			return this.jdbcTemplate.queryForObject("select * from usuario where id_usuario = ?;",
					new Object[] { id_usuario }, new UsuarioMapper());
		}
		catch (EmptyResultDataAccessException e) {
			return null;
		}
	}

	// Devuelve los datos de un usuario a partir de su DNI.
	public Usuario getUsuarioDNI(String id_nacional) {
		try {
			return this.jdbcTemplate.queryForObject("select * from usuario where id_nacional = ?;",
					new Object[] { id_nacional }, new UsuarioMapper());
		}
		catch (EmptyResultDataAccessException e) {
			return null;
		}
	}

	// Devuelve los datos de un usuario a partir de su nombre de usuario.
	public Usuario getUsuarioNombreUsuario(String usuario) {
		try {
			return this.jdbcTemplate.queryForObject("select * from usuario where usuario = ?;",
					new Object[] { usuario }, new UsuarioMapper());
		}
		catch (EmptyResultDataAccessException e) {
			return null;
		}
	}

	// Devuelve los datos de un usuario a partir de su email.
	public Usuario getUsuarioEmail(String email) {
		try {
			return this.jdbcTemplate.queryForObject("select * from usuario where email = ?;",
					new Object[] { email }, new UsuarioMapper());
		}
		catch (EmptyResultDataAccessException e) {
			return null;
		}
	}

	// Anyade un nuevo usuario a la base de datos (por administrador).
	public void addUsuario(Usuario usuario) {
		this.jdbcTemplate.update(
				"insert into usuario(id_nacional, usuario, contrasenya, rol, nombre, apellido, email, direccion, fecha_registro, " +
						"numero_telefono, es_activo) values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);",
				usuario.getId_nacional(),
				usuario.getUsuario(),
				passwordEncryptor.encryptPassword(usuario.getContrasenya()),
				usuario.getRol(),
				usuario.getNombre(),
				usuario.getApellido(),
				usuario.getEmail(),
				usuario.getDireccion(),
				usuario.getFecha_registro(),
				usuario.getNumero_telefono(),
				true);
	}

	// Anyade un nuevo usuario a la base de datos (publico).
	public void addUsuarioUser(Usuario usuario) {
		this.jdbcTemplate.update(
				"insert into usuario(id_nacional, usuario, contrasenya, rol, nombre, apellido, email, direccion, fecha_registro, " +
						"numero_telefono, es_activo) values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);",
				usuario.getId_nacional(),
				usuario.getUsuario(),
				passwordEncryptor.encryptPassword(usuario.getContrasenya()),
				"user",
				usuario.getNombre(),
				usuario.getApellido(),
				usuario.getEmail(),
				usuario.getDireccion(),
				usuario.getFecha_registro(),
				usuario.getNumero_telefono(),
				false);
	}

	// Modifica los datos de un usuario.
	public void updateUsuario(Usuario usuario) {
		this.jdbcTemplate.update("update usuario set id_nacional = ?, usuario = ?, contrasenya = ?, rol = ?, nombre = ?, apellido = ?, " +
						"email = ?, direccion = ?, fecha_registro = ?, numero_telefono = ?, es_activo = ? where id_usuario = ?;",
				usuario.getId_nacional(),
				usuario.getUsuario(),
				usuario.getContrasenya(),
				usuario.getRol(),
				usuario.getNombre(),
				usuario.getApellido(),
				usuario.getEmail(),
				usuario.getDireccion(),
				usuario.getFecha_registro(),
				usuario.getNumero_telefono(),
				usuario.isEs_activo(),
				usuario.getId_usuario());
	}

	// Cambiar la contrasenya del usuario.
	public void updateUsuarioContrasenya(Usuario usuario) {
		this.jdbcTemplate.update("update usuario set id_nacional = ?, usuario = ?, contrasenya = ?, rol = ?, nombre = ?, apellido = ?, " +
						"email = ?, direccion = ?, fecha_registro = ?, numero_telefono = ?, es_activo = ? where id_usuario = ?;",
				usuario.getId_nacional(),
				usuario.getUsuario(),
				passwordEncryptor.encryptPassword(usuario.getContrasenya()),
				usuario.getRol(),
				usuario.getNombre(),
				usuario.getApellido(),
				usuario.getEmail(),
				usuario.getDireccion(),
				usuario.getFecha_registro(),
				usuario.getNumero_telefono(),
				usuario.isEs_activo(),
				usuario.getId_usuario());
	}

	// Borra un usuario de la base de datos.
	public void deleteUsuario(Usuario usuario) {
		this.jdbcTemplate.update("DELETE FROM usuario WHERE id_usuario = ?;", usuario.getId_usuario());
	}
}
