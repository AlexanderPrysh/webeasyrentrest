package com.easyrent.web.dao;

import com.easyrent.web.domain.PropServicios;
import com.easyrent.web.domain.Propiedad;
import com.easyrent.web.domain.Servicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class PropServiciosDaoTemp {
    private JdbcTemplate jdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    private static final class PropServiciosMapper implements RowMapper<PropServicios> {
        public PropServicios mapRow(ResultSet rs, int rowNum) throws SQLException {
            PropServicios propServicios = new PropServicios();
            propServicios.setId_propiedad(rs.getInt(1));
            propServicios.setId_servicio(rs.getInt(2));
            return propServicios;
        }
    }

    // Devuelve un propServicio de la base de datos.
    public PropServicios getPropServicios(int id_propiedad, int id_servicio) {
        try {
            return this.jdbcTemplate.queryForObject("select * from propServicios where id_propiedad = ? AND id_servicio = ?",
                    new Object[]{id_propiedad, id_servicio}, new PropServiciosMapper());
        }
        catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    // Devuelve los servicios de una propiedad..
    public List<PropServicios> getPropServiciosPropiedad(int id_propiedad) {
        try {
            return this.jdbcTemplate.query("select * from propServicios where id_propiedad = ?",
                    new Object[] {id_propiedad}, new PropServiciosMapper());
        }
        catch(EmptyResultDataAccessException e) {
            return null;
        }
    }

    // Anyade un nuevo propServicio a la base de datos.
    public void addPropServicios(int id_servicio, int id_propiedad) {
        this.jdbcTemplate.update("insert into propServicios(id_propiedad, id_servicio) values(?, ?)",
               id_propiedad, id_servicio);
    }

    // Borra un propServicio de la base de datos.
    public void deletePropServicios(PropServicios servicio) {
        this.jdbcTemplate.update("delete from propServicios  where id_servicio = ?", servicio.getId_servicio());
    }
}

