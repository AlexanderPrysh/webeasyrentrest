package com.easyrent.web.dao;

import com.easyrent.web.domain.Periodo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class PeriodoDaoTemp {
	private JdbcTemplate jdbcTemplate;

	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	private static final class PeriodoMapper implements RowMapper<Periodo> {
		public Periodo mapRow(ResultSet rs, int rowNum) throws SQLException {
			Periodo periodo = new Periodo();
		
			periodo.setId_periodo(rs.getInt("id_periodo"));
			periodo.setId_propiedad(rs.getInt("id_propiedad"));
			periodo.setInicio(rs.getDate("inicio"));
			periodo.setFin(rs.getDate("fin"));
			
			return periodo;
		}
	}

	// Devuelve los datos de un periodo.
	public Periodo getPeriodo(int id_periodo) {
		try {
			return this.jdbcTemplate.queryForObject("select * from periodo where id_periodo = ?;",
					new Object[] { id_periodo }, new PeriodoMapper());
		}
		catch(EmptyResultDataAccessException e) {
			return null;
		}
	}

	// Devuelve una lista de periodos de una propiedad..
	public List<Periodo> getPeriodoPropiedad(int id_propiedad) {
		return this.jdbcTemplate.query("select * from periodo where id_propiedad = ? order by inicio;",
					new Object[] { id_propiedad }, new PeriodoMapper());
	}

	// Anyade un nuevo periodo a la base de datos (a una propiedad).
	public void addPeriodo(Periodo periodo) {
		this.jdbcTemplate.update("insert into periodo(id_propiedad, inicio, fin) values(?, ?, ?)",
				periodo.getId_propiedad(),
				periodo.getInicio(),
				periodo.getFin());
	}

	// Modifica los datos de un periodo.
	public void updatePeriodo(Periodo periodo) {
		this.jdbcTemplate.update("update periodo set id_propiedad = ?, inicio = ?, fin = ? WHERE id_periodo = ?;",
				periodo.getId_propiedad(),
				periodo.getInicio(),
				periodo.getFin(),
				periodo.getId_periodo());
	}

	// Borra un periodo de la base de datos (de una propiedad).
	public void deletePeriodo(Periodo periodo) {
		this.jdbcTemplate.update("DELETE FROM periodo WHERE id_periodo = ?", periodo.getId_periodo());
	}
}
