package com.easyrent.web.services;

import com.easyrent.web.dao.PropiedadDaoTemp;
import com.easyrent.web.domain.Propiedad;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SearchService {

    @Autowired
    PropiedadDaoTemp propDAO;

    public List<Propiedad> searchProp(String tipoProp, String ciudad, Double minPrice, Double maxPrice, int minhab, int maxhab, int minM2, int maxM2, String descrip) {
        return propDAO.getPropiedadSearch(tipoProp, ciudad, minPrice, maxPrice, minhab, maxhab, minM2, maxM2, descrip);
    }
}
