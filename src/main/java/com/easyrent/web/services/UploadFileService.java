package com.easyrent.web.services;

import com.easyrent.web.dao.ImagenDaoTemp;
import com.easyrent.web.domain.Imagen;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class UploadFileService {

    @Autowired
    ImagenDaoTemp imagenDAO;

    public void addImageService( String nom_imagen, int id_propiedad, String ruta) {
        Imagen img = new Imagen();
        img.setNom_imagen(nom_imagen);
        img.setId_propiedad(id_propiedad);
        img.setUrl(ruta);
        imagenDAO.addImagen(img);
    }
}
