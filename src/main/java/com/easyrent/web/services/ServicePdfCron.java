package com.easyrent.web.services;

import com.easyrent.web.dao.PropiedadDaoTemp;
import com.easyrent.web.dao.ReservaDaoTemp;
import com.easyrent.web.dao.UsuarioDaoTemp;
import com.easyrent.web.domain.Propiedad;
import com.easyrent.web.domain.Reserva;
import com.easyrent.web.domain.Usuario;
import com.easyrent.web.email.Mail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Repository
public class ServicePdfCron {

    private ReservaDaoTemp reservaDao;
    private UsuarioDaoTemp usuarioDao;
    private PropiedadDaoTemp propiedadDao;

    @Autowired
    public void setServicePdfCron(ReservaDaoTemp reservaDao, UsuarioDaoTemp usuarioDao, PropiedadDaoTemp propiedadDao) {
        this.reservaDao = reservaDao;
        this.usuarioDao = usuarioDao;
        this.propiedadDao = propiedadDao;
    }

    public void cleanDustOne() {

        System.out.println("Executing cleanDustOne method...");
        List<Reserva> listResPend=reservaDao.getReservasPendientes();
        Usuario inquilino;


        for (Reserva res : listResPend){

            Date dateFac = new Date(res.getFecha_reserva().getTime());
            long[] diff = getTimeDifference(dateFac, new Date());
            System.out.printf("Time difference is %d day(s), %d hour(s), %d minute(s), %d second(s) and %d millisecond(s)\n",
                    diff[0], diff[1], diff[2], diff[3], diff[4]);

            if (  diff[2]>=20 ){


                /*Aqui va el envio de correo para el inquilino con reserva BORRADA */
                System.out.println(res.getId_nacional());

                inquilino = usuarioDao.getUsuarioDNI(res.getId_nacional());
                String emailUsuario = inquilino.getEmail();
                System.out.println("email-inquilino:"+emailUsuario);
                String result = "Correo enviado con éxito";
                Mail mail = new Mail();
                mail.setRecipient(emailUsuario);
                mail.setSubject("ATENCIÓN su reserva ha sido eliminada al pasar el plazo de 24H.");
                String body = "Hola "+inquilino.getNombre()+"!<br /><br /> Le informamos de que ha pasado el plazo de 24h , y el propietario NO ha confirmado su reserva con Nº de seguimiento: "+res.getNum_seguimiento()+" de esta <a href='http://localhost:8080/showpropiedad/'"+res.getId_propiedad()+"'.html'>propiedad</a>. Por tanto su reserva ha sido ELMINADA de nuestra base de datos.<br />Atentamente,<br /> El Equipo de Asistencia de EasyRent.com <br />";
                mail.setBodyText(body);
                mail.setAttachFile(null);
                boolean verificacion= true;
                System.out.println(" Correo para inquilino con reserva borrada: "+ verificacion);
                verificacion = mail.sendEmail();
                if (!verificacion) { result="Hay un problema con nuestro servidor de correo, por favor comunique con nosotros por teléfono, perdone las molestias"; }
                System.out.println(" Correo para usuario que ha hecho la reserva enviado: " + verificacion);
                System.out.println(result);


                 /*Aqui va el envio de correo para el propietario de una reserva BORRADA*/

                Propiedad propiedad = propiedadDao.getPropiedad(res.getId_propiedad());
                String dni_propietario = propiedad.getId_nacional();
                Usuario propietario = usuarioDao.getUsuarioDNI(dni_propietario);
                String emailUsuario2 = propietario.getEmail();
                System.out.println("email-propietario:"+emailUsuario2);
                String result2 = "Correo enviado con éxito";
                Mail mail2 = new Mail();
                mail2.setRecipient(emailUsuario2);
                mail2.setSubject("ATENCIÓN una reserva ELIMINADA de su propiedad con Titulo:"+propiedad.getTitulo()+" al pasar el plazo de 24H.");
                String body2 = "Hola "+propietario.getNombre()+"!<br /><br /> Le informamos de que ha pasado el plazo de 24h de confirmación de la RESERVA con datos: <br />Fecha de la reserva: "+res.getFecha_reserva()+"<br />Fecha inicio: "+res.getFecha_inicio()+"<br />Fecha fin: "+res.getFecha_fin()+"<br />Num. personas: "+res.getNum_personas()+"<br />Coste total de la reserva: "+res.getCoste_total()+"euros<br /><br /> de su PROPIEDAD con datos : <br />Titulo: "+propiedad.getTitulo()+"<br />Descripcion: "+propiedad.getDescripcion()+"<br />Direccion: "+propiedad.getCalle()+" "+propiedad.getNumero() +"<br />. Por tanto la reserva ha sido ELMINADA de nuestra base de datos.<br />Atentamente,<br /> El Equipo de Asistencia de EasyRent.com <br />";
                mail2.setBodyText(body2);
                mail2.setAttachFile(null);
                boolean verificacion2= true;
                System.out.println(" Correo para propietario con reserva borrada: "+ verificacion2);
                verificacion2 = mail2.sendEmail();
                if (!verificacion2) { result2="Hay un problema con nuestro servidor de correo, por favor comunique con nosotros por teléfono, perdone las molestias"; }
                   System.out.println(" Correo para propietario con reserva borrada enviado: " + verificacion2);
                System.out.println(result2);

                /*reservaDao.deleteReserva(res);*/
                System.out.println("La reserva ha sido borrada a los "+diff[2]+" minutos: num_reserva= "+res.getNum_seguimiento());
            }
        }
    }

    private long[] getTimeDifference(Date d1, Date d2) {
        long[] result = new long[5];

        long diff = d2.getTime() - d1.getTime();//as given

        long miliseconds = TimeUnit.MILLISECONDS.toMillis(diff);
        long seconds = TimeUnit.MILLISECONDS.toSeconds(diff);
        long minutes = TimeUnit.MILLISECONDS.toMinutes(diff);
        long hours = TimeUnit.MILLISECONDS.toHours(diff);
        long days = TimeUnit.MILLISECONDS.toDays(diff);

        result[0] = days;
        result[1] = hours;
        result[2] = minutes;
        result[3] = seconds;
        result[4] = miliseconds;

        return result;
    }

}