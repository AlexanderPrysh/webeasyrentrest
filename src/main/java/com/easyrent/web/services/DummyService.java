package com.easyrent.web.services;

import org.springframework.stereotype.Service;

@Service
public class DummyService {
    public String sayHello() {
        return "hello";
    }
}
