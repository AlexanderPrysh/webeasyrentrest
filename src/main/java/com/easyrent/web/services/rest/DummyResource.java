package com.easyrent.web.services.rest;

import com.easyrent.web.services.DummyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Component
@Path("/hello")
public class DummyResource {


    @Autowired
    private DummyService dummyService;


    @GET
    public String hello() {
        return dummyService.sayHello();
    }

    @GET
    @Path("/super")
    public String superhello() {
        return dummyService.sayHello()+" supper";
    }
}
