package com.easyrent.web.services.rest;

import java.io.*;
import java.net.URISyntaxException;
import java.net.URL;
import java.sql.*;
import javax.sql.DataSource;
import java.util.List;
import java.util.Properties;
import java.util.logging.Logger;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.easyrent.web.dao.ImagenDaoTemp;
import com.easyrent.web.services.UploadFileService;
import com.sun.jersey.api.core.InjectParam;
import com.sun.jersey.core.header.ContentDisposition;
import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.BodyPart;
import com.sun.jersey.multipart.FormDataBodyPart;
import com.sun.jersey.multipart.FormDataMultiPart;
import com.sun.jersey.multipart.FormDataParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;


@Path("/file")
public class UploadFileResource {

	@InjectParam
	private UploadFileService imgServices;

	@POST
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Path("/upload")


	public Response uploadFile(
            @QueryParam("id_prop") int id_prop,
			@FormDataParam("file") InputStream uploadedInputStream,
			/*@FormDataParam("file") FormDataContentDisposition fileDetail,*/
			FormDataMultiPart formParams) throws URISyntaxException {

		String uploadedFileLocation = "";
		List<FormDataBodyPart> parts = formParams.getFields("file");

        for (FormDataBodyPart part : parts) {
			FormDataContentDisposition fileDetail = part.getFormDataContentDisposition();
            uploadedFileLocation = "\\static\\imgs\\" + fileDetail.getFileName();
            String[] nom = fileDetail.getFileName().split("\\.",-1);
            System.out.println(nom[0]);
            if (!nom[0].isEmpty()) {
                InputStream is = part.getEntityAs(InputStream.class);
                writeToFile(is, uploadedFileLocation);
                imgServices.addImageService(nom[0], id_prop, uploadedFileLocation);
            }
		}
		java.net.URI location = new java.net.URI("../my/propiedad/"+id_prop+"/imagenes.html");
		return Response.temporaryRedirect(location).build();
	}

	// save uploaded file to new location
	private void writeToFile(InputStream uploadedInputStream,
			String uploadedFileLocation) {

		try {
			//OutputStream out = new FileOutputStream(new File(uploadedFileLocation));
			int read = 0;
			byte[] bytes = new byte[1024];
			File f1 = new File("");
			String pathABS = f1.getAbsolutePath()+"\\src\\main\\webapp"+uploadedFileLocation;
			System.out.println("path:"+ pathABS);
			File f2 = new File(pathABS);
			OutputStream out = new FileOutputStream(f2);

			while ((read = uploadedInputStream.read(bytes)) != -1) {
				out.write(bytes, 0, read);
			}
			out.flush();
			out.close();
		} catch (IOException e) {

			e.printStackTrace();
		}

	}

}