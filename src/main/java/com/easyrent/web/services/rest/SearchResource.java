package com.easyrent.web.services.rest;

import com.easyrent.web.dao.PropiedadDaoTemp;
import com.easyrent.web.domain.Propiedad;
import com.easyrent.web.services.DummyService;
import com.easyrent.web.services.SearchService;
import com.google.gson.Gson;
import com.sun.jersey.api.core.InjectParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.util.List;


@Path("/search")
public class SearchResource {

    @InjectParam
    private SearchService propServices;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String search(
            @QueryParam("tipo_prop") String tipo_prop,
            @QueryParam("ciudad") String ciudad,
            @QueryParam("precioMin") double precioMin,
            @QueryParam("precioMax") double precioMax,
            @QueryParam("num_hab_min") int habMin,
            @QueryParam("num_hab_max") int habMax,
            @QueryParam("metros_min") int minM2,
            @QueryParam("metros_max") int maxM2,
            @QueryParam("descripcion") String descrip

            ) {

        List<Propiedad> propiedades= propServices.searchProp(tipo_prop ,ciudad, precioMin, precioMax, habMin, habMax, minM2, maxM2, descrip);

        String jsonPropiedades =  new Gson().toJson(propiedades);

        return jsonPropiedades;
    }
}